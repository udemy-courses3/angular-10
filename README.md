# Curso de Udemy Angular: De cero a experto (Angular 10+)

Udemy Angular course: From zero to expert (Angular 10+)

## Índice del curso

- [Curso de Udemy Angular: De cero a experto (Angular 10+)](#curso-de-udemy-angular-de-cero-a-experto-angular-10)
  - [Índice del curso](#índice-del-curso)
- [Sección 1:Introducción al curso de Angular](#sección-1introducción-al-curso-de-angular)
  - [5. Instalaciones y configuraciones mínimas necesarias.](#5-instalaciones-y-configuraciones-mínimas-necesarias)
- [Sección 2:Gentil introducción a TypeScript y ES6](#sección-2gentil-introducción-a-typescript-y-es6)
  - [10. Demostración de TypeScript](#10-demostración-de-typescript)
  - [11. Configuración de TypeScript](#11-configuración-de-typescript)
  - [12. Variables let y const](#12-variables-let-y-const)
  - [13. Introducción a los tipos de datos](#13-introducción-a-los-tipos-de-datos)
  - [14. Excluir archivos a traducir](#14-excluir-archivos-a-traducir)
  - [15. Template literales del ES6](#15-template-literales-del-es6)
  - [16. Funciones: Parámetros opcionales, obligatorios y por defecto](#16-funciones-parámetros-opcionales-obligatorios-y-por-defecto)
  - [17. Funciones de Flecha](#17-funciones-de-flecha)
  - [18. Desestructuración de Objetos y Arreglos (arrays)](#18-desestructuración-de-objetos-y-arreglos-arrays)
  - [19. Promesas](#19-promesas)
  - [20. Promesas y su tipado en TypeScript](#20-promesas-y-su-tipado-en-typescript)
  - [21. Interfaces de TypeScript](#21-interfaces-de-typescript)
  - [22. Introducción a las Clases de la POO](#22-introducción-a-las-clases-de-la-poo)
  - [23. Definición de una clase básica en TypeScript](#23-definición-de-una-clase-básica-en-typescript)
  - [24. Constructores de una clase en TypeScript](#24-constructores-de-una-clase-en-typescript)
  - [25. Importaciones * URL](#25-importaciones--url)
  - [26. Decoradores de Clases](#26-decoradores-de-clases)
  - [27. Tipado del retorno de una función](#27-tipado-del-retorno-de-una-función)
  - [28. Exámen práctico #1](#28-exámen-práctico-1)
  - [29. Explicación de la tarea](#29-explicación-de-la-tarea)
  - [30. Resolución del examen práctico #1](#30-resolución-del-examen-práctico-1)
  - [Cuestionario 1: Examen teórico #1](#cuestionario-1-examen-teórico-1)
  - [31. Código fuente de la sección](#31-código-fuente-de-la-sección)
- [Sección 3:Aplicación #1: Hola Mundo](#sección-3aplicación-1-hola-mundo)
  - [35. Introducción a los componentes y directivas estructurales.](#35-introducción-a-los-componentes-y-directivas-estructurales)
  - [36. Nuestra primera interacción en Angular](#36-nuestra-primera-interacción-en-angular)
  - [37. Nota de Actualización del Angular CLI](#37-nota-de-actualización-del-angular-cli)
  - [38. Creando un entorno local de Angular](#38-creando-un-entorno-local-de-angular)
  - [39. Estructura del proyecto](#39-estructura-del-proyecto)
  - [40. Utilizando Bootstrap 4](#40-utilizando-bootstrap-4)
  - [41. TemplateUrl: Separando el HTML del componente](#41-templateurl-separando-el-html-del-componente)
  - [42. Creando el footer.component](#42-creando-el-footercomponent)
  - [43. Estructura del body component](#43-estructura-del-body-component)
  - [44. Directivas estructurales: *ngFor y el *ngIf](#44-directivas-estructurales-ngfor-y-el-ngif)
  - [Cuestionario 2: Examen teórico - de la sección Hola Mundo](#cuestionario-2-examen-teórico---de-la-sección-hola-mundo)
- [Sección 4:Aplicación #2: Aplicación de una sola página (SPA)](#sección-4aplicación-2-aplicación-de-una-sola-página-spa)
  - [49. Iniciar el proyecto - SPA](#49-iniciar-el-proyecto---spa)
  - [50. Creando la estructura de nuestro proyecto](#50-creando-la-estructura-de-nuestro-proyecto)
  - [51. Instalando el bootstrap (u otras liberías de terceros) cuando usamos el Angular-CLI](#51-instalando-el-bootstrap-u-otras-liberías-de-terceros-cuando-usamos-el-angular-cli)
  - [52. Configurando el navbar y otros componentes](#52-configurando-el-navbar-y-otros-componentes)
  - [53. Rutas en Angular](#53-rutas-en-angular)
  - [54. RouterLink y RouterLinkActive - Completando las rutas](#54-routerlink-y-routerlinkactive---completando-las-rutas)
  - [55. Componente Heroes - diseño](#55-componente-heroes---diseño)
  - [56. Introducción a los Servicios](#56-introducción-a-los-servicios)
  - [57. Creando nuestro primer servicio - HeroesService](#57-creando-nuestro-primer-servicio---heroesservice)
  - [58. Página de Heroes - Diseño con *ngFor](#58-página-de-heroes---diseño-con-ngfor)
  - [59. Rutas con parametros - Router](#59-rutas-con-parametros---router)
  - [60. Recibiendo parámetros por URL - ActivatedRoute](#60-recibiendo-parámetros-por-url---activatedroute)
  - [61. Tarea práctica #1 - Componente del héroe](#61-tarea-práctica-1---componente-del-héroe)
  - [62. Resolución de la tarea práctica #1 - Componente del héroe](#62-resolución-de-la-tarea-práctica-1---componente-del-héroe)
  - [63. Pipes: Transformación visual de la data.](#63-pipes-transformación-visual-de-la-data)
  - [64. Buscador de Héroes](#64-buscador-de-héroes)
  - [65. Tarea práctica #2: Crear la pantalla de búsqueda de héroes.](#65-tarea-práctica-2-crear-la-pantalla-de-búsqueda-de-héroes)
  - [66. Resolución de la tarea 2 - Buscador de Héroes.](#66-resolución-de-la-tarea-2---buscador-de-héroes)
  - [67. Plus: Mostrando un mensaje cuando no hay resultados.](#67-plus-mostrando-un-mensaje-cuando-no-hay-resultados)
  - [68. @Input - Recibir información de un componente padre a un hijo.](#68-input-recibir-informaci%C3%B3n-de-un-componente-padre-a-un-hijo)
  - [69. @Output - Emitir un evento del hijo hacia el padre.](#69-output-emitir-un-evento-del-hijo-hacia-el-padre)
  - [70. Arreglar detalles de la búsqueda.](#70-arreglar-detalles-de-la-búsqueda)
- [Sección 5:Pipes - Transforman los valores mostrados en pantalla](#secci%C3%B3n-5-pipes-transforman-los-valores-mostrados-en-pantalla)
  - [72. Introducción a la sección](#72-introducci%C3%B3n-a-la-secci%C3%B3n)
  - [73. ¿Qué aprenderemos en esta sección?](#73-qu%C3%A9-aprenderemos-en-esta-secci%C3%B3n)
  - [74. Demostración del resultado de la sección - Pipes](#74-demostraci%C3%B3n-del-resultado-de-la-secci%C3%B3n-pipes)
  - [75. Inicio del proyecto y la introducción a los Pipes](#75-inicio-del-proyecto-y-la-introducci%C3%B3n-a-los-pipes)
  - [76. Pipe: Slice](#76-pipe-slice)
  - [77. Pipe: Decimal](#77-pipe-decimal)
  - [78. Pipe: Percent - Porcentajes](#78-pipe-percent-porcentajes)
  - [79. Pipe: Currency - Moneda](#79-pipe-currency-moneda)
  - [80. Pipe: Json](#80-pipe-json)
  - [81. Pipe: Async](#81-pipe-async)
  - [82. Pipe: Date - Fecha](#82-pipe-date-fecha)
  - [83. Registrar otros idiomas](#83-registrar-otros-idiomas)
  - [84. Pipes personalizados: Capitalizar palabras](#84-pipes-personalizados-capitalizar-palabras)
  - [85. Pipe Personalizado: Domseguro](#85-pipe-personalizado-domseguro)
  - [86. Tarea de Pipes](#86-tarea-de-pipes)
  - [87. Resolución de la tarea de Pipes](#87-resoluci%C3%B3n-de-la-tarea-de-pipes)
  - [88. Código fuente de la sección](#88-c%C3%B3digo-fuente-de-la-secci%C3%B3n)
- [Sección 6:Aplicación #3: SpotiApp](#secci%C3%B3n-6aplicaci%C3%B3n-3-spotiapp)
  - [89. Introducción a la sección](#89-introducci%C3%B3n-a-la-secci%C3%B3n)
  - [90. ¿Qué aprenderemos en esta sección?](#90-qu%C3%A9-aprenderemos-en-esta-secci%C3%B3n)
  - [91. Demostración del resultado de esta sección](#91-demostraci%C3%B3n-del-resultado-de-esta-secci%C3%B3n)
  - [92. Sitio web de developer de Spotify](#92-sitio-web-de-developer-de-spotify)
  - [93. Iniciando el proyecto - SpotiApp](#93-iniciando-el-proyecto-spotiapp)
  - [94. Creando las rutas de nuestra aplicación](#94-creando-las-rutas-de-nuestra-aplicaci%C3%B3n)
  - [95. Introducción a las peticiones HTTP](#95-introducci%C3%B3n-a-las-peticiones-http)
  - [96. Actualización - Token para uso de servicios Spotify](#96-actualizaci%C3%B3n-token-para-uso-de-servicios-spotify)
  - [97. HTTPClient - Service: Conectándonos a Spotify](#97-httpclient-service-conect%C3%A1ndonos-a-spotify)
  - [98. Consumiendo información del servicio de Spotify](#98-consumiendo-informaci%C3%B3n-del-servicio-de-spotify)
  - [99. Componente de Búsqueda de artistas](#99-componente-de-b%C3%BAsqueda-de-artistas)
  - [100. Operador Map de los Observables](#100-operador-map-de-los-observables)
  - [101. Centralizar las peticiones hacia Spotify](#101-centralizar-las-peticiones-hacia-spotify)
  - [102. Verificación de imagen y pipe para manejar las imágenes](#102-verificaci%C3%B3n-de-imagen-y-pipe-para-manejar-las-im%C3%A1genes)
  - [103. Componente de tarjetas](#103-componente-de-tarjetas)
  - [104. Creando un loading component](#104-creando-un-loading-component)
  - [105. Página del artista, nueva ruta, parámetro por url y servicio](#105-p%C3%A1gina-del-artista-nueva-ruta-par%C3%A1metro-por-url-y-servicio)
  - [106. Obtener artista de Spotify](#106-obtener-artista-de-spotify)
  - [107. Servicio: Top-tracks](#107-servicio-top-tracks)
  - [108. Widgets de Spotify](#108-widgets-de-spotify)
  - [109. Manejo de errores de un observable](#109-manejo-de-errores-de-un-observable)
  - [110. Generar Token de Spotify de forma automática](#110-generar-token-de-spotify-de-forma-autom%C3%A1tica)
  - [Cuestionario 3: Examen teórico: SpotiApp](#cuestionario-3-examen-te%C3%B3rico-spotiapp)
  - [111. Código fuente de la sección](#111-c%C3%B3digo-fuente-de-la-secci%C3%B3n)
- [Sección 7:Aplicación #4: Lista de deseos - IONIC App]()
  - [112. Introducción a la sección]()
  - [113. ¿Qué aprenderemos en esta sección?]()
  - [114. Demostración del resultado de esta sección]()
  - [115. ¿Qué es ionic?]()
  - [116. Nota de actualización]()
  - [117. Creando el esqueleto de nuestra aplicación]()
  - [118. Material de la sección - DeseosApp]()
  - [119. Componente de Pendientes, estructura y tabs]()
  - [120. Servicio y clases de nuestra lista de deseos]()
  - [121. Pantalla de pendientes - diseño y documentación de ionic]()
  - [122. Navegación entre pantallas]()
  - [123. Diseño de la página de agregar]()
  - [124. Alert Controller - Agregar una lista a nuestro servicio]()
  - [125. Localstorage - Hacer persistente la información]()
  - [126. Funcionalidad de la pantalla para agregar tareas a la lista]()
  - [127. Detalles estéticos de la pantalla agregar]()
  - [128. Eliminar items de la lista de deseos]()
  - [129. Editar elementos de otras listas]()
  - [130. Módulo de componentes y listas component]()
  - [131. Componente listas]()
  - [132. Eliminar una lista]()
  - [133. Pipes impuros]()
  - [134. Editar el título de la lista]()
  - [135. Código fuente de la sección]()

# Sección 1:Introducción al curso de Angular

## 5. Instalaciones y configuraciones mínimas necesarias.

**Instalaciones necesarias para el curso**

1. NodeJS: https://nodejs.org/es/
2. Google Chrome:
https://www.google.es/chrome/browser/desktop/
3. TypeScript:
http://www.typescriptlang.org/
4. Angular CLI
https://cli.angular.io/
https://github.com/angular/angular-cli
5. Ionic
http://ionicframework.com/getting-started/

**Editores de Texto:**

Atom
https://atom.io/

Visual Studio Code
https://code.visualstudio.com/

**Plugins recomendados para los siguientes editores:**

**ATOM**

* Angular 2 Type Script Snippets
* Atom Bootstrap3
* Atom Typescript
* File Icons
* Platformio Ide Terminal
* V Bootstrap4

**Visual Studio Code**

* Angular 2 TypeScript Emmet
* Angular 5 Snippets – TypeScript, Html, Angular Material...
* Angular Language Service
* Angular v5 Snippets
* Angular2-inline
* Bootstrap 4 & Font Awesome snippets
* HTML CSS Support
* JavaScript (ES6) code snippets
* JS-CSS-HTML Formatter
* JSHint
* Material Icon Theme
* Prettier – Code Formatter
* Terminal
* TSLint
* TypeScript Hero
* TypeScript Importer

# Sección 2:Gentil introducción a TypeScript y ES6

## 10. Demostración de TypeScript

Explicación de cómo es un lenguaje tipado, para empezar renombramos el archivo app.js a app.ts

Ahora nos muestra errores, la función saludar requería un argumento, que ahora le podemos pasar.

Cuando definimos el argumento a pasar en la función como tipo string y le pasamos el atributo de tipo string del objeto constante nos da un error:

>Uncaught SyntaxError: Unexpected token ':'

Esto es porque el navegador no soporta cargar directamente un archivo tipo .ts que es lo que le estamos indicando en el &lt;script> del html

Para compilar el archivo ts en un archivo js *tsc: typescript compiler* : 

>tsc --version

>Version 3.7.5

>tsc app.ts

[Volver al Índice](#%C3%ADndice-del-curso)

## 11. Configuración de TypeScript

Crear un archivo de configuración:

>tsc --init

Esto genera un archivo tsconfig.json

>tsc -w 

Esto hace que entre en modo observador, pendiente de recompilar cuando haya cambios en el archivo ts

[Volver al Índice](#%C3%ADndice-del-curso)

## 12. Variables let y const

Dentro de un scope (entre llaves), cuando las variables se declaran con *let* tienen validez dentro de ese scope, cuando se declaran dos variables *let* con el mismo nombre, al compilarlas a js se renombran, de tal manera que no hay conflicto.

Por otro lado, se puede declarar un valor como constante, *const*, pero entonces no se le podrá asignar otro valor, la convención es declarar los nombres todos en mayúsculas.

[Volver al Índice](#%C3%ADndice-del-curso)

## 13. Introducción a los tipos de datos

>let mensaje: string = "Hola";

>let numero: number = 123;

>let booleano: boolean = true;

>let hoy: Date = new Date();

>let cualquiercosa;

>//let cualquiercosa: string | number; <-- asignar varios tipos a una misma variable con un pipe

>cualquiercosa = mensaje;

>cualquiercosa = numero;

>cualquiercosa = booleano;

>cualquiercosa = hoy;

```
let spiderman = {
    nombre: 'Peter',
    edad: 30
};
spiderman = {
    nombre: 'Juan',
    edad: 40
};
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 14. Excluir archivos a traducir

Exclusión de archivos a traducir de TS a JS

Creamos un nuevo directorio "typescript" y hacemos una copia del archivo app.ts, renombramos el archivo copiado a tipos.ts

Si entramos en modo observador "tsc -w" también encontrará ese directorio y lo compilará. Pero no lo deseo, porque quiero tipos.ts sólo para mis propias notas.

Vamos a tsconfig.json y añadimos la línea: "exclude": ["demo-typescript/typescript"],

[Volver al Índice](#%C3%ADndice-del-curso)

## 15. Template literales del ES6

```
(function(){

    function getEdad() {
        return 100 + 100 + 300;
    }
    const nombre = "Eduardo";
    const apellido = "Córdoba";
    const edad = 38;
    // const salida = nombre + apellido + edad;
    // const salida = nombre + " " + apellido + " ( " + edad + " ) ";
    // Eduardo Córdoba (Edad: 38)
    const salida = `${ nombre } \n${ apellido } \n( ${ edad + 100 } ) funcion edad= ${getEdad()}`;
    console.log(salida);

})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 16. Funciones: Parámetros opcionales, obligatorios y por defecto

```
//Función auto-invocada para evitar errores de variables reutilizadas
(function(){
    //quien = argumento obligatorio
    //momento = argumento opcional
    //objeto = argumento por defecto (inicializado con un valor por defecto)
    // El valor por defecto debería ser el último argumento
    function activar( quien: string,
                      momento?: string,
                      objeto:string = 'batiseñal'
                      ) {
        if ( momento ) {
            console.log(`${ quien } activó la ${ objeto } en la ${ momento }`);
        } else {
            console.log(`${ quien } activó la ${ objeto }`);
        }

    }
    activar('Gordon','tarde');
})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 17. Funciones de Flecha

```
//Función auto-invocada para evitar errores de variables reutilizadas
(function(){

    // Si declaramos la función en una constante no podremos sobreescribir la declaración de la función
    const miFuncion = function( a: string) {
        return a.toUpperCase();
    }
    //La misma función en versión "Función de flecha", si sólamente se va a retornar una línea de código se pueden ahorrar las llaves y el return
    const miFuncionF = ( a: string ) =>  a.toUpperCase();


    const sumarN = function( a:number, b:number) {
        return a + b;
    }
    const sumarF = ( a:number, b:number ) => a + b;
    console.log( sumarN (8,3) );
    console.log( sumarF (5,5) );

    const hulk = {
        nombre: 'Hulk',
        smash() {
            //setTimeout para ejecutar una función tras un tiempo(en milésimas de segundo)
            //Las funciones de flecha (setTimeout( ()=>)) no modifican a lo que apunta "this", this apunta a "const hulk"
            setTimeout( ()=> {

                console.log(`${ this.nombre } smash!!!`);

            }, 1000);
        }
    }
    //console.log( miFuncion('Normal') );
    //console.log( miFuncionF('Flecha') );

    hulk.smash();
})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 18. Desestructuración de Objetos y Arreglos (arrays)

```
//Función auto-invocada de flecha para evitar errores de variables reutilizadas
(()=>{
    //objeto
    const avenger = {
       nombre: 'Steve',
       clave: 'Capitan América',
       poder: 'Droga'
    }
    //extraer propiedades
    /* console.log( avenger.nombre );
    console.log( avenger.clave );
    console.log( avenger.poder ); */
    //coger el objeto avenger y extraer todas sus propiedades, en este tipo de desestructuración no importa el orden
   /*  const {nombre, poder} = avenger;
    console.log( nombre );
    console.log( poder ); */
    //También funciona en los argumentos de una función
    /* const extraer = ( avenger: any ) => {
        const {nombre, poder} = avenger;
        console.log( nombre );
        console.log( poder );
    }
    extraer ( avenger ); */
    //Podemos pasarle directamente las propiedades extraídas
    const extraer = ( { nombre, poder }: any ) => {
        //const {nombre, poder} = avenger;
        console.log( nombre );
        console.log( poder );
    }
    //extraer ( avenger );
    //Desestructuración de arrays


    //Sin desestructurar:
    const avengers: string[] = ['Thor','Ironman','Spiderman'];
    console.log( avengers[0] );
    console.log( avengers[1] );
    console.log( avengers[2] );
    //Con desestructuración: puedo definir las variables en orden de suposición, con un objeto no es necesario, pero con un array si
    // const [ loki, hombre, arana] = avengers;
    //Si solo queremos un valor dejamos el resto vacío separados con comas
    //const [ , , arana] = avengers;
    /* console.log( loki );
    console.log( hombre ); */
    //console.log( arana );
    const extraerArr = ( [thor,ironman,spiderman]: string[]) => {
            console.log( thor );
            console.log( ironman );
            console.log( spiderman );
    }
    extraerArr(avengers);
})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 19. Promesas

```
//Función auto-invocada de flecha para evitar errores de variables reutilizadas
(()=>{
//Las promesas, básicamente, nos sirven para ejecutar un código sin bloquear el código de la aplicación

    console.log('Inicio');
//Si definimos la promesa así nos da un error porque le falta una libreria, así que en el tsconfig.json tenemos que cambiar de es5 al es6
//'Promise' only refers to a type, but is being used as a value here. Do you need to change your target library? Try changing the `lib` compiler option to es2015 or later.ts(2585)
//Dentro de la promesa necesito mandar una función que lleve dos argumentos, dentro de esos argumentos necesito mandar dos funciones
//Una que se va a llamar usualmente "resolve", que es lo que vamos a retornar cuando todo funciona correctamente
//Y otra que se va a llamar "reject", que llamaremos si sucede algún error
    const prom1 = new Promise( ( resolve, reject) => {

        setTimeout(() => {
            //resolve('Se terminó el timeout');
            //Si ejecutamos el código aparecerá el console.log inicio, luego ejecutará la promesa, pero como tiene un retraso de 1 segundo no interrumpe el código, ejecuta el console.log de fin y luego aparece el mensaje de prom1
            reject('Se terminó el timeout');
            //Si ejecutamos el código aparecerá el console.log inicio, luego ejecutará la promesa, pero como tiene un retraso de 1 segundo no interrumpe el código, ejecuta el console.log de fin y luego aparece el error (sin atrapar, sin catch) de prom1
        }, 1000);

    });
    //Llamamos a la promesa prom1, tenemos tres opciones: Symbol, catch y then, Symbol es un identificador
    //Then es lo que yo quiero ejecutar cuando se realiza todo exitosamente y catch cuando sucede un error
    prom1
        .then( mensaje => console.log( mensaje ))
        .catch( err => console.warn(err));//console.warn se muestra en amarillo
    console.log('Fin');
})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 20. Promesas y su tipado en TypeScript
```
//Función auto-invocada de flecha para evitar errores de variables reutilizadas
(()=>{
    //Para definir el tipo (tipado) de retorno de las promesas, vamos a definirlo tras los argumentos, indicando entre <> que tipo si hace el resolve (correcto)
    //:Promise<number>
    //
    const retirarDinero = ( montoRetirar: number): Promise<number> => {
        //Suponiendo que la cantidad de dinero esté en un backend, en un servidor, y se tiene que retirar de manera asíncrona, porque tengo que verificar
        //si en el servidor tengo esa cantidad de dinero, entonces la tarea se podría retrasar unos segundos
        let dineroActual = 1000;
        //Retirar dinero devolverá una promesa
        return new Promise( (resolve,reject) =>{

            if ( montoRetirar > dineroActual ) {
                reject('No hay suficientes fondos');
            } else {
                //dineroActual = dineroActual - montoRetirar; Siguiente línea version corta de esta línea de código
                dineroActual -= montoRetirar;
                //Una vez hecho el cálculo hago el resolve, no es obligatorio retornar algo, pero en este caso vamos a retornar el monto actual
                resolve( dineroActual );
            }
        } );
    }

    //Para poder manejar la promesa tenemos que especificar en la función el then o el catch
    retirarDinero( 500 )
        .then( montoActual => console.log(`Me queda ${ montoActual}`))
        //.catch( err => console.warn (err)); a continuación versión corta, porque solo estoy usando esa variable ahí
        .catch ( console.warn )
})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 21. Interfaces de TypeScript

```
//Función auto-invocada de flecha para evitar errores de variables reutilizadas
(()=>{

    //Interfaces: Reglas que queremos que cumpla un objeto para que pueda usarse como un tipo
    //Es como crear un tipo de dato "tonto", no se pueden definir la funcionalidad de los métodos internamente
    //Son manera de definir reglas para que nosotros u otros compañeros sigan si quieren usar un tipo

    interface Xmen {//no hay código equivalente en js, cuando se compila no aparece nada en el .js, son puramente para ts
        nombre: string;
        edad: number;
        poder?: string;//Declarando la variable con ?: especificamos que esta variable es opcional
    }
    const enviarMision = ( xmen: Xmen ) => {//definido como tipo de interfaz customizada anteriormente

        console.log(`Enviando a ${ xmen.nombre } a la misión`);

    }

    const regresarAlCuartel = ( xmen: Xmen ) => {

        console.log(`Enviando a ${ xmen.nombre } a la misión`);

    }

    const wolverine: Xmen = {//Definiendo el interfaz a la hora de declarar el objeto me aseguro que se cumplan las condiciones de tipado
        nombre: 'Logan',
        edad: 60
    }
    enviarMision( wolverine );
    regresarAlCuartel( wolverine );

})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 22. Introducción a las Clases de la POO

>Controlado, pasando lección.

[Volver al Índice](#%C3%ADndice-del-curso)

## 23. Definición de una clase básica en TypeScript

```
//Función auto-invocada de flecha para evitar errores de variables reutilizadas
(()=>{

    //Convención de nombres de clases, capitalizar palabras
    class Avenger {
        //Nombre: tipo
        nombre: string = "Sin nombre";
        equipo: string;
        nombreReal: string; //lowercamelcase

        puedePelear: boolean;
        peleasGanadas: number;

    }

    const antman = new Avenger();

    console.log(antman);
})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 24. Constructores de una clase en TypeScript
```
//Función auto-invocada de flecha para evitar errores de variables reutilizadas
(()=>{

    //Convención de nombres de clases, capitalizar palabras
    class Avenger {
        //Nombre: tipo
       /*  nombre: string;
        equipo: string;
        nombreReal: string; //lowercamelcase

        puedePelear: boolean;
        peleasGanadas: number; */

        //Constructor: función que se ejecuta cuando se crea una nueva instancia de una clase
        /* constructor( nombre: string , equipo: string){
            this.nombre = nombre; //this apunta a la propiedad nombre de la clase Avenger, y será igual al valor que se le pasa al constructor
            this.equipo = equipo;

        } */
        constructor( public nombre: string,
                     public equipo: string,
                     public nombreReal?: string,
                     public puedePelear: boolean = true,
                     public peleasGanadas: number = 0){}//public: acceso desde la clase o fuera de la misma

    }

    const antman = new Avenger('Antman','Capi');

    console.log(antman);
})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 25. Importaciones * URL

Descargamos el código de ejemplo de la lección: https://github.com/Klerith/webpack-starter-typescript

Dentro del directorio ejecutamos "npm install"

Posteriormente ejecutamos el index.ts con "npm start"

Creamos dentro del directorio src un nuevo directorio llamado "classes" o un nombre a nuestra elección, dentro de él creamos un archivo llamado "xmen.class.ts", el nombre .class. es opcional pero es una convencción opcional (alguna gente lo considera redundante)

Básicamente, para exportar una clase(xmen.class.ts):

```
export class Xmen {
    constructor(
        public nombre: string,
        public clave : string
    ){}

    imprimir() {
        console.log(`${ this.nombre } - ${ this.clave}`);
    }
}
```
Para importarla (index.ts):

```
import { Xmen } from './classes/xmen.class';

const wolverine = new Xmen('Logan','Wolverine');

wolverine.imprimir();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 26. Decoradores de Clases
```
function imprimirConsola( constructorClase: Function) {
    console.log( constructorClase );
}//Para recibir el constructor de la función, o firma de la clase
//Un decorador es algo que se le va a colocar (en este caso decorador de clase) antes de la definición de la clase
@imprimirConsola //<-- Decorador, tenemos que configurar en el tsconfig.json para que lo acepte: "experimentalDecorators": true
//El decorador permite añadir funcionalidades a la clase. Una función que se ejecuta en las clases, expandiendo su funcionalidad
export class Xmen {
    constructor(
        public nombre: string,
        public clave : string
    ){}

    imprimir() {
        console.log(`${ this.nombre } - ${ this.clave}`);
    }
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 27. Tipado del retorno de una función
```
//Función auto-invocada de flecha para evitar errores de variables reutilizadas
(()=>{

    const sumar = (a: number, b: number): number =>  a + b;   
    
    const nombre = (): string => 'Hola Fernando';

    const obtenerSalario = ():Promise<string> => {

        return new Promise ( (resolve, reject) => {
            resolve('Fernando')
        });
    }

    obtenerSalario().then( a => console.log( a.toUpperCase() ) )
})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 28. Exámen práctico #1

Instrucciones
Descargue el material adjunto, el cual es un archivo de TypeScript.

Se requiere que sea lo más especifico posible en cuanto a los tipos y uso de lo aprendido en esta sección (como es el ES6)

Sea lo más explícito posible y siga las instrucciones que se encuentran en los comentarios del mismo.

Por si acaso tienen problemas con la descarga (por alguna razón) Les dejo aquí el código

Lean todos los comentarios y vean el siguiente video

[Volver al Índice](#%C3%ADndice-del-curso)

## 29. Explicación de la tarea

> Ver vídeo

[Volver al Índice](#%C3%ADndice-del-curso)

## 30. Resolución del examen práctico #1

```
(()=>{

  // Uso de Let y Const
  const nombre = 'Ricardo Tapia';
  const edad = 23;

  /* const PERSONAJE = {
    nombre: nombre,
    edad: edad
  };Si una propiedad tiene el mismo valor que una variable (nombre:nombre), como este caso, se puede definir como a continuación*/
  const PERSONAJE = { nombre, edad };


  // Cree una interfaz que sirva para validar el siguiente objeto
  interface Batman  {
    nombre: string;
    artesMarciales: string[];
  }

  const batman: Batman = {
    nombre: 'Bruno Díaz',
    artesMarciales: ['Karate','Aikido','Wing Chun','Jiu-Jitsu']
  }


  // Convertir esta funcion a una funcion de flecha
  /* function resultadoDoble( a, b ){
    return (a + b) * 2
  } */
  const resultadoDoble = ( a: number, b: number):number => {
    return (a + b) *2
  }


  // Función con parametros obligatorios, opcionales y por defecto
  // donde NOMBRE = obligatorio
  //       PODER  = opcional
  //       ARMA   = por defecto = 'arco'
  function getAvenger( nombre:string, poder?:string, arma:string = 'arco' ){
    let mensaje;
    if( poder ){
      mensaje = nombre + ' tiene el poder de: ' + poder + ' y un arma: ' + arma;
    }else{
      mensaje = nombre + ' tiene un ' + poder
    }
  };

  // Cree una clase que permita manejar la siguiente estructura
  // La clase se debe de llamar rectangulo,
  // debe de tener dos propiedades:
  //   * base
  //   * altura
  // También un método que calcule el área  =  base * altura,
  // ese método debe de retornar un numero.
class Rectangulo {
  constructor ( public base:number,
                public altura:number ){}

  calcularArea = ():number => this.base * this.altura;

}
})();
```

[Volver al Índice](#%C3%ADndice-del-curso)

## Cuestionario 1: Examen teórico #1

> Resuelto / Aprobadas 9 de 10

[Volver al Índice](#%C3%ADndice-del-curso)

## 31. Código fuente de la sección

> Descargar typescript.zip

[Volver al Índice](#%C3%ADndice-del-curso)

# Sección 3:Aplicación #1: Hola Mundo

## 35. Introducción a los componentes y directivas estructurales.

- Componentes: Pequeñas clases que cumplen una tarea específica, por ejemplo: Menú de navegación, Barra Lateral, Páginas, sub-páginas...footer...

- Directivas estructurales: Instrucciones que indical al HTML qué hacer, *ngIf por ejemplo para mostrar o no contenedores, etc ... *ngFor para hacer repeticiones de elementos HTML

[Volver al Índice](#%C3%ADndice-del-curso)

## 36. Nuestra primera interacción en Angular

> Visitar https://angular.io

- Cuando buscamos en la web nos aparecen informaciones con un icono delante que representa:
- Pk: Package
- C: Clase
- I: Interfaz
- K: Constante
- F: Función
- E: Enumeración
- P: Pipe

> Visitar https://plnkr.co/ Que se usará para códigos de demostración
> También https://stackblitz.com/

Usando este último crearemos un nuevo proyecto de Angular para testeo.

[Volver al Índice](#%C3%ADndice-del-curso)

## 37. Nota de Actualización del Angular CLI

> Al hacer ng new miApp ahora Angular CLI hace dos preguntas:

- Would you like to add Angular routing? No -> Nosotros haremos nuestras propias rutas
- Which stylesheet format would you like to use? CSS (a elegir entre CSS, SCSS, SASS, LESS, Stylus)

[Volver al Índice](#%C3%ADndice-del-curso)

## 38. Creando un entorno local de Angular

- https://angular.io/guide/quickstart
- Angular CLI
> ng new my-app
> ng serve -o 

- app.compontent.html:
```
<h1>Hola Mundo</h1>

<ul>
    <li>Nombre: {{ nombre }}</li>
    <li>Apellido: {{ apellido }}</li>
</ul>
```
- app.component.ts:
```
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nombre = 'Fernando';
  apellido = 'Herrera';
}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 39. Estructura del proyecto

- e2e: Test end to end, pruebas unitarias, integración, pruebas automáticas
- node_modules: módulos de node.js
- src: aplicación de Angular
    - app
        - Primera aplicacion de angular
            - app.component.ts: primer componente
            - app.component.css: archivo de estilo que sólo se aplica a este componente
            - app.component.html: html del componente
            - app.component.spec.ts: Archivo de pruebas automáticas (todo archivo "spec")
            - app.module.ts.
    - assets: Recursos estáticos, imágenes, etc...
    - enviroments:
        - enviroment.prod.ts: Variable de ambiente de producción
        - enviroment.ts: Variable de ambiente de desarrollo
    - browserslist: lista de navegadores para consolidación de CSS
    - karma.conf.js: archivo de configuración de las pruebas de karma
    - main.js: Primer código que Angular ejecutará para lanzar la aplicación
        - platformBrowserDynamic(): Es una función que configura todo el ambiente para una aplicación web
    - polyfills.ts: Funciones que ayudan a la compatibilidad entre versiones antiguas de navegadores web
    - styles.css: CSS global de la aplicación
    - index.html: Toda la aplicación se renderiza en <app-root>
    - tsconfig.app.json: Especificaciones propias de la aplicacion de typescript
    - tsconfig.spec.json: Configuración para las pruebas
    - tslint.json: formas para la presentación de errores de código
- .editorconfig: configuración del editor
- .gitignore: ignorar archivos para el repositorio
- angular.json: configuración de la aplicación
- package-lock.json: nos dice como fué creado package.json
- package.json: dependencias de producción, de desarrollo
- README.md: cómo funciona la aplicación, archivo Markdown
- tsconfig.json: le dice a typescript cómo trabajar
- tslint.json: reglas de escritura

[Volver al Índice](#%C3%ADndice-del-curso)

## 40. Utilizando Bootstrap 4

> https://getbootstrap.com/ -> Download
```
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

```

- Creación manual de un componente:

    - app/components/header/header.component.ts

- Indicar a la aplicación la creación del componente:
    - app/app.module.ts

- app.module.ts:
```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
@NgModule({
  declarations: [// Declarar componentes
    AppComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

```
- app.component.html
```
<app-header></app-header>

<ul>
    <li>Nombre: {{ nombre }}</li>
    <li>Apellido: {{ apellido }}</li>
</ul>
```
- header.component.ts
```
import { Component } from '@angular/core';

/* Declaramos decorador para indicar que la clase HeaderComponen instanciará un componente */
@Component({
    selector: 'app-header', // Para llamar al componente mediante etiqueta en el html
    template: `<h1>Header component</h1>` // HTML que se mostrará, o bien código directamente si es poco, o bien llamada a la plantilla HTML
})
export class HeaderComponent { // indicamos export para poder llamarlo en app.module.ts

}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 41. TemplateUrl: Separando el HTML del componente

Vamos a https://getbootstrap.com/ -> Buscamos el navbar

Copiamos el código HTML del navbar y lo incluimos en el template del componente en header.component.ts

```
import { Component } from '@angular/core';

/* Declaramos decorador para indicar que la clase HeaderComponen instanciará un componente */
@Component({
    selector: 'app-header', // Para llamar al componente mediante etiqueta en el html
    template: `
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
    `
})
export class HeaderComponent { // indicamos export para poder llamarlo en app.module.ts

}
```

tslint nos avisará de que incluir todo ese código directamente excede el máximo permitido, esto se puede ajustar cambiando el parámetro max-line-length en tslint.json

Podemos tanto cambiar el máximo permitido, como desactivar el warning poniendo la propiedad en "false"

```
"max-line-length": [
      false,
      140
    ],
```
De todas maneras, como hemos comentado, cuando en template el código supera unas cuatro líneas empieza a no ser manejable, así que es recomendable crear un archivo template aparte:

> app/components/header/header.component.html

Aquí copiamos todo el código HTML del navbar

En el decorador del componente tenemos que eliminar el código que teníamos en template, y renombrar template como templateUrl para especificar la ruta a la plantilla html que hemos creado

Crearemos también un body de la misma manera, a la misma etiqueta html de componente podemos añadirle estilos, de esta manera

><app-body class="container"></app-body>

Recordar añadir los componentes al módulo de app (app.module.ts):

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { BodyComponent } from './components/body/body.component';
@NgModule({
  declarations: [// Declarar componentes
    AppComponent,
    HeaderComponent,
    BodyComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 42. Creando el footer.component

En este caso vamos a usar angularCLI para crear el componente

>ng g c components/footer

Esto es la abreviatura de:

>ng generate component components/footer
```
$ ng g c components/footer
CREATE src/app/components/footer/footer.component.html (21 bytes)
CREATE src/app/components/footer/footer.component.spec.ts (628 bytes)
CREATE src/app/components/footer/footer.component.ts (275 bytes)
CREATE src/app/components/footer/footer.component.css (0 bytes)
UPDATE src/app/app.module.ts (627 bytes)
```

Una vez generado ya tenemos todos los archivos, incluidas las importaciones en app.module.ts

Entonces podemos añadir código al footer.component.html:

```
<footer class="footer bg-dark text-center">
    <div class="container">
        <p>
            &copy; Eduardo Córdoba 2020
        </p>
    </div>
</footer>
```

Y llamar al componente en app.component.html:

```
<app-header></app-header>

<app-body class="container"></app-body>

<app-footer></app-footer>
```

Para posicionar el componente abajo en la web, usamos estilos CSS, al ser un estilo común es recomendable hacerlo en src/styles.css

```
footer {
  color: white;
  position: fixed;
  bottom: 0;
}
```

Vamos a hacer que en el footer se visualice el año actual automáticamente, para eso usaremos la función Date() de JavaScript, declaramos la variable anio y en el constructor la inicializamos con la función que devuelve el año actual, de tal manera que cuando se cargue la aplicación y este componente, en el HTML aparecerá ese valor, será de la siguiente manera, en footer.component.ts:

```
import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  anio: number;

  constructor() {
    this.anio = new Date().getFullYear();
  }

}
```

En footer.component.html:

```
<footer class="footer bg-dark text-center">
    <div class="container">
        <p>
            &copy; {{ anio }} Eduardo Córdoba
        </p>
    </div>
</footer>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 43. Estructura del body component

Vamos a https://getbootstrap.com/ -> Buscamos cards y cogemos un código tipo este:

```
<div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h5 class="card-title">Primary card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
```

Vamos a https://getbootstrap.com/ -> Buscamos list group y cogemos un código tipo este:

```
<ul class="list-group">
            <li class="list-group-item">Cras justo odio</li>
            <li class="list-group-item">Dapibus ac facilisis in</li>
            <li class="list-group-item">Morbi leo risus</li>
            <li class="list-group-item">Porta ac consectetur ac</li>
            <li class="list-group-item">Vestibulum at eros</li>
        </ul>
```

Modificamos y ajustamos el estilo del app.component.html para que reciba los nuevos elementos, algo así:

```
<app-header></app-header>

<div class="container mt-5">
    <app-body></app-body>
</div>

<app-footer></app-footer>
```

Y quedaría el body.component.html tal que:

```
<div class="row">
    <div class="col">
        <h1>*ngIf</h1>
        <hr>
        <div class="card text-white bg-primary mb-3" style="width: 100%;">
            <div class="card-body">
                <h5 class="card-title">Primary card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>
        <button class="btn btn-outline-primary btn-block">Mostrar/Ocultar</button>
    </div>
    <div class="col">
        <h1>*ngFor</h1>
        <hr>
        <ul class="list-group">
            <li class="list-group-item">Cras justo odio</li>
            <li class="list-group-item">Dapibus ac facilisis in</li>
            <li class="list-group-item">Morbi leo risus</li>
            <li class="list-group-item">Porta ac consectetur ac</li>
            <li class="list-group-item">Vestibulum at eros</li>
        </ul>
    </div>
</div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 44. Directivas estructurales: *ngFor y el *ngIf

Vamos a usar estas directivas para generar dinámicamente código HTML en nuestros elementos del body component

Creamos un objeto con dos atributos tipo string:

```
export class BodyComponent { // indicamos export para poder llamarlo en app.module.ts

    frase: any = {
        mensaje: 'Un gran poder requiere una gran responsabilidad',
        autor: 'Ben Parker'
    };
}
```

Y luego las llamamos en el html tipo {{ objeto.atributo }}

```
<div class="card-body">
                <h5 class="card-title"> {{ frase.autor }} </h5>
                <p class="card-text"> {{ frase.mensaje }} </p>
            </div>
```

Ahora usaremos *ngIf para mostrar u ocultar la tarjeta, para ello declaramos una variable booleana que haga de bandera en el componente:

```
export class BodyComponent { // indicamos export para poder llamarlo en app.module.ts

    mostrar = true;
```

Y añadimos la condición *ngIf al elemento que queremos manipular, además de en el boton añadir un evento (click) que vaya alternando el valor de la bandera de true a false y viceversa:

```
 <div *ngIf="mostrar" class="card text-white bg-primary mb-3" style="width: 100%;">
            <div class="card-body">
                <h5 class="card-title"> {{ frase.autor }} </h5>
                <p class="card-text"> {{ frase.mensaje }} </p>
            </div>
        </div>
        <button (click)="mostrar = !mostrar" class="btn btn-outline-primary btn-block">Mostrar/Ocultar</button>
```

Y ahora para usar el *ngFor y generar la lista dinámicamente primero creamos un array de string en el componente:

```
export class BodyComponent { // indicamos export para poder llamarlo en app.module.ts

    mostrar = true;
    frase: any = {
        mensaje: 'Un gran poder requiere una gran responsabilidad',
        autor: 'Ben Parker'
    };

    personajes: string[] = ['Spiderman', 'Venom', 'Dr. Octopus'];
}
```

Y posteriormente lo llamamos, aprovechamos tambien para guardar el index y usarlo para puntuar numéricamente la lista:

```
<ul class="list-group">
            <li *ngFor="let personaje of personajes; let i = index" class="list-group-item">
                {{ i +1 }} - {{ personaje }}
            </li>
        </ul>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## Cuestionario 2: Examen teórico - de la sección Hola Mundo

10 de 10 !!!

[Volver al Índice](#%C3%ADndice-del-curso)

# Sección 4:Aplicación #2: Aplicación de una sola página (SPA)

¿Qué veremos en esta sección?

A continuación vamos a aprender sobre los siguientes temas:

- Crearemos una aplicación de una sola página (Single Page Application)
- Creación de proyectos de Angular usando el CLI (Command Line Interface)
- Instalando bootstrap o librerías de terceros usando el Angular-CLI
- Creación de rutas de nuestra aplicación
- Uso de RouterLink y RouterLinkActive para movernos de página y colocar clases a los elementos activos.
- Uso del modulo Router, que nos permite movernos de página mediante código.
- Obtención de parámetros vía URL.
- Configuración de nuestro primer servicio en Angular para el manejo de la data.
- Breve introducción a los Pipes 
- Uso del buscador del navbar para realizar una consulta a nuestro arreglo de héroes.
- Durante la sección, tendremos una tarea práctica bastante retadora pero servirá de reforzamiento de todo lo que veremos en esta sección.

[Volver al Índice](#%C3%ADndice-del-curso)

## 49. Iniciar el proyecto - SPA

Sencillamente creamos el nuevo proyecto con:

>ng new spa

Para luego renombrar la carpeta "spa" a 02-spa, y entrando en ella lanzar el proyecto con:

>ng serve -o

[Volver al Índice](#%C3%ADndice-del-curso)

## 50. Creando la estructura de nuestro proyecto

Básicamente creamos dos componentes para empezar, uno para la home y otro que sea un navbar que se encuentre en un subdirectorio de componentes compartidos (comunes) a la SPA

>ng g c components/shared/navbar

>ng g c components/home

Y limpiamos cosas que no queremos, archivos css y spec

[Volver al Índice](#%C3%ADndice-del-curso)

## 51. Instalando el bootstrap (u otras liberías de terceros) cuando usamos el Angular-CLI

1. Instalando el Boostrap CDN (requiere internet)(normalmente ya se encontrará en el caché del navegador del cliente)

```
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
```
```
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
```

Los scripts deberemos colocarlos al final del body, y el link al final del head

2. Instalación local: Descarga directa de bootstrap

https://getbootstrap.com/docs/4.5/getting-started/download/

Creamos en nuestro proyecto la ruta src/assets/libs/dist y dentro copiamos los directorios css y js que hemos descargado

Puede tener la formación de rutas que sea, pero que esté dentro de assets

Se debería repetir esto con todas las librerias que se refirieron en el punto 1, y luego referenciarlas con rutas relativas tipo:

```<link rel="stylesheet" href="./assets/libs/bootstrap/css/bootstrap.min.css">```

3. Instalación con el Gestor de Paquetes de Node.js (npm)

>npm install bootstrap --save

Una vez terminado tendremos bootstrap dentro de node_modules

Hacer lo mismo para jquery y popper.js

>npm install jquery --save

>npm install popper.js --save

(Nota: Esto puede haber cambiado en las últimas versiones de Angular.....)

Ahora habría que especificar en angular.json donde está el css de bootstrap y las librerías javascript a utilizar:

```
                        "styles": [
                            "src/styles.css",
                            "node_modules/bootstrap/dist/css/bootstrap.min.css"
                        ],
                        "scripts": [
                            "node_modules/jquery/dist/jquery.slim.min.js",
                            "node_modules/popper.js/dist/umd/popper.min.js",
                            "node_modules/bootstrap/dist/js/bootstrap.min.js"
                        ]
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 52. Configurando el navbar y otros componentes

Copiamos los archivos de imágenes del curso en assets/img

Copiamos el favicon.ico en src/

Usamos template de bootstrap para definir el navbar.component.html:

```
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">
        <img src="assets/img/A-64.png" width="30" height="30" alt="" loading="lazy">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
```

Usamos template de bootstrap para definir el home.component.html:

```
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Comic App</h1>
        <p class="lead">Esta es una aplicación de comics.</p>
    </div>
</div>
```

Llamamos a los componentes en el app.component.html

```
<app-navbar></app-navbar>
<app-home></app-home>
```

Instalamos un par de componentes más: about y heroes, este ultimo lo vamos a crear sin archivo de estilos "-is" (inline style)

> ng g c components/about

> ng g c components/heroes -is

[Volver al Índice](#%C3%ADndice-del-curso)

## 53. Rutas en Angular

Las rutas nos permiten navegar por los diferentes componentes (páginas) sin hacer refresh del navegador

Creamos un nuevo archivo en src/app/app.routes.ts (el nombre es una convención), tiene una sintaxis particular:

si tenemos snippets instalados con escribir ng2routes y pulsar enter se nos genera el código, sino tendríamos que escribir:

```
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

const APP_ROUTES: Routes = [ // Array de rutas, cada ruta tiene un path y un componente
    { path: 'home', component: HomeComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' } // La ruta ** es una ruta especial por si no consigue hacer match con ninguna de las otras
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
// Si enviamos parámetros por url y hacemos refresh puede fallar, entonces usaríamos el sistema de hash en la URL
// Para usar el sistema de hash en la URL sería:
// export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true});
// De otro modo tendríamos que indicar en el servidor, con .htaccess por ejemplo, que redirija a la raíz.
// De manera automática nos configuró angular-cli en el index.html <base href="/"> necesario si no vamos a usar hash.
```

Para indicar a Angular las rutas que tiene que usar, tenemos que definirlo en app.module.ts:

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// Rutas
import { APP_ROUTING } from './app.routes'; // Importamos el sistema de rutas
// Servicios

// Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { HeroesComponent } from './components/heroes/heroes.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    HeroesComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING // Añadimos el routing a los imports
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 54. RouterLink y RouterLinkActive - Completando las rutas

Y en nuestro html los enlaces quedarán así en navbar.component.html:
```
<div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <!-- Class active para marcar enlace activo -->
            <li class="nav-item">
                <a class="nav-link" [routerLink]="['home']">Home</a> <!-- Para definir la ruta usarmos router link en lugar de href de html
                usa un array como parámetro, cada uno de los elementos del array serían las sub-rutas de la url -->
            </li>
            <li class="nav-item">
                <a class="nav-link" [routerLink]="['heroes']">Heroes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" [routerLink]="['about']">About</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
```

Hemos añadido algunos efectos de transición en el archivo styles.css

También añadimos la clase especial <li class ="nav-item" routerLinkActive="active"> para asignar los enlaces activos.

Como el <a> tiene un routerLink mira el elemento padre y al detecar routerLinkActive aplica la clase "active" (podría ser otra)

[Volver al Índice](#%C3%ADndice-del-curso)

## 55. Componente Heroes - diseño

Para la sección heroes usaremos cards de Bootstrap, de momento usaremos una ruta estática, pero en la siguiente lección usaremos servicios:

```
<h1>Héroes <small>Marvel y DC</small></h1>
<hr>

<div class="card-columns">
    <div class="card">
        <img src="assets/img/aquaman.png" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
          <button type="button" class="btn btn-outline-primary btn-block">
              Ver más...
          </button>
        </div>
    </div>
</div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 56. Introducción a los Servicios

Los servicios:

- Brindan información a quien lo necesite
- Realizan peticiones CRUD (create, read, update, delete)
- Mantienen la data de forma persistente
- Sirven como recurso reutilizable para nuestra aplicación

[Volver al Índice](#%C3%ADndice-del-curso)

## 57. Creando nuestro primer servicio - HeroesService

Creamos el directorio src/app/servicios

Y dentro de él el archivo de servicios, por convención será .service src/app/servicios/heroes.service.ts

Para definir el archivo con los snippets podemos hacer "ng2service", sino pues el código sería:

```

import { Injectable } from '@angular/core';

@Injectable()
export class HeroesService {


    constructor() {
        console.log("Servicio listo para usar!!!");
    }
}
```

Posteriormente le informamos a Angular de que dispone de este servicio en app.module.ts:

```
// Servicios
import { HeroesService } from './servicios/heroes.service';
providers: [
    HeroesService
  ],
```

Ahora para usar nuestro servicio en heroes.components.ts primero lo importamos y después lo llamamos en el constructor para usarlo, posteriormente copiamos el array de objetos de los recursos en un objeto privado "private". Luego creamos un metodo público de acceso a los datos.

```

import { Injectable } from '@angular/core';

@Injectable()
export class HeroesService {

    private heroes:any[] = [
        {
          nombre: "Aquaman",
          bio: "El poder más reconocido de Aquaman es la capacidad telepática para comunicarse con la vida marina, la cual puede convocar a grandes distancias.",
          img: "assets/img/aquaman.png",
          aparicion: "1941-11-01",
          casa:"DC"
        },
        {
          nombre: "Batman",
          bio: "Los rasgos principales de Batman se resumen en «destreza física, habilidades deductivas y obsesión». La mayor parte de las características básicas de los cómics han variado por las diferentes interpretaciones que le han dado al personaje.",
          img: "assets/img/batman.png",
          aparicion: "1939-05-01",
          casa:"DC"
        },
        {
          nombre: "Daredevil",
          bio: "Al haber perdido la vista, los cuatro sentidos restantes de Daredevil fueron aumentados por la radiación a niveles superhumanos, en el accidente que tuvo cuando era niño. A pesar de su ceguera, puede \"ver\" a través de un \"sexto sentido\" que le sirve como un radar similar al de los murciélagos.",
          img: "assets/img/daredevil.png",
          aparicion: "1964-01-01",
          casa: "Marvel"
        },
        {
          nombre: "Hulk",
          bio: "Su principal poder es su capacidad de aumentar su fuerza hasta niveles prácticamente ilimitados a la vez que aumenta su furia. Dependiendo de qué personalidad de Hulk esté al mando en ese momento (el Hulk Banner es el más débil, pero lo compensa con su inteligencia).",
          img: "assets/img/hulk.png",
          aparicion: "1962-05-01",
          casa:"Marvel"
        },
        {
          nombre: "Linterna Verde",
          bio: "Poseedor del anillo de poder que posee la capacidad de crear manifestaciones de luz sólida mediante la utilización del pensamiento. Es alimentado por la Llama Verde (revisada por escritores posteriores como un poder místico llamado Starheart), una llama mágica contenida en dentro de un orbe (el orbe era en realidad un meteorito verde de metal que cayó a la Tierra, el cual encontró un fabricante de lámparas llamado Chang)",
          img: "assets/img/linterna-verde.png",
          aparicion: "1940-06-01",
          casa: "DC"
        },
        {
          nombre: "Spider-Man",
          bio: "Tras ser mordido por una araña radiactiva, obtuvo los siguientes poderes sobrehumanos, una gran fuerza, agilidad, poder trepar por paredes. La fuerza de Spider-Man le permite levantar 10 toneladas o más. Gracias a esta gran fuerza Spider-Man puede realizar saltos íncreibles. Un \"sentido arácnido\", que le permite saber si un peligro se cierne sobre él, antes de que suceda. En ocasiones este puede llevar a Spider-Man al origen del peligro.",
          img: "assets/img/spiderman.png",
          aparicion: "1962-08-01",
          casa: "Marvel"
        },
        {
          nombre: "Wolverine",
          bio: "En el universo ficticio de Marvel, Wolverine posee poderes regenerativos que pueden curar cualquier herida, por mortal que ésta sea, además ese mismo poder hace que sea inmune a cualquier enfermedad existente en la Tierra y algunas extraterrestres . Posee también una fuerza sobrehumana, que si bien no se compara con la de otros superhéroes como Hulk, sí sobrepasa la de cualquier humano.",
          img: "assets/img/wolverine.png",
          aparicion: "1974-11-01",
          casa: "Marvel"
        }
      ];

    constructor() {
        console.log("Servicio listo para usar!!!");
    }

    getHeroes() {
        return this.heroes;
    }
}
```

Posteriormente en el componente de héroes heroes.component.ts inicializamos un array que posteriormente rellenaremos usando el método getHeroes() definido previamente:

```
import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../servicios/heroes.service';
@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {

  heroes:any[] = [];

  constructor( private _heroesService:HeroesService ) { // _heroesService es el alias que tendrá el servicio
    console.log("constructor");
  }

  ngOnInit() {
    this.heroes = this._heroesService.getHeroes();
    console.log( this.heroes);
  }

}
```

Si queremos prevenir que no se incluyesen datos fuera de la estructura del objeto de héroes, podemos crear una interfaz en el servicio, tipo:

```
export interface Heroe {
    nombre: string;
    bio: string;
    img: string;
    aparicion: string;
    casa: string;
};
```
Y entonces podemos declarar el array que contendrá la información como:

```
private heroes:Heroe[] = [
```

Y para poder acceder a todas las clases y métodos del servicio podemos importar la interfaz en heroes.components.ts tal que:

```
import { HeroesService, Heroe } from '../../servicios/heroes.service';

```

Y para ser más específicos aún podemos ahora definir el método que devuelve los héroes de esta manera:

```
getHeroes():Heroe[] {
        return this.heroes;
    }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 58. Página de Heroes - Diseño con *ngFor

Ya que tenemos el array de Héroes guardado como variable local en el componente, podemos generar dinámicamente las tarjetas con esa información.

También podremos llenar información del html con los atributos de los objetos, si por ejemplo queremos src de imagen dinámico, lo definiremos entre []

Quedando el código HTML así:

```
<div class="card-columns">
    <div class="card" *ngFor="let heroe of heroes">
        <img [src]="heroe.img" class="card-img-top" [alt]="heroe.nombre">
        <div class="card-body">
            <h5 class="card-title">{{ heroe.nombre }}</h5>
            <p class="card-text">{{ heroe.bio }}</p>
            <p class="card-text"><small class="text-muted">{{ heroe.aparicion }}</small></p>
            <button type="button" class="btn btn-outline-primary btn-block">
              Ver más...
          </button>
        </div>
    </div>
</div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 59. Rutas con parametros - Router

Navegación a otra página:

Creamos un nuevo componente con angular-cli:

```
ng g c components/heroe -is
```
Añadimos las rutas al componente (página), en el path debemos indicar que se va a pasar un parámetro, que será el ID del héroe a buscar (mostrar    )

```
import { HeroeComponent } from './components/heroe/heroe.component';
{ path: 'heroe/:id', component: HeroeComponent},
```

Para tener un id podemos sacarlo a la vez que renderizamos todos los objetos del array heroe, con index = i

```
<div class="card animated fadeIn fast" *ngFor="let heroe of heroes; let i = index">
```

Entonces ya podremos generar la ruta en el botón de enlace, usando routerLink se le pasa un array, dicho array serán las posiciones de la URL y sub-posiciones, le pasamos 'heroe' y el ID

```
<a [routerLink]="['/heroe',i]" class="btn btn-outline-primary">Ver más link...</a>
```

Para hacer el botón con programación usaremos un evento click y lanzaremos una función:

```
<button (click)="verHeroe(i)" type="button" class="btn btn-outline-primary btn-block">Ver más...</button>
```

Y en el componente necesitamos las siguientes importacion para habilitar la navegación:

```
import { Router } from '@angular/router';
```

Posteriormente necesitamos una variable de ese tipo:

```
constructor(   private _heroesService:HeroesService,
                 private router:Router) { // _heroesService es el alias que tendrá el servicio
  }
```

Entonces en la función verHeroe usaremos router y un metodo navigate que usa los mismo parametros para componer la url que en routerLink, con un array:

```
verHeroe( idx:number ){
    this.router.navigate( ['/heroe', idx]);
  } 
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 60. Recibiendo parámetros por URL - ActivatedRoute

Para recibir parámetro por URL vamos al componente que recibirá el parámetro, que es 'heroe', la ficha del héroe hacia la que navegar.

Importamos la libreria ActivatedRoute de @angular/router:

```
import { ActivatedRoute } from '@angular/router';
```

Y en el constructor creamos un escuchador para estar suscrito a los cambios que haya en el intercambio de parámetros:

```
this.activatedRoute.params.subscribe( params =>{ // Está constantemene escuchando cambios en params, como en las rutas se definió que se pasaría el id, eso es lo que recibe
      console.log(params['id']);
    })
```

Al igual que el servicio nos devolvía un array de héroes, ahora necesitamos que nos pueda devolver uno solo, tomando como referencia un ID
Para ello creamos un método que realice esta función:

```
getHeroe( idx: string ) {
      return this.heroes[idx];
    }
```

Entonces en el componente héroe ya podemos importar el servicio (que incluye este método), para posteriormente en el constructor generar una instacia del servicio, pudiendo acceder al método y así usarlo para que la variable local que almacena el héroe esté accesible, llenándola con la funcion que nos devuelve un héroes y pasandole el ID que está siendo escuchando en la URL mediante params de activated route:

```
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../servicios/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html'
})
export class HeroeComponent {

  heroe:any = {}; // Variable local para mostrar en el template

  constructor( private activatedRoute: ActivatedRoute,
               private _heroesService: HeroesService) {
    this.activatedRoute.params.subscribe( params =>{ // Está constantemene escuchando cambios en params, como en las rutas se definió que se pasaría el id, eso es lo que recibe
      this.heroe = this._heroesService.getHeroe(params['id']);
    });
   }
}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 61. Tarea práctica #1 - Componente del héroe

Crear la página de héroe, mi resolución:

```
<h1> {{ heroe.nombre }} <small> {{ heroe.aparicion }} </small></h1>
<hr>

<div class="row">
    <div class="col-md-4">
        <img [src]="heroe.img" class="img-fluid" [alt]="heroe.nombre">
        <br><br>
        <a [routerLink]="['/heroes']" class="btn btn-outline-danger btn-block">Regresar</a>
    </div>
</div>
<div class="col-md-8">

    <h3> {{ heroe.nombre }} </h3>
    <hr>
    <p>
        {{ heroe.bio }}
    </p>
    <div *ngIf="heroe.casa == 'Marvel'">
        <img src="././assets/img/marvel-logo.png" alt="Marvel">
    </div>
    <div *ngIf="heroe.casa == 'DC'">
        <img src="././assets/img/dc-logo.jpg" alt="DC">
    </div>
</div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 62. Resolución de la tarea práctica #1 - Componente del héroe

Resolución del profesor:

```
Como la mía :D
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 63. Pipes: Transformación visual de la data.

Transforman los datos en el template de forma visual

Breve muestra con pipes predefinidos de Angular que se pueden ver en la documentación de Angular.io

```
<h1 class="animated fadeIn"> {{ heroe.nombre | uppercase}} <small> ({{ heroe.aparicion | date:'y'}}) </small></h1>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 64. Buscador de Héroes

En el input del buscador definimos un alias #buscarTexto y que se dispare un evento cuando se pulse "Enter" que ejecuta una función buscarHeroe():

a buscarHeroe() se le pasará como argumento el contenido del input, que como fue definido con un alias, se recogerá con buscarTexto.value

```
<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Buscar héroe" aria-label="Search" (keyup.enter)="buscarHeroe(buscarTexto.value)" #buscarTexto>
            <button (click)="buscarHeroe(buscarTexto.value)" class="btn btn-outline-primary my-2 my-sm-0" type="button">Buscar</button>
        </form>
```

Ahora creamos la función en el componente del navbar:

```
```

Antes necesitaremos en el servicio poder controlar búsquedas en el array de héroes:

```
buscarHeroes( termino: string ) {

      let heroesArr:Heroe[] = [];
      termino = termino.toLowerCase();

      for ( let heroe of this.heroes ) {
        let nombre = heroe.nombre.toLowerCase();
        if (nombre.indexOf( termino ) >= 0) {
          heroesArr.push(heroe);
        }
      }

      return heroesArr;
    }
```

Lo que hacemos ahí es básicamente recorrer un array temporal que contiene una copia del array de Héroes y comparar con el término que se pasa como parámetro, si el indexOf es mayor que 0 significa que coincide el inicio de las cadenas de texto, por lo tanto la búsqueda coincide

[Volver al Índice](#%C3%ADndice-del-curso)

## 65. Tarea práctica #2: Crear la pantalla de búsqueda de héroes.

Cuando se busque un héroe usar la variable recibida en el input, crear una ruta a un nuevo componente para que en esa página se muestre el héroe (o héroes) que coincidan con el término de búsqueda recibido por el input

[Volver al Índice](#%C3%ADndice-del-curso)

## 66. Resolución de la tarea 2 - Buscador de Héroes.

Crear un nuevo componente:

>ng g c componentes/buscador

Agregar la nueva ruta en app.routes.ts

```
import { BuscadorComponent } from './components/buscador/buscador.component';

{ path: 'buscar/:termino', component: BuscadorComponent},
```

Vamos al componente del buscador que hemos creado y ahí tenemos que recibir el parámetro que recibimos por el input de la barra de navegación, para ello primero importamos el módulo ActivatedRoute que nos permite pasar parámetros por la url:

```
import { ActivatedRoute } from '@angular/router';
```

Para poder usarlo lo instanciamos en el constructor:

```
constructor( private activatedRoute:ActivatedRoute ) { }
```

A continuación como en las rutas definimos que se va a pasar por url un parámetro llamado "término" en el componente de buscador definimos que esté "escuchando" si se pasa dicho parámetro, para ello lo definimos, por ejemplo, en ngOnInit, así queda listo escuchando:

```
this.activatedRoute.params.subscribe( params => {
      console.log(params['termino']);
    })
```

Lo siguientes es redirigir del navbar al componente buscador, así que en el navbar component importamos el router y lo instanciamos en el constructor

```
import { Router } from '@angular/router';
constructor( private router:Router ) { }
```

Entonces en la funcion para buscar ya podemos hacer que navegue al componente buscador, pasandole el término que definimos en app.routes.ts

```
buscarHeroe( termino: string ) {
    //console.log(termino);
    this.router.navigate( ['/buscar',termino]);
  }
```

Nota importante: para que el navbar funcione pulsando enter (keyup.enter) hay que definir el botón como "submit" no como "button", corregido el código

En el componente buscador, posteriormente, importamos el servicio para poder mostrar héroes del servicio en función del parámetro recibido desde el navbar y lo instanciamos en el constructor

```
import { HeroesService } from '../../servicios/heroes.service';
constructor( private activatedRoute:ActivatedRoute,
               private _heroesService:HeroesService  ) {

  }
```

Posteriormente creamos una variable local llamada heroes que usaremos para generar el resultado de la búsqueda, como array, puesto que pueden ser varios los que coincidan con el termino de búsqueda. En la función que definimos en el ngOnInit que escuchaba el parámetro que pasamos por url ahora usamos la función de buscarheroe que definimos en el servicio para guardar en la variable local creada el resultado de la búsqueda por el término:

```
heroes:any[] = []

this.activatedRoute.params.subscribe( params => {
      this.termino = params['termino'];
      this.heroes = this._heroesService.buscarHeroes( params['termino'] );
      console.log( this.heroes )
    });


```

Ahora definamos el html, que será una versión igual o parecida al heroes.component.html, lo único que guardaremos el término del buscador en una variable para usarla en el frontal

```
termino:string;
this.termino = params['termino'];
```

```
<h1>Buscando: <small>{{ termino }}</small></h1>
<hr>

<div class="card-columns">
    <div class="card animated fadeIn fast" *ngFor="let heroe of heroes; let i = index">
        <img [src]="heroe.img" class="card-img-top" [alt]="heroe.nombre">
        <div class="card-body">
            <h5 class="card-title">{{ heroe.nombre }}</h5>
            <p class="card-text">{{ heroe.bio }}</p>
            <p class="card-text"><small class="text-muted">{{ heroe.aparicion }}</small></p>
            <button (click)="verHeroe(i)" type="button" class="btn btn-outline-primary btn-block">Ver más...</button>
            <!-- <a [routerLink]="['/heroe',i]" class="btn btn-outline-primary">Ver más link...</a> -->
        </div>
    </div>
</div>
```
[Volver al Índice](#%C3%ADndice-del-curso)

## 67. Plus: Mostrando un mensaje cuando no hay resultados.

Simplemente definimos que se muestre o no el mensaje en función de si el array tiene valores o no

```
<div class="row animated fadeIn fast" *ngIf="heroes.length == 0">
    <div class="col-md-12 col-lg-12">
        <div class="alert alert-info" role="alert">
            No existen resultados con el término: {{ termino }}
        </div>
    </div>
</div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 68. @Input - Recibir información de un componente padre a un hijo.

Vamos a extraer el código html del componente de héroes que define la tarjeta del héroe, para definir un componente independiente de tarjeta, lo cual usaremos para practicar la tranfusión de información de elementos padres a hijos y viceversa.

Empezamos por generar un componente con el Angular CLI:

> ng g c components/heroeTarjeta

Una vez generado vamos a modificar heroes.component.html, sacaremos el ngFor del html de la tarjeta y lo comentaremos, para tener constancia de que así se generaba antes, dinámicamente en función de los datos que se recibían del servicio, posteriormente sacaremos el html de la tarjeta de ahí y lo pondremos en el html del nuevo componente heroe-tarjeta.component.html

Quedando heroes.component.html:

```
<h1>Héroes <small>Marvel y DC</small></h1>
<hr>
<!-- *ngFor="let heroe of heroes; let i = index" -->
<div class="card-columns">

</div>
```

Y heroe-tarjeta.component.html:

```
<div class="card animated fadeIn fast">
    <img [src]="heroe.img" class="card-img-top" [alt]="heroe.nombre">
    <div class="card-body">
        <h5 class="card-title">{{ heroe.nombre }}</h5>
        <p class="card-text">{{ heroe.bio }}</p>
        <p class="card-text"><small class="text-muted">{{ heroe.aparicion }}</small></p>
        <button (click)="verHeroe(i)" type="button" class="btn btn-outline-primary btn-block">Ver más...</button>
        <!-- <a [routerLink]="['/heroe',i]" class="btn btn-outline-primary">Ver más link...</a> -->
    </div>
</div>
```

Ahora en heroe-tarjeta.component.ts necesitaremos el héroe, por lo cual lo definimos en la clase del componente como un objeto de tipo "any" vacío:

```
export class HeroeTarjetaComponent implements OnInit {

  heroe: any = {};
```

Y en la plantilla html de heroes.component.html ahora podemos llamar al componente por el selector y usar el ngFor que dejamos comentado anteriormente, quedando el código de esta manera:

```
<h1>Héroes <small>Marvel y DC</small></h1>
<hr>
<!-- *ngFor="let heroe of heroes; let i = index" -->
<div class="card-columns">
    <app-heroe-tarjeta *ngFor="let heroe of heroes; let i = index"></app-heroe-tarjeta>
</div>
```

Para pasar la información del componente padre (heroes.component) al componente hijo (heroe-tarjeta.component) necesitamos importar "Input" en el componente hijo de la librería de angular/core. Esto le dirá a Angular que una propiedad va a ser recibida desde afuera.

> import { Component, OnInit, Input } from '@angular/core';

Usando el decorador en la clase indicamos que puede venir desde afuera, pero si no fuera así podemos usar la propiedad desde el constructor normalmente, por ejemplo con algún parámetro por defecto, etcétera.

> @Input() heroe: any = {};

Entonces desde la llamada al componente, en el selector, podemos pasarle una propiedad al hijo, como el ngFor recibe el servicio y genera un array de datos, podemos pasarle ese "heroe" de esta manera:

```
    <app-heroe-tarjeta [heroe]="heroe" *ngFor="let heroe of heroes; let i = index"></app-heroe-tarjeta>
```

A continuación vamos a definir la función verHeroe que al haber movido el html ya no se encuentra definida en el nuevo archivo de typescript. El índice lo vamos a definir como nueva propiedad del Input. Para hacer la redirección en la función de primeras lo vamos a hacer como hicimos en el hereos.component usando Router. Esto también lo actualizaremos en el html del buscador. El código que resulta es el siguiente.

```
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html'
})
export class HeroeTarjetaComponent implements OnInit {

  @Input() heroe: any = {};
  @Input() index: number;

  constructor( private router:Router ) { }

  ngOnInit(): void {
  }
  verHeroe() {
    this.router.navigate( ['/heroe', this.index]);
  }
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 69. @Output - Emitir un evento del hijo hacia el padre.

Aquí haremos lo contrario que en la lección anterior. Que el hijo emita un evento, mientras que el padre está escuchando, y cuando lo escuche realice una acción.

Para ello en este caso práctico vamos a utilizar la función verHeroe que teníamos en heroes.component.ts, queremos llamar a esa función del padre desde el hijo (heroe-tarjeta.component)

Primeramente en el componente hijo importamos "Output" de angular/core, que va de la mano con EventEmitter que se encargará de emitir el evento que el componente padre tiene que escuchar.

Al igual que definimos las propiedades con los decoradores @Input vamos a declarar la propiedad para @Output, pero en este caso tendrá el nombre del evento que queremos que escuche el padre. Además tendremos que especificar que es de tipo EventEmitter, y también que tipo de dato es el que se emitirá, como lo que queremos es el índice del array de héroe, que es de tipo number, quedará:

> @Output() heroeSeleccionado: EventEmitter<number>

En el constructor necesito inicializar el EventEmitter

```
constructor( private router:Router ) { 
    this.heroeSeleccionado = new EventEmitter();
  }
```

Ahora podemos usar el objeto en la función verHeroe para, usando el método propio "emit", emitir el índice que nos interesa, quedando:

```
verHeroe() {
    // this.router.navigate( ['/heroe', this.index]);
    this.heroeSeleccionado.emit( this.index );
  }
```

Ahora el padre debe tener la posibilidad de escuchar, para cuando se emita el índice pueda escucharlo, para ello nos vamos al componente padre, al selector del componente hijo, en lugar de usar un evento tipo click, por ejemplo, usaremos nuestro evento propio definido como heroeSeleccionado, el cual ejecutará la función verHeroe, y como esta necesita un indice, lo que le vamos a pasar es el índice que emite nuestro evento. El código sería el siguiente:

```
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html'
})
export class HeroeTarjetaComponent implements OnInit {

  @Input() heroe: any = {};
  @Input() index: number;

  @Output() heroeSeleccionado: EventEmitter<number>

  constructor( private router:Router ) { 
    this.heroeSeleccionado = new EventEmitter();
  }

  ngOnInit(): void {
  }
  verHeroe() {
    // this.router.navigate( ['/heroe', this.index]);
    this.heroeSeleccionado.emit( this.index );
  }
}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 70. Arreglar detalles de la búsqueda.

Vamos a implementar el que, tras hacer una búsqueda y que aparezca la ficha de un héroe, se pueda hacer click en "Ver más" y funcione :)

Para ello vamos a alterar de nuevo la función verHeroe, en lugar de que funcione con el evento, lo dejaremos como estaba antes, usando Router.

El problema está en el servicio, porque los objetos no tienen un ID único. Como lo que usamos el la posición del array (el index), cuando realizamos una búsqueda satisfactoria, nos devuelve un array, al tratar de abrir esa ficha de héroe usa el índice, entonces nos devuelve la posición del objeto de héroes del servicio ... para arreglar esto vamos a arreglar la búsqueda.

Vamos a modificar el bucle for que nos rellenaba el arreglo de heroes de la búsqueda, ahora recorreremos todo el array y guardaremos la posición, la cual será opcional y será definida como tal al final, así añadiremos al array de héroes ese dato adicional, el código actualizado quedará así:

```

import { Injectable } from '@angular/core';

@Injectable()
export class HeroesService {

    private heroes:Heroe[] = [
        {
          nombre: "Aquaman",
          bio: "El poder más reconocido de Aquaman es la capacidad telepática para comunicarse con la vida marina, la cual puede convocar a grandes distancias.",
          img: "assets/img/aquaman.png",
          aparicion: "1941-11-01",
          casa:"DC"
        },
        {
          nombre: "Batman",
          bio: "Los rasgos principales de Batman se resumen en «destreza física, habilidades deductivas y obsesión». La mayor parte de las características básicas de los cómics han variado por las diferentes interpretaciones que le han dado al personaje.",
          img: "assets/img/batman.png",
          aparicion: "1939-05-01",
          casa:"DC"
        },
        {
          nombre: "Daredevil",
          bio: "Al haber perdido la vista, los cuatro sentidos restantes de Daredevil fueron aumentados por la radiación a niveles superhumanos, en el accidente que tuvo cuando era niño. A pesar de su ceguera, puede \"ver\" a través de un \"sexto sentido\" que le sirve como un radar similar al de los murciélagos.",
          img: "assets/img/daredevil.png",
          aparicion: "1964-01-01",
          casa: "Marvel"
        },
        {
          nombre: "Hulk",
          bio: "Su principal poder es su capacidad de aumentar su fuerza hasta niveles prácticamente ilimitados a la vez que aumenta su furia. Dependiendo de qué personalidad de Hulk esté al mando en ese momento (el Hulk Banner es el más débil, pero lo compensa con su inteligencia).",
          img: "assets/img/hulk.png",
          aparicion: "1962-05-01",
          casa:"Marvel"
        },
        {
          nombre: "Linterna Verde",
          bio: "Poseedor del anillo de poder que posee la capacidad de crear manifestaciones de luz sólida mediante la utilización del pensamiento. Es alimentado por la Llama Verde (revisada por escritores posteriores como un poder místico llamado Starheart), una llama mágica contenida en dentro de un orbe (el orbe era en realidad un meteorito verde de metal que cayó a la Tierra, el cual encontró un fabricante de lámparas llamado Chang)",
          img: "assets/img/linterna-verde.png",
          aparicion: "1940-06-01",
          casa: "DC"
        },
        {
          nombre: "Spider-Man",
          bio: "Tras ser mordido por una araña radiactiva, obtuvo los siguientes poderes sobrehumanos, una gran fuerza, agilidad, poder trepar por paredes. La fuerza de Spider-Man le permite levantar 10 toneladas o más. Gracias a esta gran fuerza Spider-Man puede realizar saltos íncreibles. Un \"sentido arácnido\", que le permite saber si un peligro se cierne sobre él, antes de que suceda. En ocasiones este puede llevar a Spider-Man al origen del peligro.",
          img: "assets/img/spiderman.png",
          aparicion: "1962-08-01",
          casa: "Marvel"
        },
        {
          nombre: "Wolverine",
          bio: "En el universo ficticio de Marvel, Wolverine posee poderes regenerativos que pueden curar cualquier herida, por mortal que ésta sea, además ese mismo poder hace que sea inmune a cualquier enfermedad existente en la Tierra y algunas extraterrestres . Posee también una fuerza sobrehumana, que si bien no se compara con la de otros superhéroes como Hulk, sí sobrepasa la de cualquier humano.",
          img: "assets/img/wolverine.png",
          aparicion: "1974-11-01",
          casa: "Marvel"
        }
      ];

    constructor() {
        console.log("Servicio listo para usar!!!");
    }

    getHeroes():Heroe[] {
      return this.heroes;
    }

    getHeroe( idx: string ) {
      return this.heroes[idx];
    }

    buscarHeroes( termino: string ) {
      let heroesArr:Heroe[] = [];
      termino = termino.toLowerCase();

      for ( let i = 0; i < this.heroes.length; i++) {
        
        let heroe = this.heroes[i];

        let nombre = heroe.nombre.toLowerCase();

        if ( nombre.indexOf( termino ) >= 0 ) {
          heroe.idx = i;
          heroesArr.push( heroe );
        }
      }
      return heroesArr;
    }
}
export interface Heroe {
    nombre: string;
    bio: string;
    img: string;
    aparicion: string;
    casa: string;
    idx?: number;
};

```

Ya que tenemos el índice correcto tendremos que llamarlo en el buscador.component.html, quedando:

```
<app-heroe-tarjeta [heroe]="heroe" [index]="heroe.idx" *ngFor="let heroe of heroes"></app-heroe-tarjeta>

```

[Volver al Índice](#%C3%ADndice-del-curso)

# Sección 5: Pipes - Transforman los valores mostrados en pantalla

## 72. Introducción a la sección

Básicamente un pipe es un método o una función, que recibe una serie de argumentos (como mínimo uno), los cuales los procesará y devolverá un dato que necesitemos.

Esta salida no muta el objeto que estemos manipulando, el cambio será sólo visual (por ejemplo un pipe para pasar un string a mayúsculas no alteraría esa variable)

[Volver al Índice](#%C3%ADndice-del-curso)

## 73. ¿Qué aprenderemos en esta sección?

A continuación vamos a entrar a profundidad sobre el tema de los PIPES, que no es más que una transformación visual de la data.

En detalle veremos:

Pipes uppercase y lowercase

Pipe Slice

Pice Decimal

Pipe Percent

Pipe Currency

Pipe Json

Pipe Async

Pipe Date

Pipes personalizados

Capitalizar palabras y nombres

Creación de un pipe, que permite cargar recursos externos de forma segura.

Al final de la sección tendremos una tarea para afianzar los conocimientos creando un pipe personalizado que nos permitirá ocultar un texto a voluntad.

[Volver al Índice](#%C3%ADndice-del-curso)

## 74. Demostración del resultado de la sección - Pipes

Ver vídeo explicativo con el resultado de la sección.

[Volver al Índice](#%C3%ADndice-del-curso)
## 75. Inicio del proyecto y la introducción a los Pipes

- Se llaman así porque usan el caracter pipe "|" para poder trabajar con ellos en el HTML, también se pueden utilizar del lado de los componentes.

- Únicamente funcionan para transformar los datos de manera visual, no cambian el valor, no mutan los objetos si son pasados por referencia

- Ejemplos:
  - texto = "hola mundo" -> {{ texto | uppercase }} -> HOLA MUNDO (El valor de la variable 'texto' seguiría estando en minúscula)
  - fecha = new Date(1985,10,21) -> {{ fecha | date:"dd/MM/yy" }} -> 21/11/1985

Generamos el nuevo proyecto de la sección:

> ng new pipes

Cuando pregunte si queremos generar las rutas le indicamos que no, y para estilos lo dejaremos en CSS

Una vez generado renombraremos el directorio a 03-pipes para coherencia con el resto del curso

A continuación levantamos el proyecto con:

> ng serve -o

Vamos a eliminar todo el contenido de app.component.html para generar nosotros ahí nuestro código, posteriormente instalaremos bootstrap.

Getbootstrap.com -> get started -> copiar link de estilos y añadirlo al index.html

```
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
```

Añadiremos al body la class container <body class="container">

El código que dejaremos definido para trabajar en el app.component.html será:

```
<h1 class="mt-3">Pipes <small>Angular</small></h1>
<hr>

<div class="row">
    <div class="col">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th> Variable </th>
                    <th> Pipe </th>
                    <th> Salida </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
```

En app.component.ts vamos a definir una variable tipo string para empezar a trabajar con ella:

```
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  nombre: string = 'Capitán América';
  
}
```
Entonces podremos llamarla en el html, usando los primeros pipes que vamos a aprender "uppercase" y "lowercase" los cuales vienen definidos por defecto con Angular y no hay que importar nada ni hacer nada adicional para usarlo:

```
<h1 class="mt-3">Pipes <small>Angular</small></h1>
<hr>

<div class="row">
    <div class="col">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th> Variable </th>
                    <th> Pipe </th>
                    <th> Salida </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> {{ nombre }} </td>
                    <td> uppercase </td>
                    <td> {{ nombre | uppercase }} </td>
                </tr>
                <tr>
                    <td> {{ nombre }} </td>
                    <td> lowercase </td>
                    <td> {{ nombre | lowercase }} </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
```



[Volver al Índice](#%C3%ADndice-del-curso)
## 76. Pipe: Slice

Para recortar cadenas o arrays, requiere al menos un parámetro, en estos dos ejemplos, cuando definimos como parámetro ":3" estamos indicando que corte a partir del tercer carácter de la cadena, si definimos como parámetro ":0:3" estamos diciendo que empiece la cadena en la posición 0 y acabe en la posición 3:

```
 <tr>
                    <td> {{ nombre }} </td>
                    <td> slice:3 </td>
                    <td> {{ nombre | slice:3 }} </td>
                </tr>
                <tr>
                    <td> {{ nombre }} </td>
                    <td> slice:0:3 </td>
                    <td> {{ nombre | slice:0:3 }} </td>
                </tr>
```

Para probar con un array vamos a declarar uno en el archivo ts:

> array: [1,2,3,4,5,6,7,8,9,10];

Podemos incluso usar el pipe en un bucle para formatear el resultado, independientemente de si hay suficientes valores para cortar o no:

```
<tr>
  <td> {{ array }} </td>
  <td> slice:1:5 </td>
  <td> {{ array | slice:1:5 }} </td>
</tr>
<h4> Slice </h4>
<ul>
    <li *ngFor="let item of array | slice:5:20"> {{ item }} </li>
</ul>
```

[Volver al Índice](#%C3%ADndice-del-curso)
## 77. Pipe: Decimal

Ver documentación en angular.io sobre DecimalPipe

Creamos una variable numérica con decimales en app.component.ts:

> PI:     number = Math.PI;

El formateo visual del número decimal o entero se realiza pasando un argumento string que define el formato deseado, como ejemplos:

```
<tr>
    <td> {{ PI }} </td>
    <td> number:'3.0-0' </td>
    <td> {{ PI | number:'3.0-0' }} </td>
</tr>
<tr>
    <td> {{ PI }} </td>
    <td> number:'.0-2' </td>
    <td> {{ PI | number:'.0-2' }} </td>
</tr>
```

[Volver al Índice](#%C3%ADndice-del-curso)
## 78. Pipe: Percent - Porcentajes

Al igual que en la lección anterior, este pipe recibe un string como argumento, revisar angular.io en la documentación para más info.

En app.component.ts creamos otra variable de ejemplo:

> porcentaje: number = 0.234;

Y en app.component.html:

```
<tr>
    <td> {{ porcentaje }} </td>
    <td> percent </td>
    <td> {{ porcentaje | percent }} </td>
</tr>
<tr>
    <td> {{ porcentaje }} </td>
    <td> percent:'2.2-2' </td>
    <td> {{ porcentaje | percent:'2.2-2' }} </td>
</tr>
```

[Volver al Índice](#%C3%ADndice-del-curso)
## 79. Pipe: Currency - Moneda

Al igual que en lecciones anteriores, revisar angular.io para la documentación, para el ejemplo creamos una variable en app.component.ts:

> salario:    number = 1234.5;

En este caso para formatear podemos usar el argumento currencyCode para usar un código de país y así usar su divisa. Ejemplificado en el html quedaría:

```
<tr>
    <td> {{ salario }} </td>
    <td> currency:'EUR' </td>
    <td> {{ salario | currency:'EUR' }} </td>
</tr>
<tr>
    <td> {{ salario }} </td>
    <td> currency:'CAD':'symbol-narrow':'.0-0' </td>
    <td> {{ salario | currency:'CAD':'symbol-narrow':'.0-0' }} </td>
</tr>
```

[Volver al Índice](#%C3%ADndice-del-curso)
## 80. Pipe: Json

Como en casos anteriorer, revisar documentación de angular.io JsonPipe. Para practicar nosotros creamos en app.component.ts un nuevo objeto:

```
heroe = {

    nombre: 'Logan',
    clave: 'Wolverine',
    edad: 500,
    dirección: {
      calle: 'Primera',
      casa: 20
    }
  }
```

Si intentáramos llamar directamente a {{ heroe }} en nuestro html nos aparecería [object Object]. Porque estaría tratando de representar un objeto como un string, texto plano, en el HTML, para eso sirve este pipe, para formatear la información y mostrarla como un objeto JSON. Para ejemplificarlo en nuestro app.component.html:

```
<h4> JSON </h4>
  <hr>
  <div class="row mb-5">
      <div class="col">
          {{ heroe | json }}
      </div>
  </div>
```

[Volver al Índice](#%C3%ADndice-del-curso)
## 81. Pipe: Async

Revisamos en angular.io la información sobre el pipe AsyncPipe. Vamos a crear una promesa en app.component.ts para ejemplificar:

```
valorPromesa = new Promise<string>( (resolve) => {

    setTimeout(()=> {
      resolve('llego la data');
    }, 4500);

});
```

Similar al caso del pipe de JSON, si tratamos de imprimir directamente en el html 'valorPromesa' nos mostrará [object Promise], al pasarlo por el pipe "async" nos mostrará el string que resuelve la promesa, en el html quedaría:

```
<tr>
    <td> {{ valorPromesa }} </td>
    <td> Async </td>
    <td> {{ valorPromesa | async }} </td>
</tr>
```


[Volver al Índice](#%C3%ADndice-del-curso)
## 82. Pipe: Date - Fecha

Revisamos en angular.io la documentación del pipe DatePipe que gestiona fechas.

Para nuestra práctica creamos una fecha en app.component.ts:

> fecha:      Date = new Date();

En cuanto al "idioma" de la fecha podemos usar el argumento 'timezone' para configuararlo, nosotros vamos a hacer una configuración propia del 'locale', instalándolo globalmente para que el navegador lo detecte y esa variable tome valor en función del idioma del navegador o dónde se está ejecutando este, para ello usando el angular CLI lo instalamos de la siguiente manera:

> ng add @angular/localize

Esto nos prepara y actualiza el archivo src/polyfills.ts , a continuación tendremos que realizar una configuración, para eso tenemos que añadir en src/app/app.module.ts lo siguiente:

```
import { registerLocaleData } from '@angular/common';
import  localEs  from '../../node_modules/@angular/common/locales/es';
registerLocaleData(localEs);
```

Nótese que 'localEs' será lo que usemos para registrar/llamar al idioma en la funcion registerLocaleData(localEs);

Para terminar de configurar manualmente esto deberemos añadir en app.module.ts: 

```
import { NgModule, LOCALE_ID } from '@angular/core';// LOCALE_ID nos provee de un token de localización

providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es'
    }
  ],
```

Esto también cambiará la visualización de algunas cosas, por ejemplo la posición del símbolo de moneda, que en España se pone al final, en lugar de al principio.

Finalmente el código de ejemplo que usaremos en el html será el que sigue:

```
<tr>
  <td> {{ fecha }} </td>
  <td> date </td>
  <td> {{ fecha | date }} </td>
</tr>
<tr>
  <td> {{ fecha }} </td>
  <td> date:'medium' </td>
  <td> {{ fecha | date:'medium' }} </td>
</tr>
<tr>
  <td> {{ fecha }} </td>
  <td> date:'short' </td>
  <td> {{ fecha | date:'short' }} </td>
</tr>
<tr>
  <td> {{ fecha }} </td>
  <td> date:'MMMM - dd' </td>
  <td> {{ fecha | date:'MMMM - dd' }} </td>
</tr>
```

[Volver al Índice](#%C3%ADndice-del-curso)
## 83. Registrar otros idiomas

Vamos a configurar el registro de tres idiomas para testear esta posibilidad.

En la lección anterior cuando hicimos esta instalación:

> ng add @angular/localize

Lo que hicimos fue instalar localmente todos los idiomas en nuestra máquina, pero no están en la aplicación. Hasta que no hacemos el registro usando la función registerLocaleData no formarán parte del bundle de la app.

Vamos a registrar algunos idiomas adicionales al español, actualizando el archivo app.module.ts:

```
import  localEs  from '../../node_modules/@angular/common/locales/es';
import  localFr  from '../../node_modules/@angular/common/locales/fr';

registerLocaleData(localEs);
registerLocaleData(localFr);
```

En el html podremos usar el pipe para que lo muestre en francés de la siguiente manera:

```
<tr>
  <td> {{ fecha }} </td>
  <td> date:'MMMM - dd':'':'fr' </td>
  <td> {{ fecha | date:'MMMM - dd':'':'fr' }} </td>
</tr>
```

Como ejercicio adicional vamos a crear una variable en el componente que registre un string, ese string lo vamos a usar como valor del locale como argumento para el pipe:

```
idioma:     string = 'es';
```

De esta manera podemos maquetar el html para que cuando se haga click en unos botones cambie el valor de dicho string y, por tanto, del argumento del locale, haciendo que el filtro del pipe cambie y pudiendo así cambiar el idioma de la fecha:

```
<tr>
                    <td> {{ fecha }} </td>
                    <td> date:'MMMM - dd':'':'{{ idioma }}'
                        <br>
                        <button (click)="idioma='en'" class="mr-1 btn btn-secondary">Inglés</button>
                        <button (click)="idioma='es'" class="mr-1 btn btn-primary">Español</button>
                        <button (click)="idioma='fr'" class="mr-1 btn btn-success">Francés</button>
                    </td>
                    <td> {{ fecha | date:'MMMM - dd':'':idioma }} </td>
                </tr>
```
[Volver al Índice](#%C3%ADndice-del-curso)
## 84. Pipes personalizados: Capitalizar palabras

Vamos a crear unos pipes personalizados, para empezar vamos a declarar una nueva variable en nuestro app.component.ts, para ver el efecto definiremos el string con caracteres en mayúsculas y minúsculas alternados:

```
nombre2:     string = 'eDuarDo CórDOba jOAquíN';
```

Posteriormente, para crear nuestro pipe, podemos hacerlo directamente con el angular CLI de esta manera, por convención se genera un directorio "pipes":

> ng g p pipes/capitalizado

Esto nos crea el directorio, el archivo spec de testeo y el archivo ts de definición del pipe, asimismo nos actualiza el app.module.ts de esta manera:

```
import { CapitalizadoPipe } from './pipes/capitalizado.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CapitalizadoPipe
  ],
```

Al haber sido añadido a "declarations" toda la aplicación sabrá de la existencia del pipe creado

A continuación tenemos el código del archivo ts del pipe, vamos a describirlo posteriormente

```
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizado'
})
export class CapitalizadoPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return null;
  }

}
```

Es como cualquier otra clase, solo que tenemos el decorador @Pipe, en el cual tenemos el nombre definido de nuestro pipe 'capitalizado', el que usaremos cuando queramos aplicar el formateo.

Posteriormente tenemos la función PipeTransform que será la que se encargue de realizar la transformación en el html

'value' será el valor del string (o el valor que queramos transformar, en otros casos) y 'args' serán todos los argumentos que se envíen a la función, en :unknown al final de la función definiremos el tipo de valor de salida de la función (lo que devolverá el return dentro de la función, por defecto está 'null')

Vamos a realizar una primera modificación de la función para ver su efecto, como tipo de valor devuelto vamos a especificar que va a ser de tipo string, y el retorno de la función va a ser el string 'Hola Mundo'

```
transform(value: unknown, ...args: unknown[]): string {
    return 'Hola Mundo';
  }
```

Si ahora en nuestro html aplicamos el pipe personalizado 'capitalizado' en el html lo que aparecerá en lugar del contenido de la variable 'nombre2' será 'Hola Mundo', recordemos que el valor de la variable 'nombre2' sigue existiendo, pero el resultado visual sería el que hemos indicado.

```
<tr>
    <td> {{ nombre2 }} </td>
    <td> capitalizado </td>
    <td> {{ nombre2 | capitalizado }} </td>
</tr>
```

Vamos a definir dentro de la función del pipe un par de console log para comprender cómo funciona. Si hacemos un console.log(value) veremos en la consola del navegador que, efectivamente, el valor de la variable nombre2 sigue siendo el que era, como hemos comentado anteriormente.

```
transform(value: unknown, ...args: unknown[]): string {
    console.log(value);
    console.log(args);
    return 'Hola Mundo';
  }
```

El console.log(args) en principio nos mostrará por consola un array vacío, pero probemos a definir argumentos de prueba para ver la salida,por ejemplo un número, un booleano y un string:

```
<tr>
    <td> {{ nombre2 }} </td>
    <td> capitalizado </td>
    <td> {{ nombre2 | capitalizado:1:true:'Hola' }} </td>
</tr>
```

De tal manera podemos ver los argumentos en consola, o capturarlos, la salida por consola del console.log(args) una vez definidos esos argumentos sería:

>[1, true, "Hola"]

Por tanto podríamos usar la desestructuración de array en la función, o podríamos simplemente definir los argumentos de la función que queramos, por ejemplo inventemos un 'edad', 'activo' y 'mensaje', para que concuerden con los valores que hemos especificado en el ejemplo anterior, y vamos a hacer la prueba de imprimirlos por consola de nuevo:

```
transform(value: unknown, edad: number, activo: boolean, mensaje: string): string {
    console.log(value);
    console.log({ edad, activo, mensaje });
    return 'Hola Mundo';
  }
```

La salida por consola queda tal que:

```
{edad: 1, activo: true, mensaje: "Hola"}
activo: true
edad: 1
mensaje: "Hola"
```

Una vez comprendido esto vamos a dejar definido de manera más útil para nosotros la función de la siguiente manera, definiremos un argumento 'todas' booleano, para controlar la capitalización de los caracteres. Definiremos el value como string para controlar que lo que se recibe en la función, el valor a modificar, es un string, lo cual también nos permitirá acceder a los métodos y funciones tipo string para trabajar. Entonces en la función recibiremos un string, este string lo pasaremos a minúscula por completo, luego cortaremos ese string y lo almacenaremos en un array de nombres, considerando un espacio en blanco como patrón de corte. Si en este punto hacemos un console.log(nombres) veremos imprime por consola ["eduardo", "córdoba", "joaquín"]. Efectivamente pasó todo a minúscula y las palabras por separado, ahora sólo necesitaríamos capitalizar el primer caracter de cada string del array y volver a unir todo el array en un solo string. Para esto primero preguntaremos, teniendo en cuenta el argumento booleano 'todas', si queremos capitalizar todas las palabras, cuando sea 'false' sólo se capitalizaría la primera palabra. Explicamos la función con comentarios dentro de la misma.

```
transform(value: string, todas: boolean = true): string {
    
    value = value.toLocaleLowerCase(); // Pasamos todo el string a minúscula

    let nombres = value.split(' '); // definimos un array y en él guardamos las palabras que componen el string, usando los espacios en blanco como separador

    if ( todas ) { // Si todas == true
      nombres = nombres.map( nombre => { // Usamos la función map para definir una función por cada uno de los elementos del array 'nombres'
        return nombre[0].toUpperCase() + nombre.substr(1); // Pasamos el primer caracter(posicion) de cada uno de los elementos del array (nombres[0]) a mayúscula, y le concatenamos el resto del array desde la posición 1 (subtr(1))
      });
    } else { // Si todas == false solamente al primer elemento del array le pasamos a mayúscula su primer caracter(posición) (nombres[0][0]) y concatenamos el resto desde la posición 1 (nombres[0].substr(1))
      nombres[0] = nombres[0][0].toUpperCase() + nombres[0].substr(1);
    }

    return nombres.join(' '); // Devolvemos como string el array usando como unión un espacio en blanco

  }
```

Y dejaremos el html preparado también, pasandole el argumento "true", aunque en la función ya habíamos definido que por defecto valdría "true"

```
<tr>
    <td> {{ nombre2 }} </td>
    <td> capitalizado </td>
    <td> {{ nombre2 | capitalizado:true }} </td>
</tr>
```

[Volver al Índice](#%C3%ADndice-del-curso)
## 85. Pipe Personalizado: Domseguro

En esta clase vamos a intentar colocar un vídeo o enlace externo en nuestra aplicación de Angular, y vamos a ver qué sucede.

Para ello por ejemplo vamos a YouTube y copiamos el iframe de un video a nuestra elección y lo incluímos en nuestro html:

```
<h4 class="mt-4"> Domseguro </h4>
<hr>
<div class="row mb-5">
    <div class="col">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/Brl7WmHDG-E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
        </iframe>
    </div>
</div>
```

A continuación en nuestro app.component.ts definimos una variable que contenga el string del enlace del vídeo:

```
videoUrl:   string = 'https://www.youtube.com/embed/Brl7WmHDG-E';
```

De nuevo en nuestro html tratamos de cargar el atributo de src del enlace usando la variable de Angular:

```
<div class="row mb-5">
            <div class="col">
                <iframe width="560" height="315" [src]="videoUrl" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
            </div>
        </div>
```

Entonces no cargará el vídeo, y si miramos la consola nos da un aviso de que se está tratando de cargar un recurso que es potencialmente inseguro:

> ERROR Error: unsafe value used in a resource URL context

Esto es porque el string de la variable podría ser un script o algo que Angular considera que hay que bloquear por seguridad. Pero si nosotros confiamos en el recurso que queremos cargar entonces tendremos que realizar un pequeño trabajo.

Para poder cargar recursos que vengan de fuera de nuestra aplicación, cuando nos esté dando esa indicación de unsafe value (puede suceder también con el CSS, algunas url, etc), vamos a crear un pipe domseguro.

> ng g p pipes/domseguro

Una vez creado el pipe, vamos a trabajar en la función, dicha función recibirá un string, y lo que haremos en nuestra función será pasarle un sanitizer a ese string, que nos permita limpiar o validar dicho string. En las librerías de Angular disponemos de dicho recurso, el cual vamos a importar en nuestro domseguro.pipe.ts:

```
import { DomSanitizer } from '@angular/platform-browser'
```

Tenemos que hacer la inyección del método en nuestra clase, para ello lo declaramos en el constructor, que definimos previamente.

Lo que devolverá la función con el return será una llamada a uno de los métodos de Domsanitizer dependiendo de lo que requiramos en cada caso, en este en concreto necesitamos 'bypassSecurityTrustResourceUrl', su nombre se explica por sí mismo :) lo que recibe ese método es el value, que será el string a sanear.

Si vemos el tooltip del método, vemos que retorna un tipo 'SafeResourceUrl', el cual dejaremos definido también en nuestra función del pipe. A priori no lo encontrará, así que lo definiremos también en el import anterior, puesto que se encuentra en el mismo paquete de Angular.

Finalmente el código de nuestro pipe quedará de esta manera:

```
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'
@Pipe({
  name: 'domseguro'
})
export class DomseguroPipe implements PipeTransform {

  constructor( private domSanitizer: DomSanitizer  ) {}

  transform(value: string, ...args: unknown[]): SafeResourceUrl {
    return this.domSanitizer.bypassSecurityTrustResourceUrl( value );
  }

}
```

Ahora ya podemos usar nuestro pipe y carga correctamente, porque aplica el pipe antes de que se termine de renderizar todo el html.

```
<div class="row mb-5">
            <div class="col">
                <iframe width="560" height="315" [src]="videoUrl | domseguro" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
            </div>
        </div>
```


[Volver al Índice](#%C3%ADndice-del-curso)
## 86. Tarea de Pipes

La tarea consiste en crear un pipe que, dada una variable que contiene un string, a través de un evento click en un botón oculte o muestre la llamada en el html de dicha variable string, sustituyéndola por asteriscos o mostrando su formato original. Hasta donde llegué fue:

Crear un pipe con el Angular cli

> ng g p pipes/contrasena

Me atasqué en la creación de la función que altera visualmente el string, no sabiendo bien qué opción elegir para checkear la cadena de caracteres y cómo sustituirla por los valores de asterisco, tenía en mente pasar la variable de string a array de string y recorrerla, etc, lo dejé hasta donde llegué porque intuía que la solución seguramente sería más sencilla. A continuación los códigos fuente del componente y del html creado para la tarea:

```
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContrasenaPipe implements PipeTransform {
  transform(value: string, activar:boolean ): string[] {
    let cadena = value;
    if ( activar ) {
      let contrasena = []
      for (let i = 0; i < cadena.length; i++) {
        contrasena[i]= '*'
        console.log(contrasena)     
      }   
return contrasena
    } else {
      //return value;
    }    
  }
}
```

```
<tr>
  <td> {{ nombre }} </td>
  <td> contrasena:{{ activar }}
      <br>
      <button (click)="activar = !activar" class="btn btn-outline-primary">Activar/Desactivar</button>
  </td>
  <td> {{ nombre | contrasena:activar }} </td>
</tr>
```
[Volver al Índice](#%C3%ADndice-del-curso)
## 87. Resolución de la tarea de Pipes

Primero creamos el pipe mediante el CLI

> ng g p pipes/contrasena

Efectivamente el profesor resuelve la parte de la función de manera mucho más sencilla, usando métodos actualizados de ECMAScript (.repeat()), con un condicional ternario.

```
 transform(value: string, mostrar:boolean = false ): string {
    return ( mostrar ) ? '*'.repeat( value.length ) : value;
  }
```

De esta manera comprueba si mostrar es verdadero, en tal caso devuelve un string que será consecuencia del carácter * devuelto tantas veces como sea la longitud del string original, en caso de que sea falso simplemente devolverá el string original (value)

El resto de la lógica la tenía correcta, ¡a por la siguiente sección!

[Volver al Índice](#%C3%ADndice-del-curso)

# Sección 6:Aplicación #3: SpotiApp
## 89. Introducción a la sección

En esta sección aprenderemos a desarrollar un aplicación que consume recursos de la API de Spotify, así como conceptos de HTTP, haciendo peticiones GET al servidor de Spotify, usando su API. Vamos a poder obtener su música, artistas, crearnos una pantalla de interfaz, crear un slideshow. También podremos usar el widget de Spotify para poder reproducir la canción completa. Por último veremos una introducción a la transformación de los datos que viene del servicio API de Spotify, pasándolo por un método map, para de esa manera poder usar esa información como nosotros queramos, también almacenaremos información en los servicios para que la información persista a pesar de movernos de página, al usar las rutas.

[Volver al Índice](#%C3%ADndice-del-curso)

## 90. ¿Qué aprenderemos en esta sección?

Vamos a crear una aplicación que nos ayudará a comprender sobre los siguientes temas:

1. Reforzamiento de rutas y parámetros de rutas.
2. Uso de carruseles del Bootstrap 4
3. Uso del HTTP para obtener información
4. Uso de la API de Spotify para obtener información de:
- a. Artistas
- b. Albumes
- c. Audio
5. Trabajo sobre el manejo de data asíncrona.
6. ngModel para enlazar campos de texto a variables del componente.
7. Widgets de Spotify
8. HTML5 audio
9. Observables
10. Maps
Durante la sección tendremos tareas y al final un examen teórico para reforzar los conocimientos adquiridos.

[Volver al Índice](#%C3%ADndice-del-curso)

## 91. Demostración del resultado de esta sección

Ver vídeo mostrando el resultado de la aplicación terminada y sus funcionalidades y conceptos aprendidos en ella.

[Volver al Índice](#%C3%ADndice-del-curso)

## 92. Sitio web de developer de Spotify

Sitio web de developer de Spotify

Actualización del sitio de Developer de Spotify

En esta sección crearemos una aplicación que consume los servicios de Spotify.

Pueden ver lo servicios en esta dirección

https://beta.developer.spotify.com/console/

Tengan esa página a mano, la necesitaremos pronto

[Volver al Índice](#%C3%ADndice-del-curso)

## 93. Iniciando el proyecto - SpotiApp

Primeramente descargamos una serie de recursos para la sección, unas imágenes y un archivo de estilos CSS.

Crearemos en nuevo proyecto yendo a la raíz del curso y usando el Angular cli:

> ng new spotiapp

Mientras vamos a https://developer.spotify.com/console/ porque aquí necesitamos hacer dos cosas: Crear en Spotify una aplicación y ver cómo funciona en la api de Spotify.

Vamos a https://developer.spotify.com/dashboard/ y hacemos login. (Yo por suerte tengo cuenta premium)

Posteriormente le damos a "CREATE A CLIENT ID"

Una vez creado ya dispondremos de dos datos fundamentales que son el CLIENT ID y el CLIENT ID SECRET. Ambas van a ser necesarias para crear un token.

Ya debió de terminar la creación de la app, la renombramos a 04-spotiapp

Levantamos la aplicación con `ng serve` en nuestra línea de comandos previa navegación al directorio de la nueva aplicación.

A continuación vamos a crear un par de componentes que vamos a necesitar para la aplicación, una página "home" `ng g c components/home --skipTests -is`, una página de "search" `ng g c components/search --skipTests -is`, una página de artista `ng g c components/artista --skipTests -is` y por último otro componente que estará en un subdirectorio compartido para el navbar `ng g c components/shared/navbar --skipTests -is` (--skipTests no genera archivos de test unitarios, -is es inline style, no genera referencias a archivos CSS externos y tampoco lo refiere dentro del archivo ts del componente). Crearemos más componentes a lo largo de la aplicación, pero de momento para empezar necesitaremos esos.

Copiaremos en el directorio /src/assets/ el directorio img que habíamos descargado anteriormente del archivo comprimido de recursos de la sección.

Del mismo origen copiaremos el archivo styles.css y lo sustituiremos por el que ya se encuentra en src/styles.css

Por último iremos a la página de getbootstrap.com / Descargas y añadir el CDN a nuestro index.html, dejando el <link> en el <head> y el <script> al final del <body>. También copiaremos los <script> del jquery y del popper colocándolos justo antes del <script> del bootstrap.

```
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Spotiapp</title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <app-root></app-root>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>

</html>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 94. Creando las rutas de nuestra aplicación

Vamos a crear un archivo nuevo en src/app/app.routes.ts

```
import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';



export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'search', component: SearchComponent },
    { path: '', pathMatch: 'full', redirectTo: 'home'}, // Cualquier otro path no definido nos redireccionará al home
    { path: '**', pathMatch: 'full', redirectTo: 'home'} // Cualquier otro path no definido nos redireccionará al home
];
```

En app.module.ts es donde pondremos el módulo de rutas.

Aquí en app.module.ts importaremos el módulo de rutas y lo incluiremos en los imports, asegurarnos en el imports que está cogiendo la constante del app.routes, y además definiremos que usaremos el HASH para las url (#), quedando el archivo tal que:

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { ArtistaComponent } from './components/artista/artista.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ROUTES } from './app.routes';

// Importación de rutas

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    ArtistaComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot( ROUTES, { useHash: true } ) // Quiero usar el hash
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Posteriormente vaciamos el app.component.html precargado de Angular. Incluiremos el componente de navbar, un div container y el router-outlet para posibilitar la navegación entre componentes. Para el navbar vamos a coger código de bootstrap que incluiremos en el navbar.component.html.

```
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="/assets/img/banner-ico.png" alt="" width="30" height="30"> Spotiapp
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item" routerLinkActive="active">
                    <a class="nav-link" aria-current="page" routerLink="home">Home</a>
                </li>
                <li class="nav-item" routerLinkActive="active">
                    <a class="nav-link" routerLink="search">Search</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 95. Introducción a las peticiones HTTP

Aquí obtendremos recursos mediante peticiones HTTP o Ajax, para ello necesitaremos un sitio al que pedir dicha información, para practicar vamos a ir al sitio web http://restcountries.eu/ y buscamos una lista de países por el lenguaje (API ENDPOINTS - Language).

Podemos testear con la aplicación externa Postman para hacer un get a la url: https://restcountries.eu/rest/v2/lang/es de tal manera que nos devuelve un objeto en formato JSON con la información de países que hablan español.

Para poder trabajar con este tipo de solicitudes en nuestra aplicación de Angular necesitamos dos cosas:

1. Importar en app.module.ts algo que nos permita hacer peticiones HTTP (`import { HttpClientModule } from '@angular/common/http';`), en HttpClientModule se incluyen una serie de herramientas que vamos a necesitar en nuestra app, entre ellas la que me permite hacer peticiones HTTP. Lo incluímos en los imports (ya que se trata de un módulo).

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { ArtistaComponent } from './components/artista/artista.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ROUTES } from './app.routes';

// Importación de rutas

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    ArtistaComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot( ROUTES, { useHash: true } ) // Quiero usar el hash
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

```

2. En nuestro home.component.ts vamos a incluir el código para manejar la información, en nuestro constructor declaramos `private Http: HttpClient`. De esta manera al cargar la app y, por tanto, este componente, tendré a disposicion los métodos de http, entre ellos el que nos permite hacer las peticiones.

```
constructor( private http: HttpClient ) {
    console.log('Constructor del home cargado');
    this.http.get('https://restcountries.eu/rest/v2/lang/es');
  }
```

De esta manera tenemos la información hecha, pero necesitamos suscribirnos/escuchar esta petición para poder manejar los datos que devuelve. Para ello al metodo get le añadimos a continuación un submétodo 'subscribe' en el cual definiremos mediante una función de flecha una variable en la cual guardaremos la información devuelta por el get.

```
constructor( private http: HttpClient ) {
    console.log('Constructor del home cargado');
    this.http.get('https://restcountries.eu/rest/v2/lang/es')
      .subscribe( respuesta => {
        console.log(respuesta);
      })
  }
```

Una vez que estamos suscritos a esa información podemos llamarla, vamos a definir una variable países y en el constructor guardaremos la respuesta en dicha variable.

```
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  paises: any[] = [];

  constructor( private http: HttpClient ) {
    console.log('Constructor del home cargado');
    this.http.get('https://restcountries.eu/rest/v2/lang/es')
      .subscribe( (respuesta:any) => {
        this.paises = respuesta;
        console.log(respuesta);
      })
  }

  ngOnInit(): void {
  }

}
```

Ahora ya podemos acceder a la información en el html:

```
<ul>
    <li *ngFor="let pais of paises">
        {{ pais.name }} - {{ pais.population | number }}
    </li>
</ul>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 96. Actualización - Token para uso de servicios Spotify

Para trabajar con la información o los servicios de Spotify nosotros utilizamos un token que ellos nos van a generar. Tendremos que hacer una petición para obtener un token.

Nos vamos a https://developer.spotify.com/documentation/. Hay varias formas de trabajar con la API de Spotify, vía web API y otras maneras, nosotros para este caso usaremos WEB API, hacemos click ahí y nos vamos a Guides -> Autorization Guide y bajamos hasta Client Credentials Flow.

Como vemos ahí el flow es sencillo, solicitamos un token proporcionando nuestros CLIENT ID y CLIENT ID SECRET, nos devuelve el token y en la solicitud a la api proporcionamos dicho token, devolviéndonos la información en formato JSON.

Para obtener el token haremos una petición `POST https://accounts.spotify.com/api/token` añadiendo en el Body->x-www-form-urlencoded la key 'grant_type' y en VALUE 'client_credentials', además necesitamos darle las keys 'client_id' y 'client_secret' con los VALUE que disponemos en nuestra cuenta de developer de Spotify. Nos devolverá información con el 'access_token' que expira cada hora 'expires_in:3600'.

Guardaremos esto en el Postman para tener la referencia.

[Volver al Índice](#%C3%ADndice-del-curso)

## 97. HTTPClient - Service: Conectándonos a Spotify

Vamos a trabajar con nuestra primera interacción con Spotify. Nos dirijimos a https://developer.spotify.com/console/ -> Browse

Para practicar vamos a usar la petición GET que nos da los nuevos lanzamientos de Spotify `GET	/v1/browse/new-releases` porque es lo que vamos a mostrar en el home. Si hacemos click en el enlace de esa petición nos lleva a una página donde podemos hacer pruebas. Ahí podemos definir algunas variables para definir la respuesta que queremos, como el país, la cantidad de datos que queremos que devuelva y a partir de cual registro queremos que devuelva valores (country,limit,offset). en OAuth Token podemos tocar para generar el token que utilizaríamos para dicha petición, usará nuestra cuenta de Spotify para genera el token. Si hacemos click en Try It veremos el JSON que genera. Pero todo esto es sólo para testearlo y verlo.

Vamos a testearlo ahora en Postman, copiamos el endpoint `https://api.spotify.com/v1/browse/new-releases` y creamos una nueva solicitud GET en el Postman incluyendo este endpoint, en los Headers tenemos que incluir la key 'Autorization' y en value 'Bearer BQA8T9W7bdw266ilzogCBlseFNIB5yMEpZ4IQTPbRBRH51QsV2RLGwbiyaRQi6gj8X2ZdFUJ4u9A8JI6kZU' siendo la cadena larga el token que nos generamos anteriormente en la lección anterior (podemos regenerarlo si no sirve puesto que tiene una duración de 1 hora). Lo ejecutamos y nos devuelve el JSON con la información.

Para tener esto del lado de Angular, puesto que es una información de una API externa lo ideal es centralizar la información, y para centralizar la información es recomendable crear un servicio, vamos a crearlo con angular CLI:

> ng g s services/spotify --skipTests

Anterior a la versión de Angular nos creaba también la información en el app.module.ts ahora, sin embargo, ahora en el spotify.service.ts nos encontramos:

```
@Injectable({
  providedIn: 'root'
})
```

Esto indica a la aplicación, al cargar el servicio, que debe ser incluído en los providers y hace que no sea necesario incluirlo en los providers dentro del app.module.ts

Vamos a hacer una petición a la api de Spotify, en nuestro spotify.service.ts crearemos una función para hacer una petición http a un endpoint y que quede guardado en una variable para luego poder trabajar con ello:

```
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient) {
    console.log('Spotify Service Listo');
   }
   getNewReleases() {
    this.http.get('https://api.spotify.com/v1/browse/new-releases')
      .subscribe( data => {
        console.log(data);
      });
   }
}
```

Si ahora queremos hacer uso de ello por ejemplo en nuestro componente de la home de esta manera nos acabará dando un error de no token provided:

```
import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  constructor( private spotify: SpotifyService) {
    this.spotify.getNewReleases();
  }

  ngOnInit(): void {
  }

}
```

Entonces lo que necesito es modificar los headers de la petición, regresando a spotify.service.ts en el HttpClient del import incluímos HttpHeaders que nos permitirá esas modificaciones. Con ello podremos crear una constante tipo HttpHeader que nos dejará añadir información en formato de objeto con la información que necesitemos incluir en los headers, en nuestro caso la autorización:

```
const headers = new HttpHeaders({
      'Authorization': 'Bearer BQA8T9W7bdw266ilzogCBlseFNIB5yMEpZ4IQTPbRBRH51QsV2RLGwbiyaRQi6gj8X2ZdFUJ4u9A8JI6kZU'
    });
```

Además tendremos que decirle a nuestra función get que se van a usar esos headers, de tal manera que todo el código de la función quedaría así (vamos a añadir el límite en 20, aunque la verdad es que ya estaba definido así a través del token):

```
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient) {
    console.log('Spotify Service Listo');
   }
   getNewReleases() {

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQA8T9W7bdw266ilzogCBlseFNIB5yMEpZ4IQTPbRBRH51QsV2RLGwbiyaRQi6gj8X2ZdFUJ4u9A8JI6kZU'
    });
    this.http.get('https://api.spotify.com/v1/browse/new-releases?limit=20', { headers })
      .subscribe( data => {
        console.log(data);
      });
   }
}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 98. Consumiendo información del servicio de Spotify

Si al cargar nuestra aplicación nos diera un error de token tendremos que volver a Postman y hacer una solicitud POST de token de nuevo, y en nuestro spotify.service.ts actualizar el token en el headers.

Vamos a usar unas tarjetas de bootstrap para trabajar con la información que tenemos de la función getNewReleases() que actualmente sólo la tenemos en consola.

Tomaremos información de los álbumes y la mostraremos en tarjetas.

Vamos a modificar de nuevo la función getNewReleases, eliminando el subscribe y que sólo nos devuelva toda la información mediante un return:

```
getNewReleases() {

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQCI4NYAn9ET-5cv61XCybtwihkcnwvPQDafccRAWCW9VpjvRQpxIIl1_rSKkQmQEKJzBvkWUk8MEkDXOeQ'
    });

    return this.http.get('https://api.spotify.com/v1/browse/new-releases?limit=20', { headers });
    
   }
```

De esta manera, donde yo llame a la función getNewReleases() puedo añadirle el .suscribe, por ejemplo en el home.component.ts, esto nos permitiría, por ejemplo, introducir un "loading" para manejar tiempos de carga en el constructor del componente de home. Declararemos una variable nuevasCanciones para almacenar los items de los albumes que nos devuelve la solicitud http

```
export class HomeComponent {

  nuevasCanciones: any[] = [];

  constructor( private spotify: SpotifyService) {
    this.spotify.getNewReleases()
    .subscribe( (data: any) => {
      this.nuevasCanciones = data.albums.items;
    });
  }

  ngOnInit(): void {
  }

}
```

Ahora podremos montar nuestro html de la home con bootstrap (cards + badges) y rellenando la información deseada:

```
<div class="row">
    <div *ngFor="let cancion of nuevasCanciones" class="card col-3">
        <img class="card-img-top" [src]="cancion.images[0].url">
        <div class="card-body">
            <h5 class="card-title">{{ cancion.name }}</h5>
            <p class="card-text">
                <span *ngFor="let artista of cancion.artists" class="badge rounded-pill bg-primary">{{ artista.name }}</span>
            </p>
        </div>
    </div>
</div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 99. Componente de Búsqueda de artistas

Ahora trabajaremos en la sección "search", la idea es crear un buscador para filtrar la búsqueda de los nuevos lanzamientos que recibimos en nuestra app.

Empezamos con el html, introduciremos un input donde haremos la búsqueda, en este input estará la función que nos permitirá "buscar()" y un #termino de referencia que será lo que usemos para buscar los artistas, así que lo que la función hará será "buscar(termino.value)", al definir #termino estamos creando la posibilidad de referenciar al valor que se encuentre en el input. Esto se enviará cada vez que se suelte una tecla pulsada, debido al evento (keyup). Quedando el html y el ts, respectivamente, de esta manera:

```
<div class="row">
    <div class="col">
        <input #termino type="text" (keyup)="buscar(termino.value)" class="form-control" placeholder="Buscar artista..." name="" id="">
    </div>
</div>
```

```
import { Component } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {

  constructor() { }

  buscar(termino: string) {
    console.log(termino);
  };

}
```

A continuación trabajaremos en el servicio para poder gestionar la búsqueda, para ello vamos a https://developer.spotify.com/console/ y en "search" hacemos click en el endpoint que nos ofrece `https://api.spotify.com/v1/search`, para la generación de token las variables que nos ofrece son q* (el término a buscar, por ejemplo Metallica), type* (si es una canción o artista), market (el país del mercado, de esto pasaremos), límit (límite de 15, pondremos), si damos a "try it" y da error tendremos que generar el token de nuevo en esa misma página.

Regresamos a nuestro servicio de spotify spotify.service.ts y tendremos que crear un nuevo servicio getArtista(), para este servicio necesitaré el término de búsqueda. El código será prácticamente igual al que ya teníamos de getNewReleases(), cambiará el return, puesto que la url de petición get es distinta, cogeremos la url del ejemplo que teníamos en la consola de Spotify Developers, y como esa url recibe un término usaremos la variable para generar la url deseada. El string de la url la delimitaremos por backticks para poder concatenar la variable. Es decir, originalmente la cadena es: `https://api.spotify.com/v1/search?q=ed%20sheeran&type=artist&limit=15` y en lugar de eso quedaría de esta manera:

```
getArtista( termino: string ) {

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQDt7vsa4s73U0L4PpmZEHm-PwckR1TWg4AP30YMDoMNB3ijj714erHbSt6mVXZaDjvLj_OBKCPqft3fgno'
    });

    return this.http.get(`https://api.spotify.com/v1/search?q=${ termino }&type=artist&limit=15`, { headers });

   }  
```

Regresamos al search.component.ts puesto que para usar el servicio necesito inyectarlo en el constructor, nos aseguramos de que lo importe de nuestro servicio spotify.service.ts y así ya podemos usarlo en nuestra función buscar, con sus métodos get, etc. Entonces al llamar al servicio getArtists recibiendo el término, ya podemos suscribirnos a los datos que devuelven para manipularlos en nuestro html. 

Reutilizaremos el html de la home y lo adaptaremos a lo que queremos en el search, más adelante optimizaremos todo el código porque estamoos reutilizando mucho. El html necesita ser pulido en temas de estilo, así como controlar que no reciba imágenes rotas, pero se arreglará más adelante, queda tal que así, de momento:

```
<div class="row">
    <div class="col">
        <input #termino type="text" (keyup)="buscar(termino.value)" class="form-control" placeholder="Buscar artista..." name="" id="">
    </div>
</div>

<div class="row">
    <div *ngFor="let artista of artistas" class="card col-3 m-2">
        <img class="card-img-top" [src]="artista.images[0].url">
        <div class="card-body">
            <h5 class="card-title">{{ artista.name }}</h5>
        </div>
    </div>
</div>
```

Y el código del search.component.ts tal que así:

```
import { Component } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {

  artistas: any[] = [];

  constructor(private spotify: SpotifyService) { }

  buscar(termino: string) {
    console.log(termino);
    this.spotify.getArtista( termino )
      .subscribe( (data: any) => {
        console.log(data);
        this.artistas = data.artists.items;
      });
  };

}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 100. Operador Map de los Observables

En esta lección haremos todas las optimizaciones pendientes. Vamos a comenzar con el operador map. ¿Para qué sirve?

Vamos a trabajarlo en el home.component.ts. En este componente cuando llamamos a la función del servicio getNewReleases nos suscribimos a una "data" que es el resultado de la petición http GET, esto es un JSON con una gran cantidad de información por lo general. El operador map se adjuntará a nuestra petición u observable (http.get) lo va a filtrar y nos va a devolver únicamente lo que a nosotros nos sirva. Es decir, toma toda la información pero la cambia, la adapta a lo que sólo necesitamos, pero en esencia sigue estando toda la información ahí.

Para implementarlo en nuestra app vamos a usarlo en nuestro home.component.ts, actualmente a la función le especificamos que queremos data.albums.items, deberíamos poder pedir simplemente la data y que nos la de ya filtrada de antemano, y como esto se define en el servicio vamos a spotify.service.ts para definir el operador map allí.

Primeramente hay que importar el operador map, esto no está en las librerías de Angular, sino de Reactive Extensions (RxJS), que son unas librerías de JavaScript para manipular observables y funciones asíncronas. Nosotros necesitamos el "map" que se encuentra en la sección "operators" de rxjs `import { map } from 'rxjs/operators';`.

En nuestro observable ahora podemos añadirle una función pipe que nos sirve para filtrar, y esta función recibe como argumento la función map para definir el filtrado deseado, este map recibirá la información en bruto que nos devuelve el método get previo en la "data", la cual es consecuencia de una función de flecha en la cual especificaremos el return que deseamos, en este caso queríamos solo los items que pertenecen a la propiedad albums de todo el JSON que devuelve el GET (data), resumiendo el código quedaría tal que así:

```
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient) {
    console.log('Spotify Service Listo');
   }
   getNewReleases() {

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQDt7vsa4s73U0L4PpmZEHm-PwckR1TWg4AP30YMDoMNB3ijj714erHbSt6mVXZaDjvLj_OBKCPqft3fgno'
    });

    return this.http.get('https://api.spotify.com/v1/browse/new-releases?limit=20', { headers })
      .pipe( map( data => {
        return data['albums'].items;
      }));
   }
```

Podemos abreviar un poco más las funciones, porque cuando las funciones de flecha tienen una sóla línea, y esa línea es un return (como es nuestro caso) se pueden definir así:

```
getNewReleases() {

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBMAjYA59yQOox2qI3E2t0BrXjObYO-00kpZNAqbRGIzBidws586Kuzpwt8kCDsusoyj82q5jU-1pcavhw'
    });

    return this.http.get('https://api.spotify.com/v1/browse/new-releases?limit=20', { headers })
      .pipe( map( data => data['albums'].items ) );
   }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 101. Centralizar las peticiones hacia Spotify

Ahora optimizaremos el código duplicado, puesto que en el spotify.service.ts tenemos funciones prácticamente idénticas.

Para empezar tenemos una "query" o condición que se distingue de un lugar a otro, pero el resto de la url es la misma (hablando de las peticiones GET). Vamos a centralizar esto, para ello vamos a realizar una función que llamaremos getQuery(), la cual recibirá el query di tipo string, que será el código exclusivamente único para las funciones getNewReleases() y getArtista(). Crearemos una constante "url" que tendrá como objetivo centralizar la petición, es decir, si cambiara la versión de v1 a v2 solo tendríamos que cambiar esa constante y se actualizaría en el resto de cadenas.

Esa constante además se completará con una variable, que será la query particular de cada función. Buscamos unificar y centralizar.

Ahora mediante variables podríamos tener la función definida así:

```
getQuery( query: string ) {

    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBMAjYA59yQOox2qI3E2t0BrXjObYO-00kpZNAqbRGIzBidws586Kuzpwt8kCDsusoyj82q5jU-1pcavhw'
    });

    return this.http.get(url, { headers });

  }
```

Llegados a este punto es mucho más claro simplemente leer el código ya terminado, todo centralizado y unificado, el archivo spotify.service.ts queda:

```
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient) {
    console.log('Spotify Service Listo');
   }
   
  getQuery( query: string ) {

    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBMAjYA59yQOox2qI3E2t0BrXjObYO-00kpZNAqbRGIzBidws586Kuzpwt8kCDsusoyj82q5jU-1pcavhw'
    });

    return this.http.get(url, { headers });

  }

  getNewReleases() {

    return this.getQuery('browse/new-releases?limit=20')
      .pipe( map( data => data['albums'].items ) );
  }

  getArtista( termino: string ) {

    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`)
      .pipe( map( data => data['artists'].items ) );    
  }

}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 102. Verificación de imagen y pipe para manejar las imágenes

Toca optimizar las imágenes ahora, en nuestro componente search tenemos un problema, cuando hacemos una búsqueda de artista y ese artista no tienen una imagen asociada o bien no sale, o sale rota, tenemos error en consola de que no puede encontrarlo, etc.

Podemos resolver esto de muchas maneras, con alguna función, con un operador map que verifique los datos, etc. Nosotros en este caso vamos a usar un pipe.

Vamos a crearlo con el angular CLI

> ng g p pipes/noimage --skipTests

Vamos a search.component.html la idea es que `[src]="artista.images[0].url"` sea validado antes de ser mostrado. Vamos a configurar nuestro noimage.pipe.ts.

El value que recibirá será el array de imágenes, no tendrá argumentos adicionales y devolverá un string (la url de la imagen). Haremos varias validaciones, la primera sería si el value que recibe no es válido, es decir, recibe un null, undefined o algo similar, lo que devuelva sea una imagen por defecto, en el material adjunto de la sección hay un noimage.png que usaremos para esto. La copiaremos a src/ap/assets/img/noimage.png. Nótese que la ruta relativa se considera que empieza desde el index.html, por eso para localizar la imagen por defecto que queremos devolver empezamos desde "assets".

Si por contrario si viniera una imagen, es decir la longitud de esa variable es mayor de 0, devolveremos la url de la posición 0 del array de imágenes. Pero si no fuera así también devolveríamos la imagen por defecto.

Para usarlo, como lo que devuelve en caso correcto es `images[0].url` tendremos que eliminarlo del html y generarlo aplicándole el pipe, de tal manera que el código html quedaría con `[src]="artista.images | noimage"`. El código tanto del pipe como del html quedarían como a continuación:

```
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimage'
})
export class NoimagePipe implements PipeTransform {

  transform( images: any[] ): string {
    
    if ( !images ) {
      return 'assets/img/noimage.png'; // Se considera que los path relativos empiezan en el index.html
    }
    if ( images ) {
      return images[0].url;
    } else {
      return 'assets/img/noimage.png';
    }
  }

}
```

```
<div class="row">
    <div class="col">
        <input #termino type="text" (keyup)="buscar(termino.value)" class="form-control" placeholder="Buscar artista..." name="" id="">
    </div>
</div>

<div class="row">
    <div *ngFor="let artista of artistas" class="card col-3 m-2">
        <img class="card-img-top" [src]="artista.images | noimage">
        <div class="card-body">
            <h5 class="card-title">{{ artista.name }}</h5>
        </div>
    </div>
</div>
```

NOTA IMPORTANTE: Parece ser que de esta manera no terminaba de funcionar, otro alumno encontró una solución, dejo el código del noimage.pipe.ts modificado:

```
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimage'
})
export class NoimagePipe implements PipeTransform {

  transform( images: any[] ): string {
    return images.length === 0 ? 'assets/img/noimage.png' : images[0].url;
  }
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 103. Componente de tarjetas

Seguimos optimizando código, en este caso tenemos duplicado el html del home y del search, son códigos prácticamente iguales.

Así que vamos a centralizar el código, creándonos un nuevo componente:

> ng g c components/tarjetas --skipTests

Copiaremos todo el código de home.component.html y lo copiaremos en tarjetas.component.html. Entonces en home.component.html ya podemos llamar a `<app-tarjetas></app-tarjetas>`. Como anteriormente home trabajaba con las nuevasCanciones definidas en su ts, ahora deberemos pasarle esos valores a app-tarjetas `<app-tarjetas [items]="nuevasCanciones"></app-tarjetas>`

En tarjetas.component.ts tendremos que recibir esa información con un @input que deberemos importar desde @Angular/core, que hemos llamado "items", por tanto tendremos que cambiar html y adaptar de cancion como variable a items.

Lo mismo para el componente search, solo que lo que le pasaremos como valor de "items" será artista en lugar de nuevasCanciones. Quedarían así entonces los archivos:

tarjetas.component.ts

```
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styleUrls: ['./tarjetas.component.css']
})
export class TarjetasComponent {

  @Input() items: any[] = [];

  constructor() { }

}

```

tarjetas.component.html

```
<div class="row m-5">
    <div *ngFor="let item of items" class="card col-3">
        <img class="card-img-top" [src]="item.images | noimage">
        <div class="card-body">
            <h5 class="card-title">{{ item.name }}</h5>
            <p class="card-text">
                <span *ngFor="let artista of item.artists" class="badge rounded-pill bg-primary">{{ artista.name }}</span>
            </p>
        </div>
    </div>
</div>
```

home.component.html

```
<app-tarjetas [items]="nuevasCanciones"></app-tarjetas>
```

search.component.html

```
<div class="row">
    <div class="col">
        <input #termino type="text" (keyup)="buscar(termino.value)" class="form-control" placeholder="Buscar artista..." name="" id="">
    </div>
</div>

<app-tarjetas [items]="artistas"></app-tarjetas>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 104. Creando un loading component

En esta lección vamos a crear un loading que diga al usuario que espere por favor mientras la data viene de nuestros servicios. Esto será un nuevo componente que vamos a crear como de uso general, y que incluiremos en el directorio shared, por tanto.

> ng g c components/shared/loading --skipTests

Ahora podemos llamar a `<app-loading></app-loading>` en home.component.html, por ejemplo, al principio de todo antes del resto del código.

Para generarl algo visualmente bonito en el html del nuevo componente loading vamos a utilizar fontawesome.com, nos vamos al sitio web y lo instalamos mediante el CDN a través de "Get Started". (Nota: ha cambiado un poco,hay que registarse y el link es tipo <script>).

Incluímos el código en el index.html.

A continuación crearemos el html del loading.component.html:

```
<div class="row text-center animated fadeIn">
    <div class="col">
        <i class="fas fa-sync fa-spin fa-5x"></i>
    </div>
</div>
```

La idea de este loading es que sólo aparezca cuando estamos cargando información, y cuando ya tengamos la información debería desaparecer.

Esto lo vamos a controlar con un *ngIf, asi que para ello nos vamos al home.component.ts y creamos una nueva propiedad loading: boolean; la inicializaremos en el constructor como true y en la función que cargar los lanzamientos al final la definiremos como false (es decir, cuando ya haya realizado la funcion, lo que significaría que ya se cargaron todos los datos), quedaría así:

```
export class HomeComponent {

  nuevasCanciones: any[] = [];
  loading: boolean;

  constructor( private spotify: SpotifyService) {

    this.loading = true;

    this.spotify.getNewReleases()
    .subscribe( (data: any) => {
      this.nuevasCanciones = data;
      this.loading = false;
    });
  }

  ngOnInit(): void {
  }

}
```

Ahora puedo regresar al home.component.html e implementar la condicional:

```
<app-loading *ngIf="loading"></app-loading>
<app-tarjetas [items]="nuevasCanciones"></app-tarjetas>
```

Haremos lo mismo para el search.

[Volver al Índice](#%C3%ADndice-del-curso)

## 105. Página del artista, nueva ruta, parámetro por url y servicio

En esta lección definiremos que en la página de search, cuando hagamos click en alguno de los artistas que busquemos nos lleve a la página del artista.

En el setup inicial del proyecto ya habíamos dejado creado el componente "artista".

Necesitamos el ID del artista para cuando vayamos a hacer click, porque en https://developer.spotify.com/console/artists/ el parámetro que requiere el endpoint es el ID. Este ID ya lo tenemos porque forma parte de uno de los atributos que componen el objeto de artista que recibimos con el GET en la petición http.

Para comenzar necesitamos definir una nueva ruta y que esta controle el parámetro del ID que se pasa por la url.

En app.routes.ts añadimos la nueva ruta indicando que recibirá el id como parámetro:

```
export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'search', component: SearchComponent },
    { path: 'artist/:id', component: ArtistaComponent },
    { path: '', pathMatch: 'full', redirectTo: 'home'}, // Cualquier otro path no definido nos redireccionará al home
    { path: '**', pathMatch: 'full', redirectTo: 'home'} // Cualquier otro path no definido nos redireccionará al home
];
```

Ahora en tarjetas.component.html añadiremos en el html de la tarjeta una clase css llamada "puntero" que ya tenemos definida en nuestro archivo styles.css, esto hará que el puntero cambie cuando nos situemos sobre el elemento.

Vamos a añadir el evento click en el elemento, pero nosotros lo que manejamos en tarjetas es un "item", este item será a veces artistas y a veces canciones, en función de en qué página se cargue el componente de la tarjeta, si en el home o en el search, pero uno de los atributos de ese objeto es "type", que define esto, o bien "album" o "single" o "artist", lo cual nos servirá para discriminar en nuestro código si se trata de un artista, y entonces poder capturar el ID de ese elemento para poder enviarlo por la url posteriormente.

Así pues, en tarjetas.component.html incluiremos el evento (click) que ejecutará la funcion de verArtista( item ) al cual se le pasará el item, y posteriormente controlarermos ese item como hemos comentado.

```
<div class="row m-5 animated fadeIn">
    <div (click)="verArtista( item )" *ngFor="let item of items" class="card col-3 puntero">
        <img class="card-img-top" [src]="item.images | noimage">
        <div class="card-body">
            <h5 class="card-title">{{ item.name }}</h5>
            <p class="card-text">
                <span *ngFor="let artista of item.artists" class="badge rounded-pill bg-primary">{{ artista.name }}</span>
            </p>
        </div>
    </div>
</div>
```

Ahora en tarjetas.component.ts hacemos la lógica de la función, si al hacer click es de tipo artista guardaremos su ID, por el contrario iremos al primer artista del album o la canción y cogeremos su ID (NOTA PERSONAL: lo suyo sería pulirlo y añadir la funcion a cada artista en los badges, estamos definiendo esto para que coja algo, parece ser).

```
verArtista ( item: any ) {
    let artistaID;

    if ( item.type === 'artist' ) {
      artistaID = item.id;
    } else {
      artistaID = item.artists[0].id;
    }
    console.log(artistaID);

  }
```

Ahora solo nos falta redireccionar a la página del artista cuando hagamos click (en lugar de hacer console log como estamos haciendo). Para ello necesitamos importar el Router en el componente de tarjeta e inyectarlo en el constructor. Ahora podemos usar el metodo navigate de router para navegar, como recibe un parámetro la url se pone entre []. El primer parámetro es la ruta a la que quiero navegar (/artist) y el segundo el ID que le tengo que pasar a la url, artistID. Aún no tenemos definido el html del artista, pero ya funciona puesto que si hacemos click nos llevará a una url tipo `http://localhost:4200/#/artist/7Hd38PVp634oGEb9pIDs5d`. El código quedaría así de momento:

```
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styleUrls: ['./tarjetas.component.css']
})
export class TarjetasComponent {

  @Input() items: any[] = [];

  constructor( private router: Router ) { }

  verArtista ( item: any ) {
    let artistaID;

    if ( item.type === 'artist' ) {
      artistaID = item.id;
    } else {
      artistaID = item.artists[0].id;
    }
    
    this.router.navigate(['/artist', artistaID]);

  }

}
```

Vamos a definir la recepción del ID en el componente del artista, para así luego poder usarlo para ver su página. En artista.component.ts importamos ActivatedRoute de @angular/router y lo inyectamos en el constructor, esto nos permite que en el constructor podamos usar ActivatedRoute para suscribirnos/escuchar si hay cambios en la url, si se añaden parámetros, en este caso detectar si se está pasando el ID. De momento lo dejaremos para que podamos verlo por consola, el código quedaría tal que:

```
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})
export class ArtistaComponent {

  constructor( private router: ActivatedRoute ) {

  this.router.params.subscribe( params => {
    console.log(params['id']);
  });

  }
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 106. Obtener artista de Spotify

Vamos a definir el servicio que nos permita recibir el artista, para ello antes de nada tendremo que volver a https://developer.spotify.com/console/artists/ para ver el endpoint que necesitamos, en este caso sería `https://api.spotify.com/v1/artists/{id}`

Vamos a reutilizar el código de getArtista, debió ser definido en su momento como getArtistas en plural, puesto que nos devuelve uno o varios artistas, y lo que queremos generar ahora es una función que nos devuelva uno solo y la llamaremos getArtista. Que en lugar de termino recibirá un id de tipo string.

Reutilizando el código lo único que deberemos modificar sería el string que recibe como argumento la función getQuery, quedando la función completada tal que:

```
getArtista( id: string ) {

    return this.getQuery(`artists/${id}`)
      .pipe( map( data => data['artists'].items ) );    
  }
```

Lo ideal sería saber qué es lo que nos devuelve el query, para poder validarlo correctamente o aplicar el map, para esto podríamos usar Postman, por ejemplo, en nuestro caso vamos a comentar la línea del .pipe(map) y a cerrar la función this.getQuery con punto y coma. Y vamos a ver qué devuelve el getQuery directamente desde nuestra aplicación de Angular.

```
getArtista( id: string ) {

    return this.getQuery(`artists/${id}`);
      //.pipe( map( data => data['artists'].items ) );    
  }
```

Volvemos a artista.component.ts, habíamos dejado definido que nos mostrase por consola el id de params, ya sabemos que se recibe correctamente, así que vamos a usar ese id para llamar al método getArtista y ver que información devuelve, vamos a crear un método aparte en el mismo componente que se llame también getArtista, que recibirá un id de tipo string, lo definiremos fuera del constructor para no sobrecargarlo de métodos, ese método llamará al servicio, por lo tanto necesitamos importarlo e inyectarlo en el componente, una vez hecho esto ya podemos llamar al servicio en la nueva función, el servicio llamará a la función getArtista pero a la del servicio, que recibirá el id y al cual nos suscribiremos para recibir la respuesta http.

Una vez hecho esto entonces en el ActivatedRoute del constructor ya podemos usar la escucha del id para pasarselo a la función getArtista como parámetro y así poder ver toda la información que nos devuelve del artista dado ese id.

Ahora en la consola tenemos toda la información relativa a ese artista, incluso una url externa que nos lleva al perfil del artista en Spotify  . Realmente ni haría falta filtrarlo por el map, puesto que ya nos devuelve un único objeto JSON muy ordenado y simple con un montón de información útil. Así que haremos un poco de maquetación html para mostrar la información de manera visualmente agradable, en lugar del "artista works!" :)

Por un lado vamos a maquetar un html, y por el lado del componente vamos a declarar una variable "artista" que sea un objeto vacío que usaremos para almacenar los datos que luego querremos renderizar en el html, recordamos que esta información la podemos manejar tal cual porque ya la recibimos de manera que podemos usarla sin problemas, si fuese complicado de acceder a los datos tendríamos que aplicar el pipe/map en el servicio. Entonces hasta aquí nuestro componente quedaría de esta manera:

```
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';
@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})
export class ArtistaComponent {

  artista: any = {};

  constructor( private router: ActivatedRoute,
               private spotify: SpotifyService ) {

  this.router.params.subscribe( params => {
    this.getArtista( params ['id']);
  });

  }

  getArtista( id: string ) {

    this.spotify.getArtista( id )
      .subscribe( artista => {
        console.log(artista);
        this.artista = artista;
      });
  }
}
```

Vamos a realizar una maquetación de la información. Incluiremos también el loading para que no nos tire un error de que tratar de cargar variables que aun no se encuentran cargadas, es decir, cuando se aplica el pipe en la imagen da error de que no encuentra dicha imagen, para ello declaramos una variable loadingArtist que haga de bandera y la incluimos en el constructor y en el observador de tal manera que obligamos a una pre-carga de los datos, a continuación, respectivamente, el código del componente actualizado y del html, respectivamente.

```
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';
@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})
export class ArtistaComponent {

  artista: any = {};
  loadingArtist: boolean;
  constructor( private router: ActivatedRoute,
               private spotify: SpotifyService ) {
                 this.loadingArtist = true;
  this.router.params.subscribe( params => {
    this.getArtista( params ['id']);
    
  });

  }

  getArtista( id: string ) {
    this.loadingArtist = true;
    this.spotify.getArtista( id )
      .subscribe( artista => {
        console.log(artista);
        this.artista = artista;
        this.loadingArtist = false;
        
      });
  }
}

```

```
<app-loading *ngIf="loadingArtist" class="m-5"></app-loading>
<div class="row animated fadeIn" *ngIf="!loadingArtist">

    <div class="col-2">
        <img [src]="artista.images | noimage" class="img-thumbnail img-circle" alt="">
    </div>

    <div class="col">
        <h3>{{ artista.name}}</h3>
        <p>
            <a [href]="artista.external_urls.spotify" target="_blank">Ir a la página del artista</a>
        </p>
    </div>

    <div class="col-4 text-right">
        <button routerLink="/search" class="btn btn-outline-danger">
            Regresar
        </button>
    </div>

</div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 107. Servicio: Top-tracks

En esta sección vamos a trabajar usando las canciones más exitosas de un artista en particular. Usando el método `GET	/v1/artists/{id}/top-tracks` de la consola de Spotify for Developers, como hemos visto en otros ejemplos anteriores. Hasta ahora hemos hecho pruebas para ver cómo devuelve los datos el endpoint, para luego mapearlo o no, podemos usar Postman para esto también.

Primero vamos a crear un nuevo servicio para obtener esa información, vamos a usar como base uno de los que teníamos previamente, getArtista(). Recibirá un ID y actualizamos la parte del query para definir la cadena de endpoint que necesitamos en este caso. Lo útil de la función getQuery es que así podemos centralizar todas las peticiones al mismo servicio.

```
getTopTracks( id: string ) {

    return this.getQuery(`artists/${ id }/top-tracks`);
      //.pipe( map( data => data['artists'].items ) );    
  }
```

Guardamos cambios y vamos a artista.component.ts y creamos un método para llamar a ese servicio para obtener esa información, asimismo llamamos a la función en el constructor del componente para poder luego suscribirnos a él con el método.

```
                  this.getTopTracks( params ['id']);
```

```
getTopTracks( id: string ) {

    this.spotify.getTopTracks( id )
      .subscribe( topTracks => {
        console.log(topTracks)
      });
  }
```

Guardamos cambios y vemos que nos da un error, porque ese endpoint necesita como campo obligatorio el país, esto podríamos haberlo sabido antes si hubieramos hecho pruebas con Postman, como hemos mencionado anteriormente. Haciendo uso de la consola de Spotify Developer podemos deducir y ver que la url que requiere tiene como argumento el país, de tal manera que lo actualizamos en nuestro getQuery del servicio quedando `return this.getQuery(`artists/${ id }/top-tracks?country=us`);`

Ahora ya no nos da error y por el console.log que hicimos de los datos podemos ver que devuelve un objeto "tracks".

Nosotros en realidad queremos un array limpio con sólo las canciones, así que en este caso vamos a volver a utilizar el pipe map. Descomentamos el ejemplo que teníamos comentado resultado de haber copiado el método de getArtista, adaptándolo para que en lugar del objeto 'artist' filtre el objeto 'tracks'.

```
getTopTracks( id: string ) {

    return this.getQuery(`artists/${ id }/top-tracks?country=us`)
      .pipe( map( data => data['tracks'] ) );    
  }
```

Ahora podemos usar esos datos en nuestro componente, declaramos una nueva variable array que haga de contenedor `topTracks: any[] = [];` y podemos almacenar los datos suscritos en ella.

```
getTopTracks( id: string ) {

    this.spotify.getTopTracks( id )
      .subscribe( topTracks => {
        console.log(topTracks);
        this.topTracks = topTracks;
      });
  }
```

Aquí se nos pide una tarea, y es que en el artista.component.html se añada una maquetación con este esqueleto:

```
<div class="row">
        <div class="col">
            <table class="table">
                <thead>
                    <tr>
                        <th>Foto</th>
                        <th>Album</th>
                        <th>Canción</th>
                        <th>Vista Previa</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
```

La tarea consiste en tomar la información de los toptracks y llenar los campos de las celdas de la tabla (excepto foto y vista previa que se resolverá más adelante). Bastante sencillo, la solución era:

```
<tr *ngFor="let track of topTracks">
  <td></td>
  <td>{{ track.album.name }}</td>
  <td>{{ track.name }}</td>
  <td></td>
</tr>
```

Ahora vamos a trabajar con la fotografía, que es como hemos hecho anteriormente, [src] dinámico al que le pasamos la url de la imagen que es uno de los argumentos del array, le pasaremos el pipe personalizado no-image por si hubiera algún problema. También añadiremos una clase css img-thumb que ya viene definida en el archivo styles.css de la lección, que lo reduce a 50px de alto y ancho, porque del servicio vienen con mucha resolución.

```
<img [src]="track.album.images | noimage" [alt]="track.album.name" class="img-thumb">
```

Por último vamos a añadir una vista previa, en el array ya hay una propiedad que es "preview-url" que usaremos para esto.

NOTA: por un lado he descubierto que el endpoint está obsoleto aunque sigue funcionando, ahora es "markets" en lugar de "country", siendo la query así: `return this.getQuery(`artists/${ id }/top-tracks?market=US`)` por otro lado hay mucho casos en el que no hay preview_url y devuelve null, así que añadiré yo un arreglo a la maquetación para controlar que no de error con un *ngIf, nótese en el código html final del ejercicio. Realmente no da error interno de la app ese null, pero añadí información con un span.

Así pues, con una etiqueta de HTML5 que se llama audio haremos la llamada a la url de la canción para el reproductor. El atributo de la etiqueta 'controls' incluye los controles de play, descargar, etc.

El código final, hasta el momento, sería:

```
<div class="row m-5">
        <div class="col">
            <table class="table">
                <thead>
                    <tr>
                        <th>Foto</th>
                        <th>Album</th>
                        <th>Canción</th>
                        <th>Vista Previa</th>
                    </tr>
                </thead>
                <tbody>
                    <tr *ngFor="let track of topTracks">
                        <td>
                            <img [src]="track.album.images | noimage" [alt]="track.album.name" class="img-thumb">
                        </td>
                        <td>{{ track.album.name }}</td>
                        <td>{{ track.name }}</td>
                        <td>
                            <audio [src]="track.preview_url" controls></audio><br>
                            <span *ngIf="!track.preview_url">Vista previa no disponible</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 108. Widgets de Spotify

En esta sección cambiaremos la etiqueta audio del ejercicio anterior por un widget de Spotify. El widget necesita una app de Spotify conectada y funcionando, para poder funcionar en la app. Tiramos de documentación: `https://developer.spotify.com/documentation/widgets/` Adding a Widget -> Standard HTML Pages -> Embed `https://developer.spotify.com/documentation/widgets/generate/embed/` , es incluir un iframe... no tiene mucho más, selecionamos modo compacto y poco más, usamos el iframe de ejemplo que viene y lo ponemos tal cual en nuestro html para ver el efecto.

`<iframe src="https://open.spotify.com/embed/album/1DFixLWuPkv3KT3TnV35m3" width="300" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>`

`<iframe src="https://open.spotify.com/embed?uri=spotify:album:1DFixLWuPkv3KT3TnV35m3" width="300" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>`

NOTA: Se ve que se actualizó la manera y el endpoint puede ser de distintas maneras, seguiré la del profesor, aunque se puede hacer como la primera versión que puse, la segunda también funciona.

Como vimos en la sección de pipes, es posible que nos de error al tratar de generar la url dinámicamente, tratándola como no segura, en los recursos de esta sección se añadió el archivo typescript del pipe para incluirlo (o cogerlo de la versión anterior del proyecto.) Lo copiamos al directorio pipes del proyecto y lo importamos en el app.module.ts para que funcione (import y declaración).

Ahora podemos filtrar con el pipe la url de esta manera:`<iframe [src]="track.uri | domseguro:'https://open.spotify.com/embed?uri=' " width="300" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>`

El problema es que se ve muy engorroso, porque el pipe original customizado lo que hacía era componer la cadena de url cogiendo un valor y la propia cadena y concatenándolos, pero como ese pipe lo vamos a usar para este proyecto lo vamos a personalizar, alterando el pipe y definiendo la cadena que no cambia (el principio de la url) como constante, de esa manera quedará mucho más limpio, en el domseguro.pipe.ts:

```
transform( value: string): any {
    const url = 'https://open.spotify.com/embed?uri=';
    return this.domSanitizer.bypassSecurityTrustResourceUrl( url + value );
  }
```

Y ahora en el html queda mucho más sencillo:

```
<td>
    <!-- <audio [src]="track.preview_url" controls></audio><br> -->
    <iframe [src]="track.uri | domseguro" width="300" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
    <span *ngIf="!track.preview_url">Vista previa no disponible</span>
</td>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 109. Manejo de errores de un observable

En esta clase manejaremos algunos errores, supongamos que el token de Spotify expira, se hace una petición mal, cualquier cosa.

Vamos a poner una caja de texto en el home para simular un error.(home.component.html). Lo dejaremos así, pero podríamos crear un componente sólo para esto y otros métodos de control de errores.

```
<app-loading *ngIf="loading"></app-loading>
<div class="alert alert-danger">
    <h3>Error</h3>
    <p>Hola Mundo</p>
</div>
<app-tarjetas [items]="nuevasCanciones"></app-tarjetas>
```

La idea es declarar en el componente home una variable booleana 'error' para luego mediante un *ngIf en el frontal poder controlar la bandera.

En el componente temenos la función getNewReleases() la cual tiene un subscribe, le podemos pasar una nueva funcion de flecha como argumento para que reciba esa variable de error (es un argumento opcional del método subscribe). Así podremos identificar que tipo de error nos devuelve la API (en este caso la de Spotify, pero cambiará para las distintas API que usemos en el futuro.)Con los console log podemos ver el objeto de error y que parámetros tiene para así poder acceder a la info que nos interese, por ejemplo el mensaje de error "errorServicio.error.error.message"

```
constructor( private spotify: SpotifyService) {

    this.loading = true;

    this.spotify.getNewReleases()
    .subscribe( (data: any) => {
      this.nuevasCanciones = data;
      this.loading = false;
    }, (errorServicio) => {
      this.loading = false;
      this.error = true;
      console.log(errorServicio.error.error.message);
    });
  }
```

Podemos declarar en el componente una variable string que nos sirva para almacenar ese mensaje de error mensajeError: string;

Quedando el html:

```
<app-loading *ngIf="loading"></app-loading>
<div *ngIf="error" class="alert alert-danger animated fadeIn">
    <h3>Error</h3>
    <p>{{ mensajeError }}</p>
</div>
<app-tarjetas [items]="nuevasCanciones"></app-tarjetas>
```

Y el componente:

```
import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  nuevasCanciones: any[] = [];
  loading: boolean;
  error: boolean = false;
  mensajeError: string;

  constructor( private spotify: SpotifyService) {

    this.loading = true;

    this.spotify.getNewReleases()
    .subscribe( (data: any) => {
      this.nuevasCanciones = data;
      this.loading = false;
    }, (errorServicio) => {
      this.loading = false;
      this.error = true;
      this.mensajeError = errorServicio.error.error.message;
    });
  }

  ngOnInit(): void {
  }

}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 110. Generar Token de Spotify de forma automática

La API de Spotify solo permite peticiones POST desde un servidor, por eso podemos hacerla desde Postman (que monta su propio servidor) y luego nos hemos visto obligados a añadirla manualmente como una constante.

En el curso no se da detalle, probaré a hacerlo personalmente, pero básicamente monta en un servidor cloud (Heroku) una app que genera el token. Lo probaré.

[Volver al Índice](#%C3%ADndice-del-curso)

## Cuestionario 3: Examen teórico: SpotiApp

8 de 8 Acertadas! :D

[Volver al Índice](#%C3%ADndice-del-curso)

## 111. Código fuente de la sección

Código fuente:

Pueden descargar el código fuente de la sección del material adjunto.

Si tienen curiosidad de como hice el backend para obtener el token, puede descargar el código aquí

https://github.com/Klerith/spotify-get-token

Nota:

Tengo un curso de Node que puede ser un excelente complemento para este curso de Angular, si lo desean, aquí les dejo el enlace

Curso de Node - Descuento para estudiantes

Espero que esta aplicación les gustara mucho!, los veo en el siguiente video

[Volver al Índice](#%C3%ADndice-del-curso)

# Sección 7:Aplicación #4: Lista de deseos - IONIC App
## 112. Introducción a la sección

En esta sección vamos a trabajar con el framework IONIC que usa Angular 2 como corazón para construir una aplicación (en este punto las versiones, seguramente, hayan cambiado) para móviles, que podremos usar en nuestro teléfono móvil. Aprenderemos lo que es el local storage para poder almacenar información (algo similar a las cookies,para persistir información), crearemos la aplicación y la probaremos en nuestro teléfono móvil.

[Volver al Índice](#%C3%ADndice-del-curso)

## 113. ¿Qué aprenderemos en esta sección?

A lo largo de esta sección crearemos una aplicación que correrá en nuestro dispositivo Android o IOS, y con ello aprenderemos sobre:

1. Uso del local storage guardar localmente información para nuestras aplicaciones.
2. Introducción a ionic.
3. Uso del framework de ionic.
4. Uso de servicios que afectan el local storage.
5. Uso de ionic DevApp para desplegar nuestra aplicación
6. Aprenderemos a controlar estilos usando Angular.
7. Manejo de la información a través de servicios ( update, delete, insert y select ) al local storage.
8. Aprenderemos a utilizar pipes impuros para realizar filtros.
9. Entre otras cosas interesantes.

Mientras se avanza en la sección, tendremos tareas y ejercicios prácticos que nos ayudarán a afianzar los conocimientos que iremos adquiriendo clase tras clase.

[Volver al Índice](#%C3%ADndice-del-curso)

## 114. Demostración del resultado de esta sección

Ver video con el proyecto terminado.

[Volver al Índice](#%C3%ADndice-del-curso)

## 115. ¿Qué es ionic?

Si nosotros quisieramos desarrollar aplicaciones para iOS o Android tendríamos que aprender a programar en Java para Android y Swift u Objetive-C para iOS. Nosotros usaremos Angular y algo que nos permita dar forma como venimos haciendo con Bootstrap, IONIC nos permitirá esto y para poder pasar luego la aplicación a esas plataforma tenemos el Apache Cordova, el cual es el encargado de tomar el código de HTML, CSS y JavaScript y generar aplicaciones que se instalan directamente en el dispositivo móvil y se ejecutan como si fueran aplicaciones nativas.

Resumiendo, con nuestros conocimientos en Angular más las directrices para diseño que nos proporciona IONIC (componentes especializados y estilizados), y pasando esto por el Apache Cordova, nos permitirá desplegar nuestras aplicaciones en las diferentes plataformas de aplicaciones (Google PlayStore, la appstore en la web, generar una PWA (página web progresiva)).

[Volver al Índice](#%C3%ADndice-del-curso)

## 116. Nota de actualización

Nota importante:

ionic removió la aplicación llamada ionic DevApp, la cual funcionaba para probar ciertas cosas rápidamente en el dispositivo móvil.

Esta sección no necesita dicha aplicación, eso era un extra para que fuera más divertida, pero desafortunadamente la removieron por lo que les pido que simplemente sigan la sección normalmente pero todo en el navegador web como lo verán en los videos.

Si ionic vuelve a crear una aplicación para hacer pruebas rápidas en el dispositivo, lo añadiré al curso :)

[Volver al Índice](#%C3%ADndice-del-curso)

## 117. Creando el esqueleto de nuestra aplicación

Antes de nada vamos a https://ionicframework.com/ y nos creamos una cuenta gratuita. Esto servía para poder acceder a la aplicación de prueba que se menciona en la sección anterior y que, lamentablemente, ya no está disponible.

Posteriormente instalamos ionic de manera global, al ser de manera global necesitaremos permisos de administrador o superusuario, actualmente lo estoy instalando en linux por tanto sería:

>sudo npm install -g ionic

En la web podemos ver en la sección de Get Started diferentes plantillas iniciales, en blanco, con menú lateral, con tabs... nosotros vamos a usar la de tabs, asi que vamos a nuestro directorio raiz de los proyectos y ejecutamos:

>ionic start deseos tabs

En esta versión de ionic que estoy usando (superior a la del curso) me pregunta en primer lugar que framework quiero usar, me da a elegir entre Angular y React, así que elegiremos Angular (obviamente xD).

Una vez terminada la instalación renombraremos el directorio a 05-deseos.

Para levantar el proyecto es tan sencillo como situarnos en el directorio del proyecto y en la terminar llamar a:

>ionic serve

Tras arreglar un montón de fallos de dependencias por diferencias de versiones ya tenemos desplegada la aplicación, será buena la práctica de usar el inspector del navegador en modo desarrollador con la opción de visualizar como dispositivo móvil, pues esta es nuestra finalidad.

Vamos a realizar una pequeña modificación sólo para ver el resultado, si vamos a src/app y ahí tenemos varios directorios, a simple vista vemos los distintos tabs, vamos a src/app/tab1/tab1.page.html por ejemplo.

```
<ion-header [translucent]="true">
  <ion-toolbar>
    <ion-title>
      Tab 1
    </ion-title>
  </ion-toolbar>
</ion-header>

<ion-content [fullscreen]="true">
  <ion-header collapse="condense">
    <ion-toolbar>
      <ion-title size="large">Tab 1</ion-title>
    </ion-toolbar>
  </ion-header>

  <app-explore-container name="Tab 1 page"></app-explore-container>
</ion-content>
```

Cambiaremos el texto Tab 1 por Pendientes y todo lo que hay dentro de ion-content lo eliminaremos.

También para VS Code instalaremos el Ionic Snippets para tener accesos directos a la generación de etiquetas.

Y Auto Close Tag también lo instalaremos, que no falte la ayuda!

[Volver al Índice](#%C3%ADndice-del-curso)

## 118. Material de la sección - DeseosApp

Material de la sección

Necesito que descarguen el material adjunto que usaremos en la próxima clase, básicamente es la configuración del Animate.css para hacer que se vea un poco mejor nuestra aplicación.

Simplemente descárguenlo y ténganlo a mano.

[Volver al Índice](#%C3%ADndice-del-curso)

## 119. Componente de Pendientes, estructura y tabs

Con la aplicación desplegada y en modo responsive de desarrollador podemos ver en consola una serie de warnings:

```
Native: tried calling StatusBar.styleDefault, but Cordova is not available. Make sure to include cordova.js or run in a device/simulator
```

Esto significa que normalmente cuando ejecutemos esta aplicación en un dispositivo móvil hay una libreria llamada Cordova que va a estar ejecutándose en el dispositivo móvil, cuando estamos en el escritorio esa librería no existe, por lo cual cualquier cosa que use Cordova no va a funcionar en el escritorio, podríamos decir que Cordova nos sirve para usar funcionalidades nativas del dispositivo propiamente dicho. Son normales cuando estamos desarrollando la aplicación.

Vamos a hacer algunos cambios, para nuestra aplicación sólo vamos a necesitar dos tabs, que serán el de Pendientes y el de Terminados.

Nos vamos a src/app/tabs/tabs.page.html vemos que este es el html inferior de la aplicación donde se agrupan los tabs, dejaremos la maquetación así:

```
<ion-tabs>

    <ion-tab-bar slot="bottom">
        <ion-tab-button tab="tab1">
            <ion-icon name="clipboard"></ion-icon>
            <ion-label>Pendientes</ion-label>
        </ion-tab-button>

        <ion-tab-button tab="tab2">
            <ion-icon name="ellipse"></ion-icon>
            <ion-label>Terminados</ion-label>
        </ion-tab-button>
    </ion-tab-bar>

</ion-tabs>
```

Podemos ver que dentro de los componentes predeterminados de ionic tenemos ion-icon, podemos buscar los iconos disponibles de ionic en https://ionicons.com/ y elegimos unos adecuados a la lógica, quedando:

```
<ion-icon name="clipboard"></ion-icon>
<ion-icon name="checkmark-done-outline"></ion-icon>
```

En cuestion de estilos, en el ion-header tiene como un box-shadow-bottom, que vamos a eliminar para que se vea como una sola página. Para esto ionic tiene unos estilos propios, vamos a los diferentes ion-header y lo dejamos así:

```
<ion-header [translucent]="true" class="ion-no-border">
```

Vamos a considerar algunas cosas más antes de terminar esta sección. Vamos a redistribuir los directorios, actualmente todas las páginas estan en el raíz del proyecto (app), así que vamos a crear un subdirectorio "pages" dentro de app para colocar los tabs (páginas) allí.

Como lo hice desde VSCode me preguntó si quería actualizar las rutas a los archivos, me lo hizo automáticamente, sino tendríamos que hacerlo manualmente actualizando los datos en app-routing.module.ts:

```
const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  }
];
```

El código anterior significa que solicita importar el archivo de módulos y una vez hecho esto le pasará al loadChildre el módulo TabsPageModule

Otra parte del código, que incluyo a continuación, define un preloadingStrategy, que es una configuración por defecto para decirle al router de Angular que cargue o pre-cargue ciertos módulos que se encuentran aquí.

```
  @NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
```

Finalmente eliminaremos el directorio tabs3 porque no le vamos a dar uso en esta aplicación. Pero existe una referencia a este tab que tendremos que eliminar también, esto se encuentra en src/app/pages/tabs/tabs-routing.module.ts

```
{
  path: 'tab3',
  loadChildren: () => import('../tab3/tab3.module').then(m => m.Tab3PageModule)
},
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 120. Servicio y clases de nuestra lista de deseos

Vamos a usar los ya conocidos servicios para centralizar la lógica del manejo de nuestra aplicación. Lo vamos a crear mediante la línea de comandos. Muy parecido a lo que ya hacíamos con Angular (ng)

>ionic g s services/deseos

Ahora en deseos.service.ts vamos a definir una propiedad "listas" que será un array, lo definimos de momento como tipo any, pero para poder manejar bien la información mediante métodos vamos a trabajar los modelos para ello. Podríamos generarlo con el cli de ionic, pero en este caso lo vamos a hacer de manera manual.

Creamos un directorio nuevo dentro de app llamado "models" y dentro de ese directorio un nuevo archivo de typescript "lista-item.model.ts" esto será una clase común y corriente. Comenzaremos declarándola con "export" para indicar que será usada y llamada desde fuera de este archivo. Ahora definimos este objeto que será nuestro item de la lista. Tendrá una descripción de tipo string, un estado completado de tipo boolean. Posteriormente crearemos el constructor del objeto, que solo pedirá la descripción, porque el estado completado será de inicio "false" (por lógica), quedando, de momento:

```
export class ListaItem {

    desc: string;
    completado: boolean;

    constructor( desc: string ) {

        this.desc = desc;
        this.completado = false;
    }
}
```

Dentro del directorio models vamos a crear otro archivo de typescript que definirá la lista como tal. lista.model.ts. Esta clase será exportada también y tendrá como propiedades un id de tipo number, un titulo de tipo string, fecha de creación creadaEn de tipo date, fecha de terminación terminadaEn de tipo date, una bandera de tipo boolean que llamaremos "terminada" que nos servirá para saber cuando todos los items de la lista han sido completados, y por último tendré los items, que será un array del tipo ListaItem creado anteriormente (hay que asegurarse de que queda importada la clase ListaItem).

Lo siguiente será crear el constructor de esta nueva clase, que como mínimo vamos a definir que reciba el título de la lista, por tanto el título será el recibido, creadaEn será la fecha actual, la lista no se creará terminada (lógico) e inicializaremos el valor a falso y los items será un array vacío. Para el id lo ideal sería gestionarlo a través de una base de dato o similar, un número autoincrementable o similar, pero para nuestra app de aprendizaje vamos a usar una función de Date para tomar un número que será único. La clase queda, de momento, así:

```
import { ListaItem } from "./lista-item.model";

export class Lista {

    id: number;
    titulo: string;
    creadaEn: Date;
    terminadaEn: Date;
    terminada: boolean;
    items: ListaItem[];

    constructor( titulo: string ) {

        this.titulo = titulo;
        this.creadaEn = new Date();
        this.terminada = false;
        this.items = [];
        this.id = new Date().getTime();
    }

}
```

Como ya tenemos el tipo, volvemos al servicio y a la propiedad "listas" ya podemos definirle el tipo Lista (hay que asegurarse que se importa la clase correcta de models/lista.model.ts)

Importante: El objetivo del servicio es manejar una única instancia en toda la aplicación, por tanto debe ser siempre accesible. Así que por ejemplo si queremos disponer de él en una de las páginas, nos vamos la página, digamos la tab1 (tab1.page.ts) y en el constructor le pasamos el servicio previamente importado. Lo haremos igual para el tab2. Quedando tipo:

```
import { DeseosService } from 'src/app/services/deseos.service';

constructor( public deseosService: DeseosService) {}
```

Por tanto como el servicio lo tenemos definido así:

```
import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {

  listas: Lista[] = [];

  constructor() { 
    console.log('Servicio inicializado');
  }
}
```

Y posteriormente fue inyectado en los constructores de las páginas, el servicio queda instanciado una sola vez de forma global y siempre estará accesible. (Se puede notar que el console log del Servicio inicializado aparece solo una vez aunque navegemos entre páginas)

[Volver al Índice](#%C3%ADndice-del-curso)

## 121. Pantalla de pendientes - diseño y documentación de ionic

Regresamos a deseos.service.ts y eliminamos el console log, vamos a crear dos listas para hacer todas las pruebas visuales.

En el constructor declararemos dos constantes que serán instancias de la clase Lista, como requieren el titulo obligatorio se lo definiremos ya. Esas dos listas las meteremos en la propiedad lista del servicio que ya teniamos declarado anteriormente. Podemos incluir un console log para ver dichas listas.

```
constructor() { 
    
    const lista1 = new Lista('Recolectar piedras del infinito');
    const lista2 = new Lista('Héroes a desaparecer');

    this.listas.push(lista1,lista2);

    console.log(this.listas);
  }
```

Estas dos listas las vamos a mostrar en el tab1.page.html. Vamos a usar componentes de ionic, vamos a ionicframework.com, a su documentación. En componentes podemos ver todos los componentes que hay. Básicamente estos componentes son servicios que se deben de importar para luego trabajar con ellos. Nosotros empezaremos con ion-list. Una de las ventajas de usar todos los componentes, como puede ser el ion-label es que nos controlará el responsive (gloria bendita).

Como ya tenemos en el componente inyectado el servicio en el constructor (el cual, a su vez, en el constructor definió las listas), podemos llamarlo directamente en el html y con un bucle for acceder a los títulos.

Como esperamos que hagamos click en esas listas para manipularlas le podemos agregar al ion-item  "detail" que añade la flecha que nos indica que podemos hacer click en esos elementos. De momento queda el html así:

```
<ion-header [translucent]="true" class="ion-no-border">
    <ion-toolbar>
        <ion-title>
            Pendientes
        </ion-title>
    </ion-toolbar>
</ion-header>

<ion-content [fullscreen]="true">

    <ion-list>
        <ion-item *ngFor="let lista of deseosService.listas" detail>
            <ion-label>{{ lista.titulo }}</ion-label>
        </ion-item>
    </ion-list>
</ion-content>
```

A continuación vamos a editar un poco la apariencia de la app. Ionic tiene una serie de colores predefinidos que se pueden cambiar.

src/themes/variables.scss Aquí podemos ver por ejemplo `--ion-color-dark: #222428;`

Añadiremos la clase color="dark" a todos los componentes que nos interesan, ion-toolbar, ion-content, ion-item, etc.

Para algunos elementos como el ion-list que sale con unos bordes blancos tendremos que ir al archivo src/global.scss y crear una clase para ion-list con background-color y el hexadecimal que mencionamos anteriormente en variables.scss. Tambien cambiaremos el color de los iconos tomando como referencia el terciary de las variables.scss, previa inspección del elemento y viendo que su clase es .tab-selected

```
/*
 * App Global CSS
 * ----------------------------------------------------------------------------
 * Put style rules here that you want to apply globally. These styles are for
 * the entire app and not just one component. Additionally, this file can be
 * used as an entry point to import other CSS/Sass files to be included in the
 * output CSS.
 * For more information on global stylesheets, visit the documentation:
 * https://ionicframework.com/docs/layout/global-stylesheets
 */


/* Core CSS required for Ionic components to work properly */

@import "~@ionic/angular/css/core.css";

/* Basic CSS for apps built with Ionic */

@import "~@ionic/angular/css/normalize.css";
@import "~@ionic/angular/css/structure.css";
@import "~@ionic/angular/css/typography.css";
@import '~@ionic/angular/css/display.css';

/* Optional CSS utils that can be commented out */

@import "~@ionic/angular/css/padding.css";
@import "~@ionic/angular/css/float-elements.css";
@import "~@ionic/angular/css/text-alignment.css";
@import "~@ionic/angular/css/text-transformation.css";
@import "~@ionic/angular/css/flex-utils.css";
ion-list {
    background-color: #222428!important;
}
.tab-selected {
    color: #7044ff;
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 122. Navegación entre pantallas

Vamos a crear una nueva pantalla/página que nos permitirá agregar nuevas listas, la crearemos con el CLI:

>ionic g page pages/agregar

Editamos un poco de texto en la plantilla html creada para poder ver los cambios posteriormente.

Ahora tenemos que definir la navegación a esta página, ionic desde la versión 4 en adelante trabaja directamente con el Router de Angular, por lo que si queremos llegar a la página tendremos que navegar simplemente utilizando el Router de Angular.

Vamos a ver el app-routing.module.ts, y veremos que automaticamente agregó el path 'agregar' a las rutas. Por tanto si en el navegador escribimos directamente la url `http://localhost:8100/agregar` nos llevará a la página correctamente, pero perdimos nuestros tabs, porque en app-routing.module.ts está definido que está fuera del módulo de los tabs, así que vamos a comentar la línea que define el path, para tenerlo como referencia.

Pero antes de la documentación de ionic vamos a coger un botón para usarlo de navegación a la página. Y lo añadiremos al html del tab1:

```
<ion-header [translucent]="true" class="ion-no-border">
    <ion-toolbar color="dark">
        <ion-title>
            Pendientes
        </ion-title>
    </ion-toolbar>
</ion-header>

<ion-content [fullscreen]="true" color="dark">

    <ion-list color="dark">
        <ion-item *ngFor="let lista of deseosService.listas" detail color="dark">
            <ion-label>{{ lista.titulo }}</ion-label>
        </ion-item>
    </ion-list>
    <ion-fab vertical="bottom" horizontal="end" slot="fixed">
        <ion-fab-button color="tertiary" (click)="agregarLista()">
            <ion-icon name="add"></ion-icon>
        </ion-fab-button>
    </ion-fab>
</ion-content>
```

Necesitamos inyectar el Router de Angular en el componente de tab1 para poder navegar (asegurarnos de que es importado de @angular/router). Posteriormente vamos a crear en el ts de tab1 un método llamado agregarLista que, temporalmente, lo que hará será navegar hacia esa ruta (posteriormente controlaremos la información a manejar). Y en el html definir en el botón un evento click que ejecute el método agregarLista. Asi funcionaría pero estamos perdiendo los tabs.

```
agregarLista() {

  this.router.navigateByUrl('/agregar');
}
```

Para tener esos tabs, debemos eliminar el path de app-routing.module.ts

```
/* {
    path: 'agregar',
    loadChildren: () => import('./pages/agregar/agregar.module').then( m => m.AgregarPageModule)
  } */
```

Ahora en las nuevas versiones de ionic cada tab tiene su propio modulo de rutas, así que vamos a tab1-routing.module.ts y añadimos la ruta:

```
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab1Page } from './tab1.page';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
  },
  {
    path: 'agregar',
    loadChildren: () => import('../agregar/agregar.module').then( m => m.AgregarPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule {}
```

Lo que pasará ahora al pulsar el botón es darnos un error, porque la función del boton busca navegar a la url '/agregar' que ya no está definida. Por lo que regresamos al ts de tab1 para definir la ruta correctamente, como ahora es una ruta hija (como hemos definido en tabs1-routing.module.ts) sería:

```
agregarLista() {

    this.router.navigateByUrl('/tabs/tab1/agregar');
  }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 123. Diseño de la página de agregar

Pese a que nativamente se puede cambiar de pantalla en distintos dispositivos, botón atrás de Android o slide en la pantalla de iOS, vamos a añadir un botón para ir hacia atrás en la pantalla de Agregar.

Vamos a empezar con algunos cambios en el agregar.page.html, como eliminar el sombreado del header y a añadir el estilo oscuro con `class="ion-no-border"` y `color="dark"`

Para el botón vamos a usar componentes de ionic, `<ion-buttons> </ion-buttons>` para crear un pack de botones, y dentro de él `<ion-back-button></ion-back-button>` para el botón de regresar atrás. Como ionic detecta que no hay una página anterior no lo renderizará, para evitar esto hay que definir el parámetro `defaultHref="/"` para decirle que por defecto vaya a la raíz de la app. Para posicionar el botón al principio (esquina superior izquierda) tendremos que añadirle el parámetro `slot="start"` a ion-buttons.

Ahora para el html de la lista vamos a la documentación de ionic, normalmente podríamos usar aquí ion-input para introducir datos, en nuestro caso vamos a usar ion-item dentro de ion-list (esta manera se define en la documentación dentro de la sección ion-input como una de las opciones a poder usar). Se acostumbra a poner el ion-item dentro de un ion-list, y es que el ion-list por defecto permite hacer scroll (por ejemplo para formularios muy largos). Con esto tendríamos el input para agregar nuevos items a la lista.

Ahora para crear dicha lista vamos a usar otro ion-list con un ion-list-header y un ion-label para indicar que será "Tareas por hacer" y un ion-item que tendrá un checkbox, ionic tiene los suyos propios: ion-checkbox, que también tiene un parámetro `slot="start"` para colocarlo al inicio, incluiremos un ion-label que será el texto de la tarea. La maquetación queda así:

```
<ion-header class="ion-no-border">
    <ion-toolbar color="dark">
        <ion-buttons slot="start">
            <ion-back-button text="" color="tertiary" defaultHref="/"></ion-back-button>
        </ion-buttons>
        <ion-title>Nombre de la lista</ion-title>
    </ion-toolbar>
</ion-header>

<ion-content color="dark">
    <ion-list>
        <ion-item color="dark">
            <ion-label position="floating">Nuevo item</ion-label>
            <ion-input type="text"></ion-input>
        </ion-item>
    </ion-list>
    <ion-list>
        <ion-list-header color="dark">
            <ion-label>Tareas por hacer</ion-label>
        </ion-list-header>
        <ion-item color="dark">
            <ion-checkbox slot="start" color="tertiary"></ion-checkbox>
            <ion-label>Recordar comer hoy</ion-label>
        </ion-item>
    </ion-list>
</ion-content>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 124. Alert Controller - Agregar una lista a nuestro servicio

Lo que vamos a hacer en esta sección es que en lugar de navegar a la página "Agregar" al pulsar el botón de Agregar en el inicio, no salga una pantalla donde yo pueda escribir el nombre o título de la lista y agregarla a mi servicio. Para que aparezca esa ventana vamos a usar el Alert Controller de ionic (ion-alert). En principio es sólo un Alert pop-up, pero si le defino los inputs podemos usarlo para introducir datos como si fuera un formulario.

Para poder usarlo hay que inyectarlo en el constructor del componente, asegurándonos de que se importa la librería correcta. Lo tendremos que hacer en el ts del tab1 que es donde está el botón de agregar.

Una vez inyectado llamaremos en el método del botón agregarLista al AlertController, de momento dejamos comentada la navegación. En la documentación de ionic tenemos como defifinir las funciones de AlertController, el código que necesitamos es el siguiente:

```
const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
```

Explicamos el código. Lo que hace el alertController es trabajar con una promesa, pero al usar "await" le indicamos que hasta que no se cree la constante no ejecute la promesa, pero la expresión await solo se puede usar con una función asíncrona, así que tendremos que definir la función como `async agregarLista()`, de esta manera la función pasa a ser una promesa.

Entonces lo que hará la función será ejecutar el método create y cuando esté terminado lo almacenará en la constante alert.

El método .present de alert lo que hará será renderizar el resultado, pero quitaremos el await que lleva delante porque para nuestro caso no es necesario.

Para convertir el alert en lo que el profesor llama un "prompt" (no me parece exacto este término,pero bueno), sería cuestión de definir los inputs y la acción de los botones. Para los inputs tendremos que definir el 'name', que lo usaremos para tomar referencia del input (como en cualquier formulario), el type para el tipo de input, en nuestro caso 'text' y el placeholder para dar información sobre el input al usuario.

```
inputs:[{
        name: 'titulo',
        type: 'text',
        placeholder: 'Nombre de la lista',
      }],
```

Para los botones definiremos el 'text' que será el texto del botón, 'role' que define el comportamiento del botón, si lo definimos como 'cancel' esto hará que si pulsamos fuera del alert se cierre la pantalla, el 'handler' definirá una función a ejecutar al pulsar el botón, para muestra haremos con el botón crear un console log de los datos del formulario 'alert' y otro indicando que se pulsó el botón cancelar al pulsar este otro botón. También haremos una validación para que el título no esté vacío a la hora de crear la lista.

```
buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar');
        }
      },
      {
        text: 'Crear',
        handler: ( data ) => {
          console.log( data );
          if ( data.titulo.length === 0 ) {
            return;
          }
          // Si no es 0 tengo que crear la lista
        }
      }]
```

El método para crear la lista debería estar centralizado en nuestro servicio, y para eso vamos a deseos.service.ts y creamos un método llamado crearLista que necesitará el título como argumento (recordemos que en el modelo lista.model.ts definimos como es ese tipo de objeto, y en el constructor definimos que necesitaba, como mínimo, el título.), y creará una nueva instancia del objeto Lista con dicho título y la introducirá en el array de listas que tenemos en el servicio.

```
crearLista( titulo: string ){

    const nuevaLista = new Lista( titulo );
    this.listas.push( nuevaLista );

  }
```

Ahora ya podemos llamar al método crearLista del servicio deseosService en el handler del boton crear. Cuando ejecutemos la función se refrescará automaticamente las listas, porque en el html lo que hacemos es listar directamente del servicio.

```
buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar');
        }
      },
      {
        text: 'Crear',
        handler: ( data ) => {
          console.log( data );
          if ( data.titulo.length === 0 ) {
            return;
          }
          // Si no es 0 tengo que crear la lista
          this.deseosService.crearLista( data.titulo );
        }
      }]
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 125. Localstorage - Hacer persistente la información

Dejamos la aplicación que tras tocar el botón agregar se añadía la nueva lista en el inicio, pero si refrescamos la información se pierde, en el punto en el que estamos, así que tendremos que almacenarla, existen distintos lugares para hacer esto, lo podemos ver con facilidad en las herramientas del navegador, Application->Storage (o similar dependiendo del navegador), tenemos Local Storage, Session Storage, etc. El Local Storage guarda la información como string, pero no la encripta y es de fácil acceso, por lo que no deberíamos guardar información sensible aquí, Session Storage guarda la información también en string pero en lo que dure la sesión, es decir, cuando se cierre el navegador se perderá la información, por último tendríamos las cookies, cada vez más en desuso en detrimento de las otras dos mencionadas anteriormente.

Nosotros para esta aplicación vamos a usar el Local Storage, se recomienda usar un plugin de ionic llamado "Storage" que podemos ver en las guias de la documentación para desarrollar aplicaciones con ionic. El problema con el local storage es que los datos ocuparán espacio en nuestro dispositivo, y algunas aplicaciones de limpieza de móvil pueden borrar estos datos.

Vamos a crear un par de métodos nuevos en el deseos.service.ts, guardarStorage() y cargarStorage(), para guardar la información y para cargarla cuando se abra la aplicación, respectivamente. No necesitamos importar ninguna libreria para usar el método localStorage y la función setItem, que nos permite guardar información en forma de key->value, pero el value tiene que ser un string, y como nosotros lo que tenemos es un array lo que haremos será convertirlo a un json de tipo string con el método JSON.stringify.

el método guardarStorage() lo tendremos que llamar cuando hagamos el push de los datos en el array lista[], y para cargarlo tendremos que hacer en cargarStorage() el paso inverso, getItem y convertir de string a array de nuevo, previamente tendremos que validar si hay datos en el storage, porque si tratamos de convertir a array un string null, dado ese caso, nos daría error, para llamar al cargarStorage() lo podemos hacer en el constructor, aprovecharemos para comentar/quitar las constantes de prueba, el código queda así:

```
import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {

  listas: Lista[] = [];

  constructor() {

    this.cargarStorage();
    
    //const lista1 = new Lista('Recolectar piedras del infinito');
    //const lista2 = new Lista('Héroes a desaparecer');
   
    //this.listas.push(lista1,lista2);

    //console.log(this.listas);
  }

  crearLista( titulo: string ){

    const nuevaLista = new Lista( titulo );
    this.listas.push( nuevaLista );
    this.guardarStorage();

  }

  guardarStorage() {

    localStorage.setItem('data',JSON.stringify(this.listas));
  }

  cargarStorage() {

    if ( localStorage.getItem('data') ){
      this.listas = JSON.parse( localStorage.getItem('data') );
    } else {
      this.listas = []; // Realmente esto se podría obviar porque al iniciar el servicio ya tenemos inicializado el array vacío
    }
  }

}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 126. Funcionalidad de la pantalla para agregar tareas a la lista

Vamos a trabajar en la funcionalidad de poder agregar items a esas listas. Recordemos que todas las listas tienen un id único (que habíamos creado usando la función Date()), vamos a usarlo como argumento para navegar a la pantalla de Agregar y modificar la información relevante a los items de esa lista.

Para ello vamos a tab1-routing.module.ts para definir que la ruta puede recibir un parámetro por url.

```
const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
  },
  {
    path: 'agregar/:listaId',
    loadChildren: () => import('../agregar/agregar.module').then( m => m.AgregarPageModule)
  }
];
```

Regresamos al typescript del componente tab1. Cuando en el método agregarLista() estamos agregando la nueva lista mediante la función crearLista() necesitamos saber el id de esa lista para poder llamar a la redirección que teníamos comentada `//this.router.navigateByUrl('/tabs/tab1/agregar');`

En deseos.service.ts vamos al método crearLista(), allí podemos hacer un `return nuevaLista.id;` y será el id que necesitamos. De esta manera ahora ademas de crear la lista devolvemos su id, entonces el resultado de la función lo podemos almacenar en esa variable que necesitamos (tab1.page.ts `const listaId = this.deseosService.crearLista( data.titulo );`), y mediante backticks alterar la url de navegación añadiendo ese id como argumento: `this.router.navigateByUrl(`/tabs/tab1/agregar/${listaId}`);`

Lo siguiente que haremos en el servicio será crear un método obtenerLista para poder acceder a todas las propiedades de esa lista y sus datos. El método como argumento para trabajar necesitará el id de la lista, lo definiremos como string o number, pero posteriormente haremos la comprobación y transformación a number para poder trabajar con él `id = Number(id);`(cuando se crea es number, pero en la url es tipo string, dependiendo de donde quisieramos llegar a usar el método). Después usaremos el método find() de los array para devolver la lista que coincida con la buscada mediante el método con el id coincidente de la función principal.

```
obtenerLista( id: string | number ) {

    id = Number(id);

    return this.listas.find( listaData => listaData.id === id );// Versión corta de función de flecha, un solo "return" implícito

  }
```

Esto deberá ser cargado en la página Agregar, así que en agregar.page.ts importamos el servicio DeseosService en el constructor, también tendremos que importar el ActivatedRoute para recoger el parámetro de la url, y lo que se hará en el método de constructor es ejecutar la función obtenerLista pasándole el id conseguido mediante el ActivatedRoute para tener disponibles los datos. Así que guardamos en una constante el id, sin recurrir a observables, de esta manera: `const listaId = this.route.snapshot.paramMap.get('listaId');`, definiendo un nuevo objeto de tipo Lista de nuestro modelo ahora podemos almacenar ahí la información:

```
lista: Lista;

  constructor(private deseosService: DeseosService,
              private route: ActivatedRoute) {

    const listaId = this.route.snapshot.paramMap.get('listaId');

    this.lista = this.deseosService.obtenerLista(listaId);
    
   }
```

Lo siguiente será crear la función que permita agregar items a la lista agregarItem() así como una variable nombreItem de tipo string, si vamos al html tendremos que relacionar esa variable con el input donde escribimos el nuevo item y así poder agregarlo a la lista `<ion-input type="text" [(ngModel)]="nombreItem"></ion-input>` recordar que ngModel tiene paréntesis porque emite un evento y las llaves cuadradas es porque escucha un evento.

En agregarItem() haremos una primera validación de que se escriba un nombre para el item y no esté en blanco. La función creará un nuevo item de lista que instanciaremos del modelo ListaItem, y añadiremos el item al array de items de esa lista. Finalmente limpiaremos la variable nombreItem para que sea posible usarla de nuevo.

```
agregarItem( ) {

    if ( this.nombreItem.length ===0 ){
      return;
    }

    const nuevoItem = new ListaItem(this.nombreItem);
    this.lista.items.push( nuevoItem );
    this.nombreItem = '';
  }
```

Añadiremos la función cuando se pulse intro en el input del agregar.page.html mencionado anteriormente. Y crearemos un bucle que nos muestre todos los items de la lista que se vayan agregando:

```
 <ion-item color="dark" *ngFor="let item of lista.items">
    <ion-checkbox slot="start" color="tertiary"></ion-checkbox>
    <ion-label>{{item.desc}}</ion-label>
</ion-item>
```

Para que la información sea persistente, como el objeto Lista con el que estamos trabajando realmente es una instanciación del objeto de listas que tenemos en el servicio, es como si trabajásemos directamente con el objeto del servicio, por lo tanto podemos llamar a la función que guarda toda la info en el local storage

```
agregarItem( ) {

    if ( this.nombreItem.length ===0 ){
      return;
    }

    const nuevoItem = new ListaItem(this.nombreItem);
    this.lista.items.push( nuevoItem );
    this.nombreItem = '';
    this.deseosService.guardarStorage();
  }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 127. Detalles estéticos de la pantalla agregar

En esta sección vamos a limar temas de apariencia y funcionalidad, como que persista el estado de los checkbox de los items de la lista. Recurrimos en primer lugar al archivo de texto adjunto en la sección animate.txt que es un extracto de animaciones css (muy útil, por cierto, esta fuente https://daneden.github.io/animate.css/). Añadiremos el código del txt al global.scss

Ya podemos llamarlo en nuestro agregar.page.html:

```
<ion-item color="dark" *ngFor="let item of lista.items" class="animated fadeInDown">
```

Para el tema de la persistencia de los checkbox, la mayoria de los elementos en ionic tienen posibilidad de asociar un evento (ionChange) que se dispara cuando cambia el elemento, y lo añadiremos al elemento definiendo que dispare una función que reciba ese item del checkbox como argumento (recordemos que el item tiene como propiedad un booleano, además de la descripción.). Cada vez que llamemos a esta función se llamará a la función guardarStorage para guardarlo en nuestro servicio, asímismo vamos a aprovechar para controlar las dos propiedades de la lista que son terminadaEn y terminada, para cuando se hayan marcado todos los items darla por terminada, cosa que luego será usado en la segunda parte de la aplicación.

Hacemos un return de los items que no estan completados. Para ello declaramos una constante que almacenará el resultado de la función filter sobre el array de items de la lista, dicho resultado será la longitud de ese array. Cuando sea 0 será que está completa la lista.

```
cambioCheck( item: ListaItem ) {
    
    const pendientes = this.lista.items
                        .filter( itemData => !itemData.completado)
                        .length;
    
    if ( pendientes === 0 ) {
      this.lista.terminada = true;
      this.lista.terminadaEn = new Date();
    } else {
      this.lista.terminada = false;
      this.lista.terminadaEn = null;
    }

    this.deseosService.guardarStorage();
  }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 128. Eliminar items de la lista de deseos

Para añadir la funcionalidad de eliminar vamos a usar un componente de ionic llamado ion-item-sliding, que nos permitirá deslizar el item para eliminarlo. Sería tan sencillo como englobar el item en este componente y luego añadir el ion-item-options que nos mostrará los distintos botones que queramos incluir, eliminar o cualquier otro que quisiéremos, ver la documentación de ionic para todo esto. Tambien tendremos que cambiar el bucle for y la animación al wrapper de ion-item-sliding, que será el elemento padre ahora. Para la función de borrar necesitaremos el id del item que queremos borrar, así que añadiremos la generación de índices en el bucle for. El html quedaría así:

```
<ion-item-sliding *ngFor="let item of lista.items;let i = index;" class="animated fadeInDown">
            <ion-item color="dark">
                <ion-checkbox slot="start" color="tertiary" [(ngModel)]="item.completado" (ionChange)="cambioCheck(item)"></ion-checkbox>
                <ion-label>{{item.desc}}</ion-label>
            </ion-item>
            <ion-item-options side="end">
                <ion-item-option (click)="borrar(item)" color="danger">
                    <ion-icon slot="icon-only" name="trash-outline"></ion-icon>
                </ion-item-option>
            </ion-item-options>
        </ion-item-sliding>
```

La función para borrar usará el método splice al que le pasamos por argumentos la posición desde la que queremos borrar (i) y cuantos elementos queremos borrar del array (1), despues guardaremos los cambios en nuestro servicio.

```
borrar ( i: number ) {

    this.lista.items.splice( i, 1);
    this.deseosService.guardarStorage();

  }
```


[Volver al Índice](#%C3%ADndice-del-curso)

## 129. Editar elementos de otras listas

Ahora en la sección principal de listas pendientes vamos a agregar la funcionalidad de tocar una lista para editarla, será igual a lo que hacía el botón de añadir (+), haremos el elemento clickable y le añadiremos que dispare una función tipo listaSeleccionada(lista) que navegará a la pantalla de edición usando el id de esa lista como parámetro `<ion-item *ngFor="let lista of deseosService.listas" detail color="dark" (click)="listaSeleccionada(lista)">`

```
listaSeleccionada (lista: Lista) {

    this.router.navigateByUrl(`/tabs/tab1/agregar/${ lista.id }`);
    
}
```

También vamos a añadir que en la pantalla principal de pendientes se muestre el número de items de esa lista, por dar más información, usando un componente de ionic llamado ion-note.

```
<ion-header [translucent]="true" class="ion-no-border">
    <ion-toolbar color="dark">
        <ion-title>
            Pendientes
        </ion-title>
    </ion-toolbar>
</ion-header>

<ion-content [fullscreen]="true" color="dark">

    <ion-list color="dark">
        <ion-item *ngFor="let lista of deseosService.listas" detail color="dark" (click)="listaSeleccionada(lista)">
            <ion-label>{{ lista.titulo }}</ion-label>
            <ion-note slot="end" color="tertiary">{{ lista.items.length }} items</ion-note>
        </ion-item>
    </ion-list>
    <ion-fab vertical="bottom" horizontal="end" slot="fixed">
        <ion-fab-button color="tertiary" (click)="agregarLista()">
            <ion-icon name="add"></ion-icon>
        </ion-fab-button>
    </ion-fab>
</ion-content>
```


[Volver al Índice](#%C3%ADndice-del-curso)

## 130. Módulo de componentes y listas component

Vamos a explicar que son los módulos, algo con lo que trabaja mucho ionic desde la versión 4 en adelante.

En el caso que nos ocupa, podríamos decir que Angular es una colección de módulos, cuando necesitamos incorporar nuevas funcionalidades en la aplicación las implementamos en el app.module.ts o en el app-routing.module.ts.

Actualmente tenemos una pantalla de Pendientes, y la pantalla que nos queda por hacer, la de terminadas, será en esencia exactamente igual que la anterior, con las diferencias obvias.

Entonces lo interesante sería poder reutilizar todo el html, pero copiar y pegar todo el código sería complicado y podría dar muchos errores en el proceso.

Vamos a crear un módulo de componentes con el ionic cli:

>ionic g m components

Una vez creado si vamos al archivo components.module.ts vemos en primer lugar las importaciones que hace, NgModule siempre estará, es la base, el componente nuclear de Angular, el CommonModule trae los ngIf, ngFor etc. Dentro de las declaraciones irían los componentes que va a usar ese módulo.

La idea de esto es poder centralizar y en este módulo poner toda la lógica referente a los componentes de la aplicación, así sólo tendríamos que llamar al módulo cuando queramos usarlo y este ya traerá todos los pipes, componentes, métodos, etc que albergue.

Lo siguiente será crear un componente llamado listas, que me permitirá reutilizar todo el html que teníamos hasta ahora referente a las listas.

>ionic g c components/listas

Lo siguiente será revisar si el componente se importó o no automáticamente. En nuestro caso no lo hizo, así que nos vamos a components.module.ts para importarlo en las declaraciones, también tendremos que declararlo en los exports, si queremos llamar al componente desde fuera del módulo

```
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListasComponent } from './listas/listas.component';



@NgModule({
  declarations: [
    ListasComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ListasComponent
  ]
})
export class ComponentsModule { }
```

En listas.component.ts podemos ver el nombre del selector para usar el componente `app-listas`. Pero para poder usarlo sin errores hay que importar el módulo de componentes que hemos creado en los distintos lugares donde se va a usar el componente. Para que quede más claro: Cada página (tab1 y tab2) tiene su propio archivo de módulos, si importamos ahí el modulo que hemos creado de componentes ya tendríamos acceso directo a los componentes de ese módulo, si por otro lado tratásemos de importar en cada una de las páginas, directamente, el componente nuevo (en lugar del módulo al que pertenece) nos daría error porque estaría tratando de importar dos veces el mismo componente .....

components.module.ts:

```
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListasComponent } from './listas/listas.component';



@NgModule({
  declarations: [
    ListasComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ListasComponent
  ]
})
export class ComponentsModule { }
```

tab2.module.ts:

```
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { Tab2PageRoutingModule } from './tab2-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab2PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Tab2Page]
})
export class Tab2PageModule {}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 131. Componente listas

Vamos a mover la lógica del html de las listas al componente. Para poder usar etiquetas html de ionic en nuestro nuevo componente tenemos que importarlo en el components.module.ts, hablamos de IonicModule:

```
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListasComponent } from './listas/listas.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [
    ListasComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    ListasComponent
  ]
})
export class ComponentsModule { }

```

Para poder usar el servicio y las funciones debemos importar primero en el constructor de listas.component.ts el servicio. Tambien quitaremos la función de listaSeleccionada del tab1 y la pondremos en el listas.component.ts, porque ahora es de uso común. Para esto necesitaremos, además, importar en el constructor el router y el modelo de Lista, y en el tab1 ya no necesitaremos ni el router ni el modelo de Lista. El servicio DeseosService tendremos que declararlo en el constructor como público, pues se está accediendo desde fuera a él.

listas.component.ts:

```
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Lista } from 'src/app/models/lista.model';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {

  constructor(public deseosService: DeseosService,
              private router: Router) { }

  ngOnInit() {}

  listaSeleccionada (lista: Lista) {

    this.router.navigateByUrl(`/tabs/tab1/agregar/${ lista.id }`);
    
  }
}
```

Pero aun nos falta que nos redirija correctamente cuando intentemos hacer click en uno de las listas de Terminados, por tanto primero tenemos que agregar el path hijo correspondiente en tab2-routing.module.ts

```
{
  path: 'agregar/:listaId',
  loadChildren: () => import('../agregar/agregar.module').then( m => m.AgregarPageModule)
}
```

Y después tendremos que verificar en listas.component.ts si estamos en la página de terminados o de pendientes (tab1 ó tab2). Para ello vamos a declarar un @Input (importado de @Angular/core) que tenga una variable que haga de bandera, para saber en qué página estamos, en función de si es verdadero o falso llevará a una url u otra

```
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Lista } from 'src/app/models/lista.model';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {

  @Input() terminada = true;

  constructor(public deseosService: DeseosService,
              private router: Router) { }

  ngOnInit() {}

  listaSeleccionada (lista: Lista) {

    if (this.terminada) {
      this.router.navigateByUrl(`/tabs/tab2/agregar/${ lista.id }`);  
    } else {
      this.router.navigateByUrl(`/tabs/tab1/agregar/${ lista.id }`);
    }
    
  }
}
```

Como tenemos el input ya listo vamos a cada uno de las llamadas al componente y definimos la variable en verdadero o falso en función de en qué página se esté llamando al selector `<app-listas [terminada]="false">`

[Volver al Índice](#%C3%ADndice-del-curso)

## 132. Eliminar una lista

Para eliminar una lista lo haremos con ion-item-sliding de nuevo. La diferencia es que aquí no podremos usar el índice para definir que elemento se va a eliminar, porque el orden de listas puede cambiar debido a que pasen de Pendientes a Terminadas en distinto orden.

Vamos a listas.component.html y maquetamos los componentes de ionic ion-item-sliding, definiendo correctamente el bucle for y dejando lista una función de borrar en el botón de eliminar del sliding, la función la declararemos en listas.component.ts, pero para que esté todo centralizado la funcionalidad deberá estar definida en el servicio. A continuación listas.component.html:

```
<ion-list color="dark">
    <ion-item-sliding *ngFor="let lista of deseosService.listas">
        <ion-item detail color="dark" (click)="listaSeleccionada(lista)">
            <ion-label>{{ lista.titulo }}</ion-label>
            <ion-note slot="end" color="tertiary">{{ lista.items.length }} items</ion-note>
        </ion-item>
        <ion-item-options side="end">
            <ion-item-option (click)="borrarLista(lista)" color="danger">
                <ion-icon slot="icon-only" name="trash-outline"></ion-icon>
            </ion-item-option>
        </ion-item-options>
    </ion-item-sliding>

</ion-list>
```

Tenemos por tanto que ir a deseos.service.ts y tras crearLista vamos a definir el método borrarLista(), en el cual vamos a hacer un filtrado de todos los elementos cuyo id sea diferente al de la lista, y esto lo sobreescribiremos en nuestro array de listas, es decir, recorreremos el array de lista comparando con el id de la lista que queremos borrar y reescribiremos todos las listas exceptuando esa, quedando borrada (o excluída, a efectos prácticos), para hacer persistente el cambio deberemos guardarlo en el localStorage al final.

```
borrarLista( lista: Lista ) {

    this.listas = this.listas.filter( listaData => listaData.id !== lista.id );
    this.guardarStorage();

  }
```

Ya podemos llamar al método del servicio en listas.component.ts

```
borrarLista( lista: Lista ) {

  this.deseosService.borrarLista( lista );

}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 133. Pipes impuros

Ahora toca discriminar entre las listas terminadas y las que no, pues ahora mismo muestra la mismas listas en los dos tabs, estén terminadas o no. Para esto vamos a crearnos un pipe, que no sólo sirven para modificar los datos visualmente, también los podemos usar como filtro, pondremos ese filtro en el ngFor de la lista de listas y mostrará una lista u otra en función de unas condiciones.

Al igual que con los componentes, vamos a crear un módulo para los pipes, por el mismo tema de centralización y reutilización, sobre todo pensando en ionic y en su enfoque de cara a los módulos para funcionar.

>ionic g m pipes

Una vez creado podemos eliminar de pipes.module.ts el CommonModule porque no vamos a usar ngIf, ngFor, etc, sólo vamos a usarlo este módulo para la gestión de los pipes.

Lo siguiente es crear el pipe con el ionic cli:

>ionic g pipe pipes/filtroCompletado

Nótese que actualizó el pipes.module.ts y lo importó y declaró. Como lo usaremos fuera necesitamos exportarlo aquí en pipes.module.ts, si creamos más pipes deberíamos controlarlos aquí. Ahora vamos a la lógica de filtro-completado.pipe.ts. Deberíamos recibir como valor el array con todas las listas, y como argumento si estan completadas o no, y lo que devolverá será otro array con la lista filtrada, el pipe lo que hará será devolver un array filtrando por la propiedad "terminada" que tienen las listas, quedando el pipe así:

```
import { Pipe, PipeTransform } from '@angular/core';
import { Lista } from '../models/lista.model';


@Pipe({
  name: 'filtroCompletado'
})
export class FiltroCompletadoPipe implements PipeTransform {

  transform(listas: Lista[], completada: boolean = true): Lista[] {

    return listas.filter( lista => lista.terminada === completada );
    
  }

}
```

Para poder usar el pipe en el listas.component.html hay que importarlo en compoments.module.ts, porque hay que recordar que ese html es de un componente que forma parte de un módulo de componentes, y al definir el pipe en el módulo todos los componentes que formen parte de ese módulo podrán hacer uso de él.

Como habíamos definido un @Input terminada que nos hacía de bandera podemos usarlo como argumento para pasarselo al pipe y que lo use como criterio de filtrado.

Pero falta un detalle, y es que hasta que no se recarga la aplicacion no se actualizan las listas, es decir, no se vuelve a aplicar el filtro del pipe, esto es porque como la alteración de los datos no se está dando dentro del mismo componente, sino fuera (en su propio módulo) el pipe no está escuchando atento a esos cambios, para que esté atento a los cambios en todo momento en el pipe debemos incluir, en el decorador, tras el name, pure: false, y ya renderizará en todo momento pese a que los cambios se estén dando fuera, por tanto queda así el código:

filtro-completado.pipe.ts:

```
import { Pipe, PipeTransform } from '@angular/core';
import { Lista } from '../models/lista.model';


@Pipe({
  name: 'filtroCompletado',
  pure: false
})
export class FiltroCompletadoPipe implements PipeTransform {

  transform(listas: Lista[], completada: boolean = true): Lista[] {

    return listas.filter( lista => lista.terminada === completada );

  }

}

```

components.module.ts:

```
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListasComponent } from './listas/listas.component';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from '../pipes/pipes.module';



@NgModule({
  declarations: [
    ListasComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    PipesModule
  ],
  exports: [
    ListasComponent
  ]
})
export class ComponentsModule { }

```

listas.component.html:

```
<ion-list color="dark">
    <ion-item-sliding *ngFor="let lista of deseosService.listas | filtroCompletado:terminada">
        <ion-item detail color="dark" (click)="listaSeleccionada(lista)">
            <ion-label>{{ lista.titulo }}</ion-label>
            <ion-note slot="end" color="tertiary">{{ lista.items.length }} items</ion-note>
        </ion-item>
        <ion-item-options side="end">
            <ion-item-option (click)="borrarLista(lista)" color="danger">
                <ion-icon slot="icon-only" name="trash-outline"></ion-icon>
            </ion-item-option>
        </ion-item-options>
    </ion-item-sliding>

</ion-list>
```



[Volver al Índice](#%C3%ADndice-del-curso)

## 134. Editar el título de la lista

Esta lección consiste en una tarea práctica para el alumno. Agregar la funcionalidad de deslizar una lista para que aparezca un icono de edición, al pulsar el icono de edición que aparezca un alert (como el de crear lista) para editar el título de la lista, que se pueda guardar y almacenar en el Local Storage.

Como es repaso de conceptos yo hice por mi cuenta todo el proceso y llegué hasta la última parte de almacenar, que no lo tenía claro y como siempre me lío y lo hago más complicado, así que me remito a dejar aquí todo el código resultante. Previamente de manera esquemática dejaré algunas consideraciones.

- Para poder usar el alert en el componente hay que importar las librerías que lo posibilitan
- Mucho es tema de documentación, como el que se cierre el slide automáticamente, es algo posible del componente de ionic
- En todo momento estamos haciendo uso del Servicio, cargando y guardando las listas que tenemos ahí.
- Podemos hacer referencia a un componente usando @ViewChild, es decir, tenemos el componente listas, en él hay un componente ion-list que es al que queremos hacer referencia y queremos aplicar el método de ion-list que permite cerrar el slide, pues bien, con @ViewChild podemos hacer que se "vea" o "tener en cuenta" un elemento de listas.component.html, en este caso el ion-list, y con @ViewChild creamos una instancia u objeto que representa ese elemento y le decimos que será de tipo IonList, en nuestro código fue llamado lista (en mi opinión nombre poco intuitivo), como solo hay un ion-list en todo el componente y creando su instanciación ya podremos aplicar los métodos que queramos sobre él, si hubiera varios elementos de ese tipo se podrían definir identificadores para cada uno de los elementos y poder instanciarlos por separado (es decir, si hubiera 5 ion-list en el componente, al usar @ViewChild sobre ese tipo tendríamos un array de elementos de ese tipo, por lo tanto podríamos recorrerlos, filtrarlos o seleccionar la posición del array que nos interese para aplicarle cada método o tarea en cuestión):

Ejemplo:

@ViewChild( IonList ) // Un array con todos los IonList del componente, si sólo hay uno, pues obviamente sólo selecciona ese, sería un array con sólo un elemento.
@ViewChild( 'lista' ) // Un elemento en concreto, en el componente de ionic tendríamos que definir un id local de esta manera: <ion-list #lista>

listas.component.html:

```
<ion-list color="dark">
    <ion-item-sliding *ngFor="let lista of deseosService.listas | filtroCompletado:terminada">
        <ion-item detail color="dark" (click)="listaSeleccionada(lista)">
            <ion-label>{{ lista.titulo }}</ion-label>
            <ion-note slot="end" color="tertiary">{{ lista.items.length }} items</ion-note>
        </ion-item>
        <ion-item-options side="end">
            <ion-item-option (click)="borrarLista(lista)" color="danger">
                <ion-icon slot="icon-only" name="trash-outline"></ion-icon>
            </ion-item-option>
        </ion-item-options>
        <ion-item-options side="start">
            <ion-item-option (click)="editarLista(lista)" color="primary">
                <ion-icon slot="icon-only" name="create"></ion-icon>
            </ion-item-option>
        </ion-item-options>
    </ion-item-sliding>
</ion-list>
```

listas.component.ts

```
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonList } from '@ionic/angular';
import { Lista } from 'src/app/models/lista.model';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {

  @ViewChild( IonList ) lista: IonList;
  @Input() terminada = true;

  constructor(public deseosService: DeseosService,
              private router: Router,
              private alertCtrl: AlertController) { }

  ngOnInit() {}

  listaSeleccionada (lista: Lista) {

    if (this.terminada) {
      this.router.navigateByUrl(`/tabs/tab2/agregar/${ lista.id }`);  
    } else {
      this.router.navigateByUrl(`/tabs/tab1/agregar/${ lista.id }`);
    }
    
  }
  borrarLista( lista: Lista ) {

    this.deseosService.borrarLista( lista );

  }
  async editarLista(lista: Lista) {

    
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Editar lista',
      inputs:[{
        name: 'titulo',
        type: 'text',
        value: lista.titulo,
        placeholder: 'Nombre de la lista',
      }],
      
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar');
          this.lista.closeSlidingItems();
        }
      },
      {
        text: 'Editar',
        handler: ( data ) => {

          if ( data.titulo.length === 0 ) {
            return;
          }
          
        lista.titulo = data.titulo; 
        
        this.deseosService.guardarStorage();
        this.lista.closeSlidingItems();
        
        }
      }]
    });

    alert.present();
  }
}

```


[Volver al Índice](#%C3%ADndice-del-curso)

## 135. Código fuente de la sección

Ver enlaces y videos de información en el curso.

[Volver al Índice](#%C3%ADndice-del-curso)


# Sección 8:Componentes, directivas de atributos, ciclo de vida y más...
## 137. Introducción a la sección

Ver video resumen de la sección.

[Volver al Índice](#%C3%ADndice-del-curso)

## 138. ¿Qué aprenderemos en esta sección?

Esta sección esta llena de pequeñas cosas útiles que te ayudaran a mejorar la forma en la que usas Angular.

A continuación veremos:

1. Cambios en el estilo de algún elemento HTML utilizando variables en los componentes.
2. Comprender y aplicar CSS en un determinado scope, sin afectar los demás componentes. 
3. Adicionar y remover clases según variables o cualquier tipo de condición.
4. Crearemos directivas personalizadas.
5. Uso del ngSwitch.
6. Crearemos una pequeña aplicación para el uso de rutas y rutas hijas.
7. Comprenderemos el ciclo de vida de un componente o directiva.

Entre otras cosas bien interesantes.

[Volver al Índice](#%C3%ADndice-del-curso)

## 139. Demostración de lo que lograremos al finalizar la sección

Ver vídeo con demostración de los apredizajes de la sección.

[Volver al Índice](#%C3%ADndice-del-curso)

## 140. Creando el proyecto de esta sección - Misceláneos

En esta sección más que una aplicacion como tal, simplemente iremos desarrollando una serie de funcionalidades en un proyecto de prueba, se nos proporciona un archivo con el font-awesome, creamos el proyecto:

>ng new miscelaneos

Renombramos el directorio a 06-miscelaneos por coherencia con el resto de directorios del curso. Las dependencias podríamos instalarlas con npm y de otras maneras, en este caso lo vamos a hacer manual. Descargamos pues los archivos de Bootstrap, jQuery y tether. Ubicamos todo en src/assets/, con los directorios b4/jquery/tether, respectivamente, en B4 copiamos los directorios de css y js de bootstrap que hemos descargado, y lo mismo con los archivos js de jquery y tether respectivamente.

Ahora tenemos que indicar las librerías en nuestro angular.json, para los estilos:

```
"styles": [
    "src/styles.css",
    "src/assets/b4/css/bootstrap.min.css"
],
```

lo mismo para "scripts" referente a jquery y tether, así como el js de bootstrap

```
"scripts": [
  "src/assets/jquery/jquery-3.5.1.slim.min.js",
  "src/assets/tether/tether.js",
  "src/assets/b4/js/bootstrap.min.js"
]
```

También copiaremos el directorio font-awesome dentro de assets, completo, de lo que habíamos descargado y lo añadimos al angular.json

```
"styles": [
                            "src/styles.css",
                            "src/assets/b4/css/bootstrap.min.css",
                            "src/assets/font-awesome/css/font-awesome.min.css"
                        ],
                        "scripts": [
                            "src/assets/jquery/jquery-3.5.1.slim.min.js",
                            "src/assets/tether/tether.js",
                            "src/assets/b4/js/bootstrap.min.js"
                        ]
```

Comprobamos el font-awesome usando un icono en nuestro app.component.html principal:

```
<h1>{{title}}</h1>

<i class="fa fa-2x fa-star"></i>
```



[Volver al Índice](#%C3%ADndice-del-curso)

## 141. ngStyle y su uso con directivas de atributos.

Buscamos la referencia ngStyle en la documentación de Angular. Básicamente nos permite añadir estilos al html "en caliente", es decir, utilizando variables del componente. En la documentación podemos ver que se puede usar de distintas maneras, con variables, objetos, etc. Nosotros lo vamos a ir poniendo en práctica sobre la marcha, creamos un contenedor en nuestro html y nuestro primer componente mediante el CLI.

```
<div class="containter main-container">
  <h1>Demo <small>Angular</small></h1>
  <hr>

  
</div>
```

>ng g c components/ngStyle --skipTests

Vamos a dejar el template y los estilos del componente definidos así para hacer pruebas directamente en el archivo TS :

```
@Component({
  selector: 'app-ng-style',
  template: `


  `,
  styles: []
})
```

Ya podemos definir un primer estilo de esta manera en el template: `<p [ngStyle]="{ 'font-size': '15px' }">`, si por ejemplo definimos en el componente una variable tipo `tamano: number = 30;` podríamos definir estilos con variables de esta manera: `<p [ngStyle]="{ 'font-size': tamano + 'px' }">`.

Otra manera sería `<p [style.fontSize]="'40px'">` o incluso usando variables si definimos el tipo de valor `<p [style.fontSize.px]="tamano">`

Podemos alterar dinámicamente el valor de esas varialbes, un ejemplo sencillo (lo ideal sería lanzar una función con validación en el evento, pero para entenderlo suficiente) sería:

```
<p [style.fontSize.px]="tamano">Hola mundo... esto es una etiqueta
  </p>

  <button class="btn btn-primary" (click)="tamano = tamano + 5">
    <i class="fa fa-plus"></i>
  </button>
  
  <button class="btn btn-primary" (click)="tamano = tamano - 5">
    <i class="fa fa-minus"></i>
  </button>
```

De esta manera podríamos cambiar el font-size de ese elemento dinámicamente.



[Volver al Índice](#%C3%ADndice-del-curso)

## 142. Aplicando CSS a un sólo componente

Para este ejemplo vamos a crear otro componente, esta vez para no tener que reconfigurar la manera de gestionar la plantilla y los estilos (por defecto los crea ambos como archivos aparte y luego en el componente se trazan las rutas a esos archivos), vamos a definir en el CLI que sea inline-template e inline-style (-it -is) de tal manera que todo estará dentro del mismo componente.

>ng g c components/css -it -is

Como ya sabemos, para llamar al componentente usaremos <app-css></app-css> en el app.component.html

Vamos a ver que se pueden aplicar estilos sólo al componente, para ello creamos un párrafo en el app.component.html, y luego hacemos que el componente contenga otro párrafo, si en el componente aplicamos una regla CSS que use como selector 'p' lo aplicará sólo a los 'p' del componente, y no al resto del html.

```
<div class="containter main-container">
    <h1>Demo <small>Angular</small></h1>
    <hr>

    <!-- <app-ng-style></app-ng-style> -->
    <app-css></app-css>

    <p>Hola mundo desde app.component</p>
</div>
```

```
@Component({
  selector: 'app-css',
  template: `
    <p>
      css works!
    </p>
  `,
  styles: [`
  
  p {
    color:red;
    font-size:20px;
  }
  `
  ]
})
```

El texto css works! aparece en rojo y el Hola mundo desde app.component no se ve afectado

Si inspeccionamos el elemento coloreado de rojo podremos ver que el estilo se muestra así:

```
p[_ngcontent-bgp-c11] {
    color: red;
    font-size: 20px;
}
```

Ese "[_ngcontent-bgp-c11]" es un scope que angular está aplicando a ese elemento, lo genera Angular de manera automática para evitar que colisione con otros atributos en los 'p'.

Si aplicásemos estilos en un componente padre los aplicaría sólo al padre y no a los hijos, cada estilo de componente es cargado solo en ese componente.

[Volver al Índice](#%C3%ADndice-del-curso)

## 143. ngClass - Agregando clases de estilos a nuestros elementos HTML

A efectos prácticos usaremos mucho más ngClass que ngStyle, porque lo normal es tener que editar varios atributos css de un elemento, y esto siempre es más funcional con el uso de clases css.

En la documentación de Angular podemos ver las distintas maneras que podemos usar para aplicar esta funcionalidad:

```
<some-element [ngClass]="'first second'">...</some-element>

<some-element [ngClass]="['first', 'second']">...</some-element>

<some-element [ngClass]="{'first': true, 'second': true, 'third': false}">...</some-element>

<some-element [ngClass]="stringExp|arrayExp|objExp">...</some-element>

<some-element [ngClass]="{'class1 class2 class3' : true}">...</some-element>
```

Para este ejemplo crearemos otro componente solamente con el inline-style, porque no es recomendable en los templates escribir mucho código html como ya vimos en las primeras secciones del curso, así que el html del componente estará aparte:

>ng g c components/clases -is

Llamamos al componente en nuestro app.component.html con el selector <app-clases></app-clases> y en bootstrap buscamos los Alerts:

```
<div class="alert alert-primary" role="alert">
  A simple primary alert—check it out!
</div>
<div class="alert alert-secondary" role="alert">
  A simple secondary alert—check it out!
</div>
<div class="alert alert-success" role="alert">
  A simple success alert—check it out!
</div>
...
```

Como vemos cada color y tipo de alert tiene una clase "alert" y otra clase tipo "alert-..." con lo cual nos sirve perfecto para nuestro ejemplo de intercambiar clases con Angular. Podemos usar uno de los div anteriores y dejar solo la clase "alert" como la básica y la segunda clase añadirsela mediante Angular:

```
<div [ngClass]="'alert-info'" class="alert" role="alert">
    A simple success alert—check it out!
</div>
```

Tambien podríamos crear una variable (alerta) que contenga el nombre de la clase y en el html llamar a dicha variable:

```
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clases',
  templateUrl: './clases.component.html'
})
export class ClasesComponent implements OnInit {

  alerta:string = "alert-danger";

  constructor() { }

  ngOnInit(): void {
  }

}
```

```
<div [ngClass]="alerta" class="alert" role="alert">
    A simple success alert—check it out!
</div>
```

Lo siguiente va a ser, aprovechando que ya tenemos una variable "alerta" crear unos botones para que cuando suceda el evento (click) el nombre de la clase cambie y, por tanto, se cambie el estilo del elemento:

```
<div [ngClass]="alerta" class="alert" role="alert">
    A simple success alert—check it out!
</div>

<button type="button" class="btn btn-info" (click)="alerta = 'alert-info'">Info</button>
<button type="button" class="btn btn-success" (click)="alerta = 'alert-success'">Success</button>
```

Ahora en lugar de string con los nombres de las clases vamos a usar objetos que contengan propiedades css.

Para ello definimos un nuevo objeto que tenga la propiedad "danger" que será un booleano:

```
propiedades:Object = {
    danger: true
  }
```

Nota: Definir propiedades como tipo "Object" no funciona tal cual en las últimas versiones de TypeScript, por lo cual para que funcione lo declararé como tipo "any".

De esta manera, en el html, podemos usar ngClass con condicionales, aplicará la clase en cuestión "text-danger" o "text-info" en función de si el objeto propiedades.danger es verdadero o falso. Y podemos definir un botón que con el evento click cambie la bandera "danger" de verdadero a falso y viceversa. Podríamos incluso usar esto para que el mismo estilo del botón cambie en función de eso:

```
<h3 [ngClass]=" { 'text-danger': propiedades.danger, 'text-info': !propiedades.danger } ">Hola Mundo</h3>

<button [ngClass]="{ 'btn-danger': propiedades.danger, 'btn-info': !propiedades.danger }" (click)="propiedades.danger = !propiedades.danger" type="button" name="button" class="btn">Cambiar</button>
```


[Volver al Índice](#%C3%ADndice-del-curso)

## 144. Usando procesos asíncronos con indicadores de usuario

En esta sección vamos a seguir trabajando en el ejemplo anterior, vamos a añadir un botón con un icono de font-awesome, vamos a usar el icono de guardar y el icono de refrescar, con la clase "fa-spin" de font-awesome podemos hacer que el icono gire (dando un efecto al fa-spin). Así que definimos el botón:

```
<button class="btn btn-primary" type="button" name="button"><i class="fa fa-save"></i>Guardar cambios</button>
```

Y por otro lado, en el componente, definimos una variable booleana "loading" que definiremos en "false" de primeras `loading:boolean = false;`, y una función "ejecutar" que cuando se active cambie loading a true y que haga un timeout que usaremos para simular un proceso asíncrono, que hará que tras 3 segundos el valor de loading cambie de nuevo a false.

```
ejecutar(){

    this.loading = true;

    setTimeout( () => this.loading = false, 3000 );
    
  }
```

Entonces en el botón tendremos que definir el evento click para que ejecute la función ejecutar y en el icono usaremos el ngClass para definir condicionalmente que tendrá la clase "fa-save" si loading es falso (es decir, no está cargando) y que tenga la clase "fa-refresh" y "fa-spin" si loading es verdadero.


```
<button (click)="ejecutar()" class="btn btn-primary" type="button" name="button">
    <i [ngClass]="{ 'fa-save':!loading, 'fa-refresh fa-spin':loading }" class="fa"></i>
    Guardar cambios</button>
```

Para terminar vamos a mejorarlo un poco y hacer que el botón se desactive cuando esté cargando, si la propiedad html "disabled" la ponemos entre llaves cuadradas y la igualamos a la variable loading estamos generando una condicional en función de dicha variable, podemos usar esta variable también para cambiar el texto del botón, quedaría entonces el botón de esta manera:

```
<button (click)="ejecutar()" class="btn btn-primary" type="button" name="button" [disabled]="loading">
    <i [ngClass]="{ 'fa-save':!loading, 'fa-refresh fa-spin':loading }" class="fa"></i>
    <span *ngIf="!loading">Guardar cambios</span>
    <span *ngIf="loading">Espere por favor...</span>
</button>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 145. Directivas personalizadas

Supongamos que queramos crearnos un mecanismo que funcione cuando le ponemos una etiqueta o atributo a algún componente html o bien a otro componente.

Como base para el ejercicio vamos a tener un sencillo párrafo en nuestro app.component.html, a continuación vamos a crear una directiva personalizada mediante el CLI. Para ello definiremos un nuevo directorio en src/app llamado "directives" y crearemos una directiva llamada "resaltado", porque lo que queremos es que cuando le pongamos ese atributo al elemento lo resalte:

>ng g d directives/resaltado

Si vamos al componente de directiva creado, podemos definir un console.log en el constructor para saber cuando es creado.

```
import { Directive } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor() {
    console.log('Directiva llamada');
  }

}
```

Si ahora queremos referenciar a esa directiva podemos hacerlo usando el selector indicado 'appResaltado' en el elemento de párrafo que tenemos de esta manera, y así podremos ver en consola el mensaje que nos indica que esta funcionando y verificamos que funciona:

```
<p appResaltado>Hola mundo desde app.component</p>
```

Ahora para poder trabajar con referencias a elementos html y así poder aplicar directivas a esos elementos necesitamos importar de @Angular/core el módulo ElementRef, que le pasaremos al constructor para poder crear una instancia de dicho módulo, una vez que tenemos esa instancia podemos aplicar los métodos de la misma, para seleccionar el elemento y por ejemplo acceder al style del mismo y modificarlo:

```
import { Directive, ElementRef } from '@angular/core';


@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor( private el:ElementRef ) {
    console.log('Directiva llamada');
    el.nativeElement.style.backgroundColor = 'yellow';
  }

}
```

Podríamos usar esto, por ejemplo, para cuando el cursor se situe sobre el elemento se aplique ese color y en caso contrario no pase nada. Para ello necesitaríamos otro módulo adicional, que nos permita "escuchar" si este "evento" sucede, hablamos de HostListener, este no necesitaremos inyectarlo en el constructor, para poder usarlo lo haremos mediante un decorador al cual le tendremos que pasar el evento que queremos escuchar, en este caso 'mousenter' (si buscamos en internet podremos encontrar algunas listas sobre todos los tipos de eventos disponibles), también definiremos la función que se disparará (mouseEntro()) al escuchar dicho evento. También se puede definir lo mismo para cuando el puntero abandone el elemento:

```
@HostListener('mouseenter') mouseEntro(){
  this.el.nativeElement.style.backgroundColor = 'yellow';
}

@HostListener('mouseleave') mouseSalio(){
  this.el.nativeElement.style.backgroundColor = null;
}
```

Vamos a complicar un poco el ejemplo, añadir la posibilidad de, desde el html, pasarle un parámetro que sea el color con el que jugar. Poniendo el selector entre corchetes [appResaltado] nos posibilita pasarle un valor, como sabemos, tipo `[appResaltado]="'orange'"`, para poder recibirlo en el componente necesitamos importar otro módulo, que ya conocemos, Input. Una vez importado podemos crear un decorador @Input en el componente para indicar que ese appResaltado que pusimos entre corchetes va a enviar un valor, declarando junto al decorador una variable podremos capturar el valor en dicha variable:

```
@Input('appResaltado') nuevoColor:string;
```

Si queremos podemos definir una función privada solo accesible desde el propio componente, que reciba como parametro un color y que dicha función se ejecute en el mouseenter, de tal manera que si no se definió un color en el html tenga un por defecto (yellow), o por el contrario coja el del html. Quedarían el html y el ts respectivamente así:

app.component.html

```
<div class="container main-container">
    <h1>Demo <small>Angular</small></h1>
    <hr>

    <!-- <app-ng-style></app-ng-style> -->
    <!-- <app-css></app-css> -->
    <!-- <app-clases></app-clases> -->

    <p [appResaltado]="'teal'">Hola mundo desde app.component</p>
</div>
```

resaltado.directive.ts

```
import { Directive, ElementRef, HostListener, Input } from '@angular/core';


@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor( private el:ElementRef ) {
    console.log('Directiva llamada');
    // el.nativeElement.style.backgroundColor = 'yellow';
  }

  @Input('appResaltado') nuevoColor:string;

  @HostListener('mouseenter') mouseEntro(){

    this.resaltar( this.nuevoColor || 'yellow');

  }

  @HostListener('mouseleave') mouseSalio(){

    this.resaltar(null);

  }

  private resaltar( color: string ){

    this.el.nativeElement.style.backgroundColor = color;

  }
}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 146. ngSwitch - Múltiples opciones con una sola decisión

Aquí aprenderemos a manejar otra directiva estructural, ngSwitch, que nos permite (como el switch de JS) tener una condicion con multiples opciones a elegir como posibles resultados. Vamos a volver a usar las Alerts de Bootstrap para ejemplificarlo. Vamos a crear un nuevo componente:

>ng g c components/ngSwitch -is

Añadimos lo primero en el app.componente el selector del nuevo componente, luego en el componente vamos a declarar una variable de tipo string llamada alerta con el valor 'info' `alerta:string = "info"`.

Ahora en el html de ng-switch.component, si tenemos los snippets de Angular instalados podemos escribir ng-switch y tabulador y nos crea un esqueleto base de ng-switch:

```
<span [ngSwitch]="">
<p *ngSwitchCase="true">

</p>
<p *ngSwitchCase="false"> 

</p>
<p *ngSwitchDefault>

</p>
</span>
```

[ngSwitch] está esperando una expresión que será la condición, en nuestro caso será la variable 'alerta' previamente creada, luego tendremos cada uno de los casos ngSwitchCase (nos puso dos casos tipicos true o false, nosotros definiremos los nuestros) y luego el valor por defecto ngSwitchDefault, a priori tendríamos algo así:

```
<button type="button" (click)="alerta='success'" class="btn btn-default"> Cambiar </button>
<div [ngSwitch]="alerta">
    <div *ngSwitchCase="'success'" class="alert alert-success" role="alert">
        A simple success alert—check it out!
    </div>
    <div *ngSwitchDefault class="alert alert-danger" role="alert">
        A simple danger alert—check it out!
    </div>
    <div *ngSwitchCase="'warning'" class="alert alert-warning" role="alert">
        A simple warning alert—check it out!
    </div>
    <div *ngSwitchCase="'info'" class="alert alert-info" role="alert">
        A simple info alert—check it out!
    </div>
</div>
```

De esa manera como tendríamos por defecto en la variable declarada el valor 'info' nos mostraría a priori ese alert, luego al pulsar el botón pasaríamos a ver el div del success.

[Volver al Índice](#%C3%ADndice-del-curso)

## 147. Rutas y Rutas Hijas

En esta sección revisaremos el concepto de rutas y rutas hijas, que nos sirve para navegar entre páginas y subpáginas. Para ello vamos a crear el componente home que hará de la página principal, y en su template pondremos los ejercicios hechos hasta ahora.

>ng g c components/home -it -is

```
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <app-ng-style></app-ng-style>
    <app-css></app-css>
    <app-clases></app-clases>
    <app-ng-switch></app-ng-switch>

    <p>Hola mundo desde app.component</p>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
```

Por convención el archivo de rutas se suele crear o ubicar en el mismo nivel de directorios que el app.module.ts, así que allí crearemos el app.routes.ts, usando los snippets vamos a generar el contenido de lo que sería un AppRouterModule:

```
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'path', component: FeatureComponent },
    { path: '**', component: PageNotFoundComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
```

Tendremos que importar el módulo de home y añadir el path, también verificar (mi VSCode lo hace automáticamente) que se añade en app.module.ts.

En el ejemplo es una versión más antigua y en app.module.ts también se añade la constante que exporta el app.routes.ts, pero en las versiones actuales lo que se exporta es un módulo como tal AppRoutingModule, tendremos que verificar que se importa correctamente en el app.module.ts

Ahora en nuestro app.component.html pondremos el selector <router-outlet>, como ya hemos definido una home este selector tomará como componente principal dicho componente.

Una vez hecho esto vamos a crear dos componentes más para el tema de rutas hijas, serán usuario y usuarioNuevo

>ng g c components/usuario
>ng g c components/usuario/usuarioNuevo -it -is --flat (esto hace que no cree directorio para el componente)
>ng g c components/usuario/usuarioEditar -it -is --flat
>ng g c components/usuario/usuarioDetalle -it -is --flat

Para gestionar el navegar entre páginas vamos a usar un navbar que generaremos como componente y pondremos un html que nos guste, usaremos un navbar de bootstrap en este caso.

>ng g c components/navbar -is

El selector del navbar lo colocaremos al principio del todo en nuestro app.component.html

Y dejaremos definidas las rutas a usuario y el html:


app.routes.ts

```
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UsuarioComponent } from './components/usuario/usuario.component';


const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'usuario', component: UsuarioComponent },
    { path: '**', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(ROUTES)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
```

navbar.component.html

```
<nav class="navbar navbar-expand-lg navbar-inverse bg-inverse">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Demos</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item" routerLinkActive="active">
                    <a class="nav-link" [routerLink]="['home']" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item" routerLinkActive="active">
                    <a class="nav-link" [routerLink]="['usuario']" href="#">Usuario</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 148. Rutas Hijas

La idea es poder gestionar rutas tipo `http://localhost:4200/usuario/10/detalle` en la que tengamos un id (que sería un parámetro que se le pasaría a la ruta 'usuario') y luego una subruta, ruta hija, de 'usuario', que para ello habíamos creado 3 componentes, 'detalle', 'editar' y 'nuevo'

Para hacer la navegación interna vamos a usar un button-group de Bootstrap, que incluiremos en el usuario.component.html:

```
<div class="row">
    <div class="col-md-12 text-center">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-primary">Nuevo</button>
            <button type="button" class="btn btn-primary">Editar</button>
            <button type="button" class="btn btn-primary">Detalle</button>
        </div>
    </div>
</div>
<router-outlet></router-outlet>
```

Luego para definir las rutas hijas es bastante sencillo, en app.routes.ts al path:'usuario' le añadiremos una propiedad más 'children', que será un array con todas las rutas hijas. Definiremos eso y que la ruta padre espere recibir el parámetro 'id', en el link a usuario le pasaremos una constante '10' para poder ir trabajando(navbar.component.html: `<a class="nav-link" [routerLink]="['usuario',10]" href="#">Usuario</a>`), más adelante lo dejaremos como variable, el app.routes.ts queda:

```
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UsuarioDetalleComponent } from './components/usuario/usuario-detalle.component';
import { UsuarioEditarComponent } from './components/usuario/usuario-editar.component';
import { UsuarioNuevoComponent } from './components/usuario/usuario-nuevo.component';
import { UsuarioComponent } from './components/usuario/usuario.component';


const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'usuario/:id', component: UsuarioComponent,
        children: [
            { path: 'nuevo', component: UsuarioNuevoComponent },
            { path: 'detalle', component: UsuarioDetalleComponent },
            { path: 'editar', component: UsuarioEditarComponent },
        ] },
    { path: '**', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(ROUTES)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
```

Todavía no hemos definido la funcionalidad de los botones, pero podemos tratar de navegar definiendo la url manualmente en el navegador, si hacemos esto ahora veremos que no funciona porque no encuentra un sitio donde renderizar el contenido de los componentes, tenemos que definir el selector <router-outlet> para indicar que ahí se deben renderizar los componentes hijos. Finalmente dejaremos definidas las rutas en el button-group con su clase active y todo:

```
<div class="row">
    <div class="col-md-12 text-center">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-primary" routerLinkActive="active" [routerLink]="['nuevo']">Nuevo</button>
            <button type="button" class="btn btn-primary" routerLinkActive="active" [routerLink]="['editar']">Editar</button>
            <button type="button" class="btn btn-primary" routerLinkActive="active" [routerLink]="['detalle']">Detalle</button>
        </div>
    </div>
</div>
<router-outlet></router-outlet>
```

Por perfeccionarlo también definiremos que cuando se navegue a cualquier ruta de usuario que no sea ninguna de las hijas (no se pase id, o simplemente sea la primera vez que se pulsa 'usuario' en el navbar) lleve por defecto a la subpágina 'nuevo':

```
const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'usuario/:id', component: UsuarioComponent,
        children: [
            { path: 'nuevo', component: UsuarioNuevoComponent },
            { path: 'detalle', component: UsuarioDetalleComponent },
            { path: 'editar', component: UsuarioEditarComponent },
            { path: '**', redirectTo: 'nuevo' },
        ] },
    { path: '**', component: HomeComponent },
];
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 149. Separar las rutas hijas a un archivo especifico

Vamos a separar las rutas hijas del resto de rutas, puesto que estamos definiendo todas hasta ahora en el archivo app.routes.ts, imaginemos que en un proyecto grande que la web tenga muchas rutas se nos podría quedar un archivo demasiado grande y con mucha información agolpada.

En nuestro caso vamos a crear un archivo que albergue todas las rutas del usuario, imaginemos que aparte de este usuario tuvieramos muchas otras páginas que a su vez tuvieran rutas hijas, así podríamos dividirlas por "secciones".

Dentro del directorio usuario donde tenemos todos los componentes creemos un nuevo archivo llamado usuario.routes.ts que nos permita controlar todas las rutas referentes al usuario, para ello lo crearemos con snippet como en el ejemplo anterior, o simplemente escribiremos la estructura básica de un módulo de rutas que ya conocemos.

Así que en este nuevo archivo lo único que necesitamos es la librería Routes e importar los distintos componentes hijos de usuario (quitando las importaciones del padre pues ya no las necesitaremos allí) y exportar una constante con el array de las rutas hijas, importando luego en el padre dicha constante y añadiéndosela a la propiedad children de las rutas del padre. Quedando archivo de rutas padre e hijas de esta manera, respectivamente:

app.routes.ts

```
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { USUARIO_ROUTES } from './components/usuario/usuario.routes';

const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'usuario/:id', component: UsuarioComponent,
      children: USUARIO_ROUTES },

    { path: '**', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(ROUTES)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
```

usuario.routes.ts

```
import { Routes } from '@angular/router';

import { UsuarioDetalleComponent } from './usuario-detalle.component';
import { UsuarioEditarComponent } from './usuario-editar.component';
import { UsuarioNuevoComponent } from './usuario-nuevo.component';

export const USUARIO_ROUTES: Routes = [
            { path: 'nuevo', component: UsuarioNuevoComponent },
            { path: 'detalle', component: UsuarioDetalleComponent },
            { path: 'editar', component: UsuarioEditarComponent },
            { path: '**', redirectTo: 'nuevo' }
];
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 150. Obteniendo parámetros del padre, desde las rutas hijas

En este supuesto en el que estamos trabajando consideramos que deberíamos poder recibir el parámetro que se le pasa al padre desde las rutas hijas.

Para ello antes de todo necesitamos poder manejar los parametros con ActivatedRoute en el componente usuario. Para ello importamos la librería e inyectamos una instancia de la misma en el constructor, llamándo en dicho constructor al observador del router para que escuche si se pasa un id (que ya habíamos definido previamente en los path del app.routes.ts).

El unico cambio real que hay es que a la hora de llamar al método observador de parámetros previamente se le indica que tiene que escuchar del padre, tan sencillo como eso, y así podríamos anidar infinitamente rutas, recordando que tendríamos que llamar en los respectivos html a <router-outlet> para poder renderizar cada uno de los contenidos de esas subrutas, quedarían respectivamente los componentes padre e hijo de esta manera:

usuario.component.ts (padre):

```
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
})
export class UsuarioComponent implements OnInit {

  constructor( private router:ActivatedRoute) {

    this.router.params.subscribe( parametros => {
      console.log("Ruta padre:");
      console.log(parametros);
    });
   }

  ngOnInit(): void {
  }

}
```

usuario-nuevo.component.ts (hijo):

```
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-nuevo',
  template: `
    <p>
      usuario-nuevo works!
    </p>
  `,
  styles: [
  ]
})
export class UsuarioNuevoComponent implements OnInit {

  constructor( private router:ActivatedRoute) {

    this.router.parent.params.subscribe( parametros => {
      console.log("Ruta Hija: Usuario nuevo");
      console.log(parametros);
    });
   }

  ngOnInit(): void {
  }

}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 151. Ciclo de vida completo de un componente

El método ngOnInit() forma parte del ciclo de vida de un componente, en el proceso de construcción/destrucción de un componente podemos llamar a los siguientes métodos:

- ngOnInit (Cuando el componente está inicializando, o después del ngOnChanges)
- ngOnChanges (Cuando los datos en las propiedades del componente cambian de alguna manera)
- ngDoCheck (Cada vez que se hace una revisión del ciclo de revisión de cambios)
- ngAfterContentInit (Después de insertar contenido (<app-alguna-pagina>))
- ngAfterContentChecked (Después de la revisión del contenido insertado)
- ngAfterViewInit (Despues de la inicialización de los componentes y componentes hijos)
- ngAfterViewChecked (Después de cada una de las revisiones de los componentes y componentes hijos)
- ngOnDestroy (Justo antes de que se destruya el componente o directiva, por ej al movernos de vista, o incluso si hacemos un *ngIf del elemento)

Todo esto lo veremos con una función en la siguiente y última sección de la lección

[Volver al Índice](#%C3%ADndice-del-curso)

## 152. Demostración del ciclo de vida de un componente

Como información adicional al ciclo de vida se puede consultar en la documentación de Angular la sección Lifecycle Hooks.

Para nuestra práctica vamos a importar todas esas librerías en nuestro home.component.ts

```
import { Component, OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';
```

Adicionalmente tendremos que añadirlas en el 'implements' de la exportación de la clase. A continuación todo el código de prueba para ver los efectos en la consola mientras testeamos nuestra demo.

Notar que realmente útiles a priori son el OnInit, OnChange y el OnDestroy (este se da cada vez que se cambia de página, por ejemplo y hay que eliminar componentes para añadir otros que lo sustituyan).

home.component.ts

```
import { Component, OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

// tslint:disable-next-line: no-conflicting-lifecycle
@Component({
  selector: 'app-home',
  template: `
    <app-ng-style></app-ng-style>
    <app-css></app-css>
    <app-clases></app-clases>
    <app-ng-switch></app-ng-switch>

    <p>Hola mundo desde app.component</p>
  `,
  styles: [
  ]
})
// tslint:disable-next-line: max-line-length
export class HomeComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

  constructor() {
    console.log('constructor');
  }

  ngOnInit(): void {
    console.log('ngOnInit');
  }
  // tslint:disable-next-line: typedef
  ngOnChanges(){
    console.log('ngOnChanges');
  }
  // tslint:disable-next-line: typedef
  ngDoCheck(){
    console.log('ngDoCheck');
  }
  // tslint:disable-next-line: typedef
  ngAfterContentInit(){
    console.log('ngAfterContentInit');
  }
  // tslint:disable-next-line: typedef
  ngAfterContentChecked(){
    console.log('ngAfterContentChecked');
  }
  // tslint:disable-next-line: typedef
  ngAfterViewInit(){
    console.log('ngAfterViewInit');
  }
  // tslint:disable-next-line: typedef
  ngAfterViewChecked(){
    console.log('ngAfterViewChecked');
  }
  // tslint:disable-next-line: typedef
  ngOnDestroy(){
    console.log('ngOnDestroy');
  }
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

# Sección 9:Aplicación #5: Aplicación con autenticación Auth0
## 153. Introducción a la sección

En esta sección trabajaremos con Auth0 un sistema de autenticación para bloquear rutas de nuestra aplicación, es decir sólo queremos mostrar de manera pública a usuarios no autenticados unas pantallas y mediante Auth0 mostraremos información de páginas que sólo serán visibles tras autenticación. Podremos crear autenticaciónes mediante Facebook, Twitter, etc.

[Volver al Índice](#%C3%ADndice-del-curso)

## 154. ¿Qué aprenderemos en esta sección?

A continuación trabajaremos en una pequeña aplicación que tendrá como finalidad, utilizar el sistema de autenticación de usuarios Auth0.

Tras concluirla, aprenderemos mucho sobre:

1. ¿Qué es Auth0 y la documentación basada en Angular 2?

2. Autenticación con Facebook, Twitter, Google, entre otros...

3. Creación de un formulario de captura para la creación de usuarios.

4. Personalización de la caja de login.

5. Uso de servicios para bloquear rutas que no son válidas si el usuario no esta autenticado.

6. Obtener la información del perfil del usuario ingresado.

7. Entre otras cosas útiles para nuestras aplicaciones.

Espero que estén emocionados con este mecanismo de autenticación!

[Volver al Índice](#%C3%ADndice-del-curso)

## 155. Demostración de lo que lograremos al finalizar la sección

Ver video explicativo con el resultado de la sección.

[Volver al Índice](#%C3%ADndice-del-curso)

## 156. Aplicación con autenticación Auth0 - Inicio del proyecto

La base del proyecto es usar la librería de Auth0, que es gratuita hasta cierto límite de usuarios. 

Podemos ver la información en su web auth0.com, allí nos crearemos una cuenta y nos registraremos.

Una vez tengamos una cuenta, tendremos acceso a un dashboard con toda la información que se puede manejar, usuarios, logins, etc.

Lo siguiente será crear el proyecto con el Angular CLI, cuando nos pregunte si queremos generar automaticamente el sistema de rutas le diremos que si (ya hemos venido aprendiendo como generarlo manualmente) y para los estilos elegiremos css:

>ng new authapp

Renombraremos el proyecto a 07-authapp y lo dejaremos vacío para trabajar.

Añadiremos al index.html el link para bootstrap `<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">`

A continuación crearemos unos cuantos componentes para las páginas del proyecto:

>ng g c components/navbar -is --skipTests
>ng g c components/home -is --skipTests
>ng g c components/protegida -is --skipTests
>ng g c components/precios -is --skipTests

Lo siguiente será añadir un navbar de bootstrap al html en cuestión, elegiremos uno que tiene un texto inline a la derecha que sustituiremos por los botones de login y logout posteriormente y dejaremos definidos los valores de textos, etcétera:

```
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Auth0 App</a>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Precios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Protegida</a>
                </li>
            </ul>
            <span class="navbar-text">
          Navbar text with an inline element
        </span>
        </div>
    </div>
</nav>
```

Como mencionamos anteriormente ya tenemos definidos los archivos para las rutas (app-routing.module.ts), solo necesitamos añadir los path:

```
const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'precios', component: PreciosComponent},
  {path: 'protegida', component: ProtegidaComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];
```

En nuestro app.component.html sustituimos el h1 que teníamos en el container por <router-outlet> para indicar que tiene que renderizar las vistas de las rutas ahí.

```
<app-navbar></app-navbar>

<div class="container">
    <router-outlet></router-outlet>
</div>
```

Y en nuestro navbar.component.ts definiremos las rutas de los links y las clases de link activo:

```
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand">Auth0 App</a>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" routerLinkActive="active" routerLink="/home" aria-current="page">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" routerLinkActive="active" routerLink="/precios">Precios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" routerLinkActive="active" routerLink="/protegida">Protegida</a>
                </li>
            </ul>
            <span class="navbar-text">
          Navbar text with an inline element
        </span>
        </div>
    </div>
</nav>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 157. Creando la interfaz de nuestra aplicación

Llegados a este punto, tendremos que ir a nuestro Dashboard de la web de auth0 y en Applications crear una nueva aplicación.

Le indicaremos un nombre y seleccionaremos que vamos a usar una SPA. Posteriormente nos preguntará qué tecnología vamos a usar, indicamos Angular. Entonces tendremos acceso a tutoriales o guías de los pasos a seguir para la implementación y uso. Nosotros vamos a tener en cuenta lo que vamos a necesitar solamente.

Empezaremos por configurar los Callbacks de url, lo necesitaremos para cuando salgamos de nuestra aplicacion para acceder a la página de login luego poder regresar con la información pertinente (login, logout y los datos de usuario). Para ello iremos a los Settings de nuestra aplicación recién creada y en "Allowed Callback URLs", "Allowed Logout URLs" y "Allowed Web Origins" tendremos que añadir "http://localhost:4200" (para desarrollo, para producción sería otra, sería la url base de nuestra web).

Ahora en nuestra aplicación tenemos que instalar la librería de Auth0, lo vamos a hacer mediante el npm (node package manager)

>npm install @auth0/auth0-angular --save (puede cambiar en distintas versiones el paquete a instalar)

A continuación en el curso se indica como crear un servicio de autenticación, pero mirando la documentación actual de Auth0 para Angular veo que esto no es necesario, a cambio lo que indica es que tenemos que registrar y configuarar el paquete instalado en el app.module.ts, definiendo en los imports el domain y el clientID, quedaría nuestro app.module.ts de esta manera:

```
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { ProtegidaComponent } from './components/protegida/protegida.component';
import { PreciosComponent } from './components/precios/precios.component';

// Import the module from the SDK
import { AuthModule } from '@auth0/auth0-angular';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ProtegidaComponent,
    PreciosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // Import the module into the application, with configuration
    AuthModule.forRoot({
      domain: 'dev-ezhy03op.eu.auth0.com',
      clientId: '5taG31RxvW6XMmd3W9fFRSRzeH3z0ZG5'
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 158. Comenzando con Auth0 - Componentes y servicios

Parece ser que la implementación de Auth0 en la versión actual con respecto a Angular ha mejorado mucho. En esta lección se explica como configurar el servicio que en la lección anterior fue creado manualmente, nosotros no hicimos eso y ya está todo listo para funcionar, simplemente instalando la librería e importándola en el app.module.ts ya podemos empezar a trabajar en ella.

Así que me saltaré los pasos, ver el curso para saber cómo se hacía antes, y pasaré directamente al punto en el que se pueda empezar a poner en uso Auth0.

Si comentar antes que se mencionan los distintos métodos de que dispone Auth0, en la documentación de Auth0 se puede consultar todo.

Una vez tenemos la librería podemos inyectar una instancia del servicio en el constructor del app.component.ts, asegurándonos que lo importa del paquete instalado de Auth0.

```
import { Component } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Auth0 Demo';

  constructor( private auth: AuthService) {}  
}
```

Una vez que lo tenemos en el núcleo de la aplicación podemos usarlo en cualquier parte de la web, en este caso lo vamos a implementar en nuestro navbar, pues es ahí donde estará el botón de login/logout y necesitaremos llamarlo cuando se usen estos botones, para ello tendremos que importarlo e inyectarlo en el constructor del navbar.component.ts, pero lo inyectaremos como instancia pública, pues debe ser accesible por el html donde se gestionará la autenticación.

Antes de esto haremos los botones

```
<button type="button" class="btn btn-outline-primary">Ingresar</button>
<button type="button" class="btn btn-outline-danger">Salir</button>
```


NOTA: Rectifico de nuevo manteniendo lo anterior, voy a empezar la sección de nuevo esperando que funcione con los métodos antiguos pese a tener las librerías nuevas ..... o una mezcla de todo, a ver qué sale:

Tras instalar el paquete nuevo me dispongo a generar un servicio:

>ng g s services/auth --skipTests

No es necesario incluirlo en el app.module.ts siempre y cuando esté definida la propiedad providedIn: 'root' :

```
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
}
```

De esta manera será accesible en toda la aplicación, cosa fundamental para un servicio.

[Volver al Índice](#%C3%ADndice-del-curso)

## 159. Configuraciones en Auth0 dashboard y uso del servicio

Aquí lo primero que se indica en el curso es copiar el código que debería llevar el archivo auth.service.ts.

De momento voy a pasar este paso, porque ahora teóricamente el servicio se encuentra 'dentro' del AuthModule y se inyecta en el constructor del componente desde la librería AuthService que está en la nueva librería de Auth0. Supongo que así el servicio queda externo a la aplicación y más seguro aún.

Como digo me saltaré los pasos y trataré de que funcione el ejemplo del curso con las librerías actuales de Auth0.

Yo tengo actualmente el navbar así, es posible que lo rectifique en la siguiente sección:

```
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand">Auth0 App</a>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" routerLinkActive="active" routerLink="/home" aria-current="page">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" routerLinkActive="active" routerLink="/precios">Precios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" routerLinkActive="active" routerLink="/protegida">Protegida</a>
                </li>
            </ul>
            <span class="navbar-text">
                <button *ngIf="auth.isAuthenticated$" type="button" class="btn btn-outline-primary">Ingresar</button>
                <button *ngIf="!auth.isAuthenticated$" type="button" class="btn btn-outline-danger">Salir</button>
        </span>
        </div>
    </div>
</nav>
```

El navbar.component.ts queda:

```
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor( public auth: AuthService) { }

  ngOnInit(): void {
  }

}

```

IMPORTANTE - ACTUALIZACIÓN - NOTA:

Confirmado que todo es mucho más sencillo ahora, resumo los pasos hasta el momento y es posible que la sección 160 no sea necesaria:

1) Instalar el SDK de Auth0 con el Angular CLI
2) Importar el módulo de Auth0 en app.module.ts
3) Inyectar en el constructor de app.components.ts una instancia privada de AuthService de la librería '@auth0/auth0-angular'
4) Inyectar en el constructor del componente que gestion el login (navbar.component.ts) una instancia pública de AuthService desde @auth0/auth0-angular
5) Importar Inject de @angular/core y DOCUMENT de @angular/common para inyectar en el constructor una instancia del DOCUMENT html y así poder usarlo para localizar dónde redirigir cuando se haga logout.

Quedarían así todos los archivos:

navbar.component.ts:

```
import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor( @Inject(DOCUMENT) public document: Document, public auth: AuthService ) {}

  ngOnInit(): void {
  }

}
```

navbar.component.html

```
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand">Auth0 App</a>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" routerLinkActive="active" routerLink="/home" aria-current="page">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" routerLinkActive="active" routerLink="/precios">Precios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" routerLinkActive="active" routerLink="/protegida">Protegida</a>
                </li>
            </ul>
            <span class="navbar-text">
                <ng-container *ngIf="auth.isAuthenticated$ | async; else loggedOut">
                    <button class="btn btn-outline-danger" (click)="auth.logout({ returnTo: document.location.origin })">Salir</button>
                </ng-container>
              
                <ng-template #loggedOut>
                    <button class="btn btn-outline-primary" (click)="auth.loginWithRedirect()">Ingresar</button>
                </ng-template>
            </span>
        </div>
    </div>
</nav>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 160. Conectar los botones con los métodos de Auth0

En esta lección se definen las funciones de los botones de login y logout. Con la configuración del html y los archivos de typescript de la sección anterior es suficiente, no es necesario lo indicado en esta sección, también en esta sección se debe crear un callback, cosa que tampoco es necesario con la versión actual de Auth0 (como quedó definico el auth.logout del html ya se encarga de la redirección con el parámetro returnTo que se le pasa, asimismo el método le login cambió)

[Volver al Índice](#%C3%ADndice-del-curso)

## 161. Servicio de bloqueo - CanActivate - AuthGuard

Seguimos adecuando el código a la última versión de Auth0 y Angular.

Primero vamos a ocultar o mostrar el link "Protegida" del navbar en función de si está logueado o no el usuario:

`<li class="nav-item" *ngIf="auth.isAuthenticated$ | async">`

Nótese que no se comentó en las secciones anteriores, pero como estamos usando el observable isAuthenticated$ (por convención los observables se definen con $ al final), debemos usar el pipe async, de esta manera siempre que se renderice el html se comprobará el último valor del observable.

Ahora tenemos que prevenir que no se pueda acceder a esa página si no se está autenticado. (Actualmente si escribiéramos la url directamente podríamos acceder a la página "Protegida" `http://localhost:4200/protegida`).

Aqui también tenemos la mayoría del trabajo hecho, basta con irnos a nuestro archivo de rutas, en este caso 'app-routing.module.ts', importar AuthGuard de la librería y definir la ruta 'protegida' de esta manera:

```
{path: 'protegida', component: ProtegidaComponent, canActivate: [AuthGuard]},
```

Sólo con esto cada vez que tratemos de acceder a la url /protegida sin estar autenticados el resultado será que nos redirigirá a la página externa de login de Auth0 para que lo hagamos.

```
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PreciosComponent } from './components/precios/precios.component';
import { ProtegidaComponent } from './components/protegida/protegida.component';
// Import the authentication guard
import { AuthGuard } from '@auth0/auth0-angular';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'precios', component: PreciosComponent},
  {path: 'protegida', component: ProtegidaComponent, canActivate: [AuthGuard]},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 162. Obteniendo el perfil del usuario que inició sesión

Tendremos que inyectar el servicio de AuthService en el componente de la página protegida, de manera pública.


```
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-protegida',
  templateUrl: './protegida.component.html',
  styles: [
  ]
})
export class ProtegidaComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit(): void {
  }

}
```

Ahora en el html podemos llamar al objeto user$ que es un observable del servicio auth que hemos inyectado en el constructor y ver sus atributos:

```
<h1>Perfil del usuario</h1>

<hr>
<pre *ngIf="auth.user$ | async as profile">
    <code>{{ profile | json }}</code>
</pre>
```

Un atributo muy importante que podemos recibir así es el ID de la conexión de autenticación, para localizar los logins:

```
"sub": "google-oauth2|1106....."
```

Antes había un problema con la recarga de la página que no mantenía la sesión y se explica en el curso como solucionarlo, pero en las versiones actuales de Auth0 eso ya fue arreglado.

Finalmente decir que con visitar la documentación de Auth0 he podido adecuar todo a nuestro proyecto, está todo muy bien explicado y resumido (mucho mejor que cuando se hizo el curso), y es muy intuitivo a la hora de gestionar y customizar los logins, así como muchísimas funcionalidades que tiene Auth0.

[Volver al Índice](#%C3%ADndice-del-curso)

# Sección 10: Bonus: Login tradicional y manejo de tokens - Firebase
## 164. Introducción a la sección

Ver video introductorio de la sección

[Volver al Índice](#%C3%ADndice-del-curso)

## 165. Temas puntuales de la sección

El objetivo de la sección, es trabajar con un proceso de autenticación por token tradicional, veremos temas como:

* Validar formularios

* Tokens

* LocalStorage

* Borrar tokens
 
* Caducidad de tokens
 
* Creación de usuarios
 
* Posteos
 
* Firebase REST API

También trabajaremos con un diseño elaborado elegante para nuestro login, y poder reconstruir un proyecto usando un repositorio externo.

[Volver al Índice](#%C3%ADndice-del-curso)

## 166. Demostración de la aplicación - Login tradicional

Ver video mostrando el proyecto terminado y sus funcionalidades.

[Volver al Índice](#%C3%ADndice-del-curso)

## 167. Inicio de proyecto - LoginApp

Descargamos el proyecto base del repositorio que nos proporciona el profesor y lo renombramos a '07-bonus-loginApp'

Tras eso ejecutaremos en el terminal, dentro del directorio 'npm install' para instalar los modulos de node definidos en el package.json

La estructura del proyecto no es nada que no hayamos visto hasta ahora, con tres páginas: home, login y registro, solo quedó pendiente añadir en el index.html el link a Font Awesome.

```
  <link rel="stylesheet" href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
```

Esto permitirá que se vea el icono "check" de los checkboxes

[Volver al Índice](#%C3%ADndice-del-curso)

## 168. Crear modelo para el manejo de los usuarios

En la página de crear cuenta necesitaremos capturar el correo, nombre y contraseña para enviarlo al servidor y realizar el registro del usuario.

Para ello vamos a crear un modelo, para ello crearemos el directorio y archivo src/app/models/usuario.model.ts que será una clase de typescript que nos permitirá acceder a las propiedades y métodos de un usuario:

```
export class UsuarioModel {
    email: string;
    password: string;
    nombre: string;
}
```

Simplemente tendría ese código, esto nos permitirá ahora crear instancias de ese objeto con los valores que se introduzcan, para poder enviar dicho objeto al servidor.

[Volver al Índice](#%C3%ADndice-del-curso)

## 169. Conectar el formulario de registro con una instancia del modelo de usuario

Primero tenemos que asignar los campos al formulario, para ello vamos al registro.component.ts y crearemos una nueva instancia del modelo del usuario, para ello importaremos el modelo, declararemos un objeto usuario de tipo UsuarioModel y en el inicio del componente (ngOnInit) crearemos una instancia de dicho objeto.

La gestión de las propiedades del objeto a través del html lo haremos con los formularios, si en el input de email colocamos un [(ngModel)]="usuario.email" y en el componente, en el ngOnInit le asociamos un valor por defecto al campo email `this.usuario.email = 'test@gmail.com';` debería aparecer en el campo del formulario, ahora mismo no aparecerá porque para poder usar la funcionalidad del ngModel en un formulario necesitamos importar en los módulos la librería de Angular para gestión de formularios.

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RegistroComponent } from './pages/registro/registro.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Ahora ya existe una relación, es decir, el ngModel es de "entrada/salida" podríamos decir (de ahí que esté entre corchetes y paréntesis a la vez), y dinámicamente tendrá el valor asociado a la propiedad del objeto (usuario.email), de tal manera que cuando se crea el componente ya aparece el valor que le dimos en él, y si en el input del formulario cambiamos el valor, ya podríamos asociarlo a la propiedad del objeto.

Haremos lo mismo para el nombre y el password.

Ahora si hacemos click en "Crear cuenta" no hará refresco de la página, puesto que al contener ngModel el formulario Angular ya sabe que esto debe ser gestionado y no hace nada (porque no hemos definido aún que haga nada) así que ahora recogeremos los datos del formulario.

Para ello en la etiqueta <form> del html añadiremos (ngSubmit) que nos permitirá ejecutar una función al pulsar el botón que ya tenemos declarado como type="submit", solo para las pruebas haremos que al pulsar el botón se haga un console.log con un mensaje y otro console.log con los valores del objeto usuario (que aparecerán si hemos rellenado los campos del formulario)

```
<form (ngSubmit)="onSubmit()" class="login100-form validate-form flex-sb flex-w">
```

```
onSubmit() {
    console.log('Formulario enviado');
    console.log(this.usuario);
  }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 170. Validar la información antes de enviarla a un servidor

Las validaciones se pueden hacer o bien por template (plantilla), es decir, en el mismo html, o en el componente, aquí usaremos el primer caso y el segundo lo haremos en la siguiente sección que es específica de formularios de Angular.

Por tanto, primero vamos a añadirle las propiedades "required" y "email" al input del email, con esto ya estamos indicando que ese campo es obligatorio y que tiene que ser de tipo email, aun así además el type lo definiremos como email en lugar de text.

Tenemos que hacerle saber a Angular de alguna manera, ya que hemos usado el modo de plantilla, de que tiene que tener en cuenta el html del formulario como tal, para ello vamos a declarar en el formulario una referencia local (#) que declararemos indicando que esto es un ngForm, los fragmentos del html a continuación:

```
<form #f="ngForm" (ngSubmit)="onSubmit()" class="login100-form validate-form flex-sb flex-w">
```

```
<input class="input100" type="email" name="email" [(ngModel)]="usuario.email" placeholder="Email" required email>
```

Ahora en nuestra función onSubmit podemos pasarle la referencia del formulario onSubmit( f )

Y en la función de nuestro componente podemos definir que recibirá un objeto form de tipo ngForm (la referencia local 'f' que habíamos declarado), deberemos, por tanto, importar NgForm de @angular/forms, cuyo módulo ya habíamos importado, a su vez, anteriormente en el app.module.ts

```
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioModel } from '../../models/usuario.model';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usuario: UsuarioModel;

  constructor() { }

  ngOnInit() {

    this.usuario = new UsuarioModel();

    this.usuario.email = 'test@gmail.com';
  }

  onSubmit( form: NgForm ) {
    console.log('Formulario enviado');
    console.log(this.usuario);
    console.log(form);
  }
}
```

Podemos ver el objeto form en consola, con muchísima información acerca del formulario. Actualmente si tratamos de enviar un formulario vacío debería dar error, puesto que el input email es required, si en la consola miramos la propiedad "controls" del objeto, veremos los inputs a los cuales les hemos definido la propiedad "name" en el html, en nuestro caso 'email', 'nombre' y 'pass'. Si vemos la propiedad 'valid' veremos que es 'false'.

Vamos a restringir un poco más el formulario, haciendo requeridos todos los campos, añadiendo 'required' a los dos inputs, y también definiremos un mínimo de caracteres para el nombre y para la contraseña.

También añadiremos al ngOnInit un validador de si el formulario es válido o no, lo cual mostrará la información por consola o no.

```
onSubmit( form: NgForm ) {

    if ( form.invalid ) { return; }
    console.log('Formulario enviado');
    console.log(this.usuario);
    console.log(form);
  }
```

El html del formulario quedaría hasta el momento así:

```
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-t-50 p-b-90">
            <form #f="ngForm" (ngSubmit)="onSubmit(f)" class="login100-form validate-form flex-sb flex-w">

                <span class="login100-form-title p-b-51">
                        Crear nueva cuenta
                    </span>

                <!-- <span class="text-danger">El correo es obligatorio</span> -->
                <div class="wrap-input100 m-b-16">
                    <input class="input100" type="email" name="email" [(ngModel)]="usuario.email" placeholder="Email" required email>

                    <span class="focus-input100"></span>
                </div>

                <!-- <span class="text-danger">El nombre es obligatorio</span> -->
                <div class="wrap-input100 m-b-16">
                    <input class="input100" type="text" name="nombre" [(ngModel)]="usuario.nombre" placeholder="Nombre y apellidos" required minlength="2">

                    <span class="focus-input100"></span>
                </div>

                <!-- <span class="text-danger">La contraseña debe de ser más de 6 letras</span> -->
                <div class="wrap-input100 m-b-16" data-validate="Password is required">
                    <input class="input100" type="password" name="pass" [(ngModel)]="usuario.password" placeholder="Password" required minlength="6">
                    <span class="focus-input100"></span>
                </div>

                <div class="flex-sb-m w-full p-t-3 p-b-24">
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                        <label class="label-checkbox100" for="ckb1">
                                Recordar mi usuario
                            </label>
                    </div>

                    <div>
                        <a routerLink="/registro" class="txt1">
                                ¿Ya tienes cuenta? / Ingresar
                            </a>
                    </div>
                </div>

                <div class="container-login100-form-btn m-t-17">
                    <button class="login100-form-btn" type="submit">
                            Crear cuenta
                        </button>
                </div>

            </form>
        </div>
    </div>
</div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 171. Mostrar errores en pantalla

Ya tenemos comentados en el html unos <span> que deberemos de renderizar cuando los inputs no cumplan las condiciones de validación. Vimos en la lección anterior que podemos acceder a las distintas propiedades del formulario, del cual tenemos una referencia local en el html (#f), por tanto si queremos saber si un campo está validado podemos hacerlo sencillamente con las propiedades del ngForm de angular, por ejemplo acceder a f.controls['email].errors, y si el formulario se ha tratado de enviar con errores que muestre el error, sería algo así:

```
<span *ngIf="f.submitted && f.controls['password'].errors" class="text-danger animated fadeIn">La contraseña debe de ser más de 6 letras</span>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 172. Tarea - Pantalla de login y sus validaciones

En el formulario de registro hay un enlace que lleva a la página de login y viceversa, vamos a hacerlo funcionar. Sólo habría que definir en el enlace la ruta correcta, que estaba puesta en la plantilla una por defecto `<a routerLink="/login" class="txt1">`, también puliremos algunos estilos añadiendo animaciones en registro y login html `<div class="limiter animated fadeInRight">` y cambiaremos el efecto de animación por uno más rápido en animate.css `animation-duration: 0.5s;`

La tarea consiste en dejar el formulario de login hasta el punto que hemos dejado el de registro, validar nombre y password, pero el nombre deberá ser un email, y el password de al menos 6 caracteres y que cuando esté validado muestre la información del formulario en consola, pero que no lo haga en caso contrario.

login.component.html

```
<div class="limiter animated fadeInLeft">
    <div class="container-login100">
        <div class="wrap-login100 p-t-50 p-b-90">
            <form (ngSubmit)="login(f)" #f="ngForm" class="login100-form validate-form flex-sb flex-w">

                <span class="login100-form-title p-b-51">
                    Login
                </span>

                <span *ngIf="f.submitted && f.controls['email'].errors" class="text-danger">El correo es obligatorio</span>
                <div class="wrap-input100 m-b-16">
                    <input [(ngModel)]="usuario.email" required email class="input100" type="email" name="email" placeholder="email">

                    <span class="focus-input100"></span>
                </div>

                <span *ngIf="f.submitted && f.controls['password'].errors" class="text-danger">La contraseña debe de ser más de 6 letras</span>
                <div class="wrap-input100 m-b-16" data-validate="Password is required">
                    <input [(ngModel)]="usuario.password" required minlength="6" class="input100" type="password" name="password" placeholder="Password">
                    <span class="focus-input100"></span>
                </div>

                <div class="flex-sb-m w-full p-t-3 p-b-24">
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                        <label class="label-checkbox100" for="ckb1">
                            Recordar mi usuario
                        </label>
                    </div>

                    <div>
                        <a routerLink="/registro" class="txt1">
                            ¿No tienes cuenta?
                        </a>
                    </div>
                </div>

                <div class="container-login100-form-btn m-t-17">
                    <button class="login100-form-btn" type="submit">
                        Ingresar
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>
```

login.component.ts

```
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioModel } from '../../models/usuario.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel;

  constructor() { }

  ngOnInit() { this.usuario = new UsuarioModel(); }

  login( form: NgForm ) {

    if ( form.invalid ) { return; }

    console.log('Login enviado');
    console.log(form);

  }

}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 173. Firebase y servicios REST

Como backend para la gestión de los registros de usuarios y validaciones vamos a usar Firebase, que es parecido a MongoDB.

Para ello nos creamos una cuenta y vamos a la consola de administración y creamos un nuevo proyecto 'login-app'.

Tras esto vamos a "Authentication" y "Sign-in method" y seleccionamos correo y contraseña como método de autenticación y activamos "Habilitar", esto nos permitirá hacer llamadas a una API y crear usuarios, en la pestaña Usuarios podremos ver los usuarios autenticados y conectados.

Por otro lado tenemos este enlace 'https://firebase.google.com/docs/reference/rest/auth#section-create-email-password' que nos da información de los servicios REST de que dispone Firebase, nosotros usaremos el de registro con usuario y contraseña, de hecho está diferenciado entre registro e inicio de sesión, para crear y autenticar usuarios, respectivamente. Empezamos con la creación de usuario.

Para ello primero necesitamos poder hacer peticiones HTTP a la api REST, para ello debemos disponer del módulo, lo importaremos en nuestro app.module.ts

```
import { HttpClientModule } from '@angular/common/http';
...
imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
```

Lo siguiente será crear un servicio para manejar todo lo referente a la autenticación: `ng g s services/auth`

Copiamos de la documentación referida anteriormente el endpoint para el Sign-up y el Sign-in en auth.service.ts:

```
// Crear nuevos usuarios
  // https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]
  // Login
  // https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]
```

E inyectaremos el cliente http en el constructor del servicio:

```
import { HttpClient } from '@angular/common/http';
...
  constructor( private http: HttpClient ) { }
```

Para la composición de los endpoints, como hicimos en lecciones anteriores, vamos a componer la url del endpoint diferenciando entre la parte común y la que difiere (Entre sign-up y sign-in), además definiremos otra variable (la anterior y esta privadas) que contenta la api_key de nuestra aplicación de firebase, la cual podemos ver en la configuración del proyecto, en General.

```
private url = 'https://identitytoolkit.googleapis.com/v1/accounts:';
private apikey = 'AIzaSyCdaX1vaVW_MzsmGAV******fOGWVoBwhVkw';
```

Por último dejaremos definidos los métodos del servicio que usaremos para la gestión de login, logout y registro de usuarios, necesitando tener importado el modelo de Usuario, puesto que será requerido como parámetro para usar los métodos.

```
import { UsuarioModel } from '../models/usuario.model';
...
logout() {

}
login( usuario: UsuarioModel ) {

}
nuevoUsuario( usuario: UsuarioModel ) {

}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 174. Registrar un nuevo usuario

En la documentación de Firebase podemos ver que necesitamos enviar cierta información si queremos registrar un nuevo usuario:

```
email	string	The email the user is signing in with.
password	string	The password for the account.
returnSecureToken	boolean	Whether or not to return an ID and refresh token. Should always be true.
```

Entonces en auth.service.ts en nuestro método nuevoUsuario() crearemos una constante con los valores que nos piden, definiendo returnSecureToken a true y pasando el email y password del objeto usuario:

```
const authData = {
      email: usuario.email,
      password: usuario.password,
      returnSecureToken: true
    };
```

Podríamos usar un método más abreviado que lo que haría sería pasar el objeto entero, definiendo como "email" el campo en "usuario.email" y así sucesivamente, quedando el código más limpio:

```
const authData = {
      ...usuario,
      returnSecureToken: true
    };
```

Lo siguiente sería hacer una petición POST a la api, necesitamos hacer un return para poder suscribirnos en otro lugar, la función post del objeto http client necesita, cómo mínimo, que le pasemos la url del endpoint (que hemos preparado previamente) y el body con los datos requeridos (que también hemos definido como una constante llamada authData):

```
nuevoUsuario( usuario: UsuarioModel ) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    };

    return this.http.post( `${this.url}signUp?key=${this.apikey}` , authData );

  }
```

Ahora para poder usarlo en nuestro componente de registro inyectamos en el constructor nuestro servicio auth servicio de manera privada:

```
import { AuthService } from 'src/app/services/auth.service';
...
constructor( private auth: AuthService ) { }
```

Ahora al servicio le podremos pasar como argumento el usuario que recibimos del formulario y suscribirnos a él, de momento sólo vamos a hacer un console log de la respuesta http para ver qué nos devuelve:

```
this.auth.nuevoUsuario( this.usuario )
    .subscribe( resp => {
      console.log(resp);
    });
```

Pudiera ser que diera un error si probamos a enviar el formulario en este punto, porque al haber hecho cambios en el servicio, y este estar definido con el decorador @Injectable providedIn: 'root' puede que no se noten los cambios hasta haber bajado y levantado la aplicación.

Podemos ahora ver la respuesta http con toda la información que necesitamos, idToken, tiempo de expiración, localId, etcétera.

Tenemos que capturar un error adicional, porque si tratamos de enviar por segunda vez el formulario, como es normal, da error porque ese usuario ya fue creado con ese email. Vamos a capturar los posibles errores en el subscribe de la petición POST http, de esta manera creamos un objeto 'err' con toda la información del error y podemos, por ejemplo, usar una propiedad de dicho objeto que contiene el mensaje de error para mostrarlo al usuario y sepa qué está sucediendo:

```
onSubmit( form: NgForm ) {

    if ( form.invalid ) { return; }

    this.auth.nuevoUsuario( this.usuario )
    .subscribe( resp => {
      console.log(resp);
    }, (err) => {
      console.log(err.error.error.message);
    });
  }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 175. Login de usuarios

Para esta parte tendremos, al menos, un usuario creado en Firebase.

Luego sería el mismo código, viendo la documentación, que el que usamos en el método nuevoUsuario() en nuestro servicio, cambiando la parte de la cadena de caracteres de la url que era diferente para login():

```
login( usuario: UsuarioModel ) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    };

    return this.http.post( `${this.url}signInWithPassword?key=${this.apikey}` , authData );

  }
```

En el lado del componente sería prácticamente igual que con el registro, solo cambiaría la llamada al método login(), pero el control de errores y todo sería igual, podemos ver en consola si hacemos un login satisfactorio que disponemos de un idToken que será lo que haya que destruir a la hora de hacer logout (cerrar la sesión), el componente de login quedaría así:

```
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioModel } from '../../models/usuario.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel;

  constructor( private auth: AuthService ) { }

  ngOnInit() { this.usuario = new UsuarioModel(); }

  login( form: NgForm ) {

    if ( form.invalid ) { return; }

    this.auth.login( this.usuario )
    .subscribe( resp => {
      console.log(resp);
    }, (err) => {
      console.log(err.error.error.message);
    });

  }

}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 176. Guardar Token en el LocalStorage

Si vamos a auth.service.ts y vemos las funciones de login y nuevoUsuario podemos ver, recordamos, que devuelven un observable, en ese observable podemos hacer algunas modificaciones antes de notificar a la página de registro o la página del login, es decir cuando se devuelva el observable podemos almacenar el token que viene de la petición, en el caso de que devolviese esa información.

Entonces vamos a crear dos métodos más, un método privado guardarToken que recibirá como parámetro el idToken del response, crearemos previamente la variable de almacenamiento de tipo string "userToken", inicializada a null no debe dar problema alguno, sencillamente nuestro método recibirá el idToken y lo almacenará en dicha variable, la cual usaremos para llamar al localstorage y usar su método setItem para pasarle la variable:

```
private guardarToken( idToken: string ) {

  this.userToken = idToken;
  localStorage.setItem('token', idToken);

}
```

Ahora necesitaremos otro método para leer el token del localstorage, no necesitaremos ningún argumento, pero si necesitaremos verificar si existe algún dato en el localstorage, en el caso de que esté usaremos el método getItem pasandole como argumento la referencia que en este caso la habíamos definido como 'token', si lo encuentra se almacenará en la variable de token que teníamos definida, si no existiera podríamos inicializar la variable a una cadena vacía, en cualquier caso acabará devolviendo el valor de la variable userToken:

```
private leerToken() {

  if ( localStorage.getItem('token') ) {
    this.userToken = localStorage.getItem('token');
  } else {
    this.userToken = '';
  }

  return this.userToken;

}
```

Para usar el método de guardarToken lo haremos en el mismo servicio, de esta manera cuando llamemos a los métodos para registrar un nuevo usuario o para hacer login pasaremos la petición http por un pipe con el operador map de rxjs, de esta manera filtraremos el response recibiendo sólo el idToken y, al mismo tiempo, llamando al método guardarToken para que lo almacene en el localstorage. Esto tiene una ventaja y es que si por algun motivo sucede un error en la petición, el map no se dispara. Adicionalmente para que el operador map no bloquee la respuesta tendremos que devolver la respuesta filtrada.Obviamente necesitaremos importar el map de la librería `import { map } from 'rxjs/operators';`.

```
nuevoUsuario( usuario: UsuarioModel ) {

  const authData = {
    ...usuario,
    returnSecureToken: true
  };

  return this.http.post( `${this.url}signUp?key=${this.apikey}` , authData )
  .pipe(
    map( resp => {
      this.guardarToken( resp['idToken']);
      return resp;
    }
    )
  );

}
```

Si tras hacer login o registro vamos a Application en el navegador veremos el token en el localstorage

Por otro lado podemos llamar al método leerToken cuando inicialicemos el servicio de autenticación:

```
constructor( private http: HttpClient ) {
  this.leerToken();
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 177. SweetAlert2 - Mostrar notificaciones al usuario

En el material adjunto tenemos información sobre SweetAlert2, que nos permite (puesto que tiene soporte para typescript) usar unos Alerts customizados, que usaremos para dar información al usuario si hay algún tipo de error en login y los registros, como ya tenemos la información (hasta ahora la mostrábamos por consola cogiendo los error message) usaremos dicha información para mostrarla en el alert.

Lo vamos a instalar con npm:

>npm install sweetalert2

Primero vamos a usarlo en el login.component.ts, para ello vamos a importar la librería instalada, al no tratarse de un módulo será de la siguiente manera:

`import Swal from 'sweetalert2';` 

Ahora podemos acceder al objeto Swal y sus métodos y funciones, en la documentación podremos ver todas las posibilidades, podemos empezar por en el login hacer un "loading", para eso usaremos el método 'fire' al que le pasaremos como argumento un objeto, este objeto tendrá las propiedades 'allowOutsideclick' que inicializaremos a 'false' haciendo que no se pueda hacer click fuera del alert para cerrar la ventana, 'icon' nos permitirá definir el icono, que será 'info' y el 'text' a mostrar.

```
Swal.fire({
  allowOutsideClick: false,
  icon: 'info',
  text: 'Espere por favor...'
});
```

Esto definiría la ventana del alert, pero si no queremos que aparezca el botón de OK y en su lugar aparezca un loading a continuación definiríamos `Swal.showLoading();`

En la lógica cuando nos suscribamos correctamente (hagamos login) debería de cerrarse esa ventana `Swal.close();`, y si hubiera algun error debería de mostrarse la ventana con el error, para ello crearemos otra ventana que contenga el mensaje de error que recibimos de la respuesta:

```
...
, (err) => {
      console.log(err.error.error.message);
      Swal.fire({
        icon: 'error',
        title: 'Error al autenticar',
        text: err.error.error.message
      });
    });
```

Haremos lo mismo para la sección de registro, quedarían registro.component.ts y login.component.ts, respectivamente, de esta manera:

```
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioModel } from '../../models/usuario.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usuario: UsuarioModel;

  constructor( private auth: AuthService ) { }

  ngOnInit() { this.usuario = new UsuarioModel(); }

  onSubmit( form: NgForm ) {

    if ( form.invalid ) { return; }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();

    this.auth.nuevoUsuario( this.usuario )
    .subscribe( resp => {
      console.log(resp);
      Swal.close();
    }, (err) => {
      console.log(err.error.error.message);
      Swal.fire({
        icon: 'error',
        title: 'Error al autenticar',
        text: err.error.error.message
      });
    });
  }

}

```

```
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioModel } from '../../models/usuario.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel;

  constructor( private auth: AuthService ) { }

  ngOnInit() { this.usuario = new UsuarioModel(); }

  login( form: NgForm ) {

    if ( form.invalid ) { return; }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();

    this.auth.login( this.usuario )
    .subscribe( resp => {
      console.log(resp);
      Swal.close();
    }, (err) => {
      console.log(err.error.error.message);
      Swal.fire({
        icon: 'error',
        title: 'Error al autenticar',
        text: err.error.error.message
      });
    });

  }

}

```

Por último vamos a dejar preparada la navegación para en las siguientes clases validar tokens, etc.

Para ello inyectamos en el constructor de registro.component.ts el Router:

```
import { Router } from '@angular/router';
...
constructor( private auth: AuthService, private router: Router ) { }
```

Lo siguiente sería usarlo cuando yo ya sé que tengo una autenticación válida, para que nos lleve a la home, navegando por url:

```
this.auth.nuevoUsuario( this.usuario )
    .subscribe( resp => {
      console.log(resp);
      Swal.close();
      this.router.navigateByUrl('/home');
```

Haremos lo mismo para el login.component.ts

[Volver al Índice](#%C3%ADndice-del-curso)

## 178. Recordar usuario

Empezaremos con el login, para ello vamos a crear una variable booleana llamada "recordarme" y en el html lo asociaremos con ngmodel `<input [(ngModel)]="recordarme" class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">`

Luego en el componente, cuando se verifica la autenticación (tras el Swal.close()), verificaremos si se marcó el checkbox, y en ese caso del objeto usuario (que se maneja con el formulario, recordemos) guardaremos la propiedad email en el local storage:

```
if ( this.recordarme ) {
  localStorage.setItem('email', this.usuario.email);
}
```

Para que esto se efectúe, en el ngOnInit del componente tendremos que hacer la misma consulta, para poder poner como valor del input el email guardado en el localstorage, además de dejar marcado el checkbox:

```
ngOnInit() { 
  if ( localStorage.getItem('email') ) {
    this.usuario.email = localStorage.getItem('email');
    this.recordarme = true;
  }
}
```

Necesitaremos hacer lo mismo en el registro.component.ts, pero la lógica de base es un poco distinta, porque en ese formulario marcar el checkbox lo que hará será, que cuando se registre, recuerde el email para ser usado en la pantalla de login.

En el formulario del registro asociaremos el ngModel a la variable, en el componente declararemos la variable y haremos la comprobación en la función de nuevoUsuario():

[Volver al Índice](#%C3%ADndice-del-curso)

## 179. Guard para proteger la ruta si no se está autenticado

Aquí usaremos un Guard para proteger la ruta home que solo será accesible al registrar o hacer login, actualmente escribiendo directamente localhost:4200/home es accesible. Angular CLI ya trae una instrucción para crear un Guard:

>ng g guard guards/auth

* CanActivate
* CanActivateChild
* CanLoad

Nos da a elegir entre varios métodos por defecto que se usarán para proteger rutas, rutas hijas y para lazy load, respectivamente. Nosotros con la primera tenemos suficiente de momento. Explicamos el auth.guard.ts:

```
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }

}
```

Es un servicio que implementa CanActivate de @angular/router, que es la instrucción que Angular debe ejecutar cuando se navegue a una ruta, verificando si se puede navegar a dicha ruta o no. 'next' contiene cual es la siguiente ruta a la cual el usuario quiere navegar y el 'state' es el estado actual de la ruta, vemos que puede retornar observables, promesas etc, en función de si esa navegación va a necesitar de algún proceso asíncrono en su ejecución,en nuestro caso solo necesitaremos que devuelva un booleano tal cual. Nosotros usaremos esto porque vamos a definir un método sencillo en nuestro auth.service.ts que verifique si existe un token de usuario, y que tenga una longitud mayor a 2, posteriormente afinaremos esto porque pudiera ser que exista un token pero que dicho token hubiera expirado, si la verificación funciona en tal caso devolverá 'true', usando esto como argumento de comprobación en el CanActivate.

Así que primero de todo en auth.service.ts crearemos dicho método:

estaAutenticado() : boolean {

  return this.userToken.length > 2;

}

Regresando a nuestro auth.guard.ts necesitamos inyectar el servicio en el constructor para poder acceder al método estaAutenticado(), entonces lo que devolverá canActivate será el estado de estaAutenticado():

```
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private auth: AuthService ) {}
  canActivate(): boolean {
    return this.auth.estaAutenticado();
  }

}
```

Una vez implementado ahora podemos en nuestro app-routing.module.ts definir que rutas necesitan esta autenticación, en nuestro caso sólo será el home, importaremos el guard y modificaremos el path a la home añadiendo una propiedad mas, canActivate que deberá ser ejecutada desde el AuthGuard, quedando el app-routing.module.ts así:

```
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: 'home'    , component: HomeComponent, canActivate: [ AuthGuard ]},
  { path: 'registro', component: RegistroComponent },
  { path: 'login'   , component: LoginComponent },
  { path: '**', redirectTo: 'registro' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

```

Recordamos que al haber modificado el servicio, y este estar como @Injectable providedIn: root, debemos bajar y levantar la aplicación de nuevo para que se apliquen los cambios.

Por último, hecho esto lo que sucede es que nos lleva a una página en blanco si no estamos autenticados, para evitar esto podemos hacer una redirección en el auth.guard.ts, así que modificaremos el método canActivate para hacer una validación primero de si esta autenticado, en tal caso devolverá 'true' como antes, pero si no lo que hará será navegar hasta el /login, y para ello implementaremos el router en el servicio:

```
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private auth: AuthService, private router: Router ) {}
  canActivate(): boolean {
    if ( this.auth.estaAutenticado() ) {
      return true;
    } else {
      this.router.navigateByUrl('/login');
    }

  }

}
```


[Volver al Índice](#%C3%ADndice-del-curso)

## 180. Logout - Cerrar sesión

En nuestra aplicación lo único que necesitamos para hacer logout es eliminar el token de usuario, para ello en la función logout() que tenemos por definir en auth.service.ts tendremos que destruir el item 'token':

```
logout() {
    localStorage.removeItem('token');
  }
```

Ahora en home.component.html haremos un html sencillo que incluya el botón que ejecute una función salir():

```
<div class="m-5">

    <h1>Mi aplicación secreta</h1>
    <hr>

    <button class="btn btn-outline-danger" (click)="salir()">Cerrar sesión</button>
</div>
```

En el componente de home necesitaremos crear dicha función salir que ejecutará el método logout del servicio, por tanto también necesitaremos inyectar en el constructor tanto el servicio como el router, para que nos lleve a la página de login de nuevo si cerramos la sesión:

```
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  salir() {
    this.auth.logout();
    this.router.navigateByUrl('/login');
  }

}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 181. Mejorar la validación del token

En el response cuando hacemos la validación del token de usuario recibimos una propiedad 'expiresIn', que nos devuelve el tiempo de duración del token, por defecto son 3600 segundos (una hora), con los métodos de la función Date() de Javascript podemos controlar la fecha exacta de expiración con respecto a la fecha actual usando dichos métodos.

Por tanto cuando recibamos el token podremos saber la fecha exacta (horas, minutos y segundos inclusive) en la que dejará de ser válido, esa fecha la almacenaremos y luego compararemos esa fecha con el momento actual, para saber si el token sigue siendo válido.

En auth.service.ts trabajaremos primero en el login(), que a su vez llama a la función guardarToken() donde guarda el token en el localStorage, aquí tendremos que hacer también la validación de la fecha, para ello crearemos una variable que almacenará la fecha en el momento en que se guarda el token en el localStorage, a esa fecha le tendremos que sumar 3600 segundos (Que es por defecto lo que define Firebase pero también, por si esto cambiase, podríamos directamente pasarle como parámetro el valor que esté en expiresIn del objeto que devuelve Firebase) y esto será lo que guardemos, pero tendremos que pasarlo a String porque en el localStorage los datos tienen que ser de este tipo, y la función getTime() devuelve un número.

```
private guardarToken( idToken: string ) {

  this.userToken = idToken;
  localStorage.setItem('token', idToken);

  let hoy = new Date();
  hoy.setSeconds( 3600 );

  localStorage.setItem('expira', hoy.getTime().toString());

}
```

Una vez ya tenemos la fecha de expiración en el localStorage tendremos que validarlo cada vez que lo leamos, para ello en la función estaAutenticado() cojeremos la fecha almacenada y la compararemos con la actual, y en función de eso devolveremos verdadero o falso para que lo use el Guard.

```
estaAutenticado(): boolean {

    if ( this.userToken.length < 2 ) {
      return false;
    }

    const expira = Number(localStorage.getItem('expira'));
    const expiraDate = new Date();
    expiraDate.setTime(expira);

    if ( expiraDate > new Date() ) {
      return true;
    } else {
      return false;
    }
  }
```

Por último hay que tener en cuenta que ciertamente se podría manipular el token desde el navegador, pero ***siempre tendremos que validar también desde el backend (servidor) ***

[Volver al Índice](#%C3%ADndice-del-curso)

# Sección 11: Formularios en Angular
## 183. Introducción a la sección

Veremos en esta sección formularios de dos tipos, por template y reactivos, aconsejando usar siempre reactivos, ver video explicativo.

[Volver al Índice](#%C3%ADndice-del-curso)

## 184. ¿Qué aprenderemos en esta sección?

¿Qué aprenderemos en esta sección?

A continuación vamos a aprender sobre los siguientes temas:

1. Diferentes aproximaciones que tiene angular para trabajar formularios.
2. Profundizaremos en el uso del ngModel.
3. Utilizar las validaciones pre fabricadas.
4. Crear validaciones personalizadas.
5. Crear validaciones asíncronas.
6. Realizar un submit utilizando el ngSubmit.
7. Postear información únicamente cuando el formulario es valido,
8. Crear formularios del lado del componente.
9. Cargar información por defecto a los formularios.
10. Subscribirnos a los cambios de los valores de los formularios.
11. Entre otras cosas bien interesantes.

Durante la sección, tendremos tareas y un examen teórico al final.

[Volver al Índice](#%C3%ADndice-del-curso)

## 185. Demostración de lo que lograremos al finalizar la sección

Ver vídeo.

[Volver al Índice](#%C3%ADndice-del-curso)

## 186. Inicio del proyecto de formularios - Material adjunto incluido

Crearemos el nuevo proyecto con el bien ya conocido:

>ng new formularios

Creando automáticamente el routing y con css como estilo del proyecto.

También descargaremos una plantilla html con un formulario tipo para incluirlo en el proyecto.

En nuestro index.html del proyecto añadiremos el CDN de bootstrap `<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">`

[Volver al Índice](#%C3%ADndice-del-curso)

## 187. Creación de componentes - Formularios y Rutas

Vamos a crear dos páginas, para los formularios por template y para los reactivos, respectivamente:

>ng g c pages/template --skipTests
>ng g c pages/reactive --skipTests

Usaremos el html del recurso adjunto a la sección como esqueleto para ambos html.

En el app.component.html crearemos como base un div container con el router-outlet

Y en el app-routing.module.ts crearemos las rutas a las dos páginas:

```
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveComponent } from './pages/reactive/reactive.component';
import { TemplateComponent } from './pages/template/template.component';

const routes: Routes = [
  { path: 'template', component: TemplateComponent },
  { path: 'reactivo', component: ReactiveComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'template'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

```

Nótese que si pulsamos el botón de submit (guardar) del formulario hace un refresh de la página, que es la acción por defecto que tienen los formularios, vamos a desactivar esto para tener nosotros el control de como se comporta el formulario, para ello importaremos y añadiremos en los imports el FormsModule de @angular/forms en el app.module.ts, también añadi el RouterModule porque VSCode me disparaba un error con el router-outlet (aunque no impedía el funcionamiento correcto de la app):

```
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplateComponent } from './pages/template/template.component';
import { ReactiveComponent } from './pages/reactive/reactive.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TemplateComponent,
    ReactiveComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

```
[Volver al Índice](#%C3%ADndice-del-curso)

## 188. Template: ngModel - ngSubmit - Referencias locales a los elementos HTML

Vamos a ir al template.component.html y en el input del Nombre si escribiéramos en la etiqueta *ngModel* nos daría un error, nos pide al menos que definamos el "name", de esta manera podremos hacer alusión a ese campo. Una vez definidos name y ngModel podemos inspeccionar el elemento y vemos que angular añadío varias clases, que podríamos usar para aplicar css: ng-untouched que nos indica que el campo no ha sido tocado, pero si lo tocamos y pasamos a otro campo pasará de ng-untouched a ng-touched, ng-pristine que nos indica que el valor inicial del campo no ha sido modificado, si se modifica se eliminará y pasará a tener ng-dirty, y ng-valid que nos indica que el campo está pasando en ese momento todas las validaciones.

En el tag de form (que tiene definida una propiedad autocomplete="off" para evitar el autocompletado) para poder tener la información del formulario tendremos que definir un 'evento' de Angular que es (ngSubmit) que lanzará una función que definiremos como guardar() cuando se ejecute el formulario (el button type="submit" de rigor)

En el mismo input podemos definir la propiedad 'required' que es propia de html y evita el envío del formulario si el campo no se completó, Angular se da cuenta de esto, y si no tenemos completado el campo ahora podemos ver que la clase ng-valid pasó a ser ng-invalid (si no hemos incluido datos en el campo). Nótese que aun así el submit se ejecuta, pese a ser inválido el campo, porque es posible que esto sea necesario por ejemplo para hacer algún tipo de validación o acción paralela.

Podemos añadir una propiedad más, a efectos de validación, minlength="5" para que requiera al menos 5 caracteres, en este punto podremos observar que ng-invalid se mantiene hasta que escribimos, al menos, cinco caracteres en el input, pasando entonces a ser ng-valid.

Realmente todo esto nos servirá principalmente para cambiar CSS dinámicamente, las validaciones las haremos de otra manera.

```
<form autocomplete="off" (ngSubmit)="guardar()">

    <div>

        <div class="form-group row">
            <label class="col-2 col-form-label">Nombre</label>
            <div class="col-8">

                <input class="form-control" required minlength="5" ngModel name="nombre" type="text" placeholder="Nombre">
                .....
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 189. Template: Obteniendo la información del formulario

En los formularios por template la mayoría de la configuración se hace del lado del html, mientras que en los reactivos se hace del lado del componente.

Si queremos la información del formulario necesitamos una referencia a este, para esto vamos a crear una referencia local en el tag del form, #forma="ngForm", de esta manera indicamos que este formulario pasa a ser el objeto 'forma' que será un ngForm, y de esta manera a la función guardar que teníamos le podemos pasar como argumento el objeto 'forma', (ngSubmit)="guardar( forma )". De tal manera que ahora en la declaración de la función guardar del componente podremos definir el argumento a recibir, de tipo NgForm, importando el tipo desde @angular/forms.

```
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  guardar( forma: NgForm ) {
    console.log( forma );
  }

}
```

Como hemos hecho un console log del objeto, podemos ver toda la información del formulario, entre las muchas propiedades vemos "valid" que nos servirá para saber si el formulario está siendo válido o no para su envío. Por ejemplo 'pending' es utilizado cuando hay validaciones asíncronas, será un estado que cambiará de true a false cuando se termine la función asíncrona, un caso típico sería consulta a base de datos de un servidor para comprobar existencia de usuario y tener control de esto mientras se realiza. Podemos observar también otra propiedad del objeto de formulario, controls, que nos da detalle de los inputs del formulario, ahora sólo aparecerá nombre puesto que para que se tengan en cuenta debemos definir en la etiqueta el ngModel y el name, dentro de cada control tenemos sus propiedades, si es válido, su parent o su value. Por tanto si llamamos a forma.value nos devolverá un objeto con los valores de los campos.

Por tanto podríamos definir que aparezcan valores por defecto en los campos del formulario en la primera carga del mismo, para ello podemos definir una variable que sea un objeto que tenga como propiedades los campos del formulario.

```
...
export class TemplateComponent implements OnInit {

  usuario = {
    nombre: 'Carlos'
  }
...
```

Si en el template el ngModel lo ponemos entre corchetes le indicamos que va a recibir un valor (y si lo ponemos entre paréntisis indicamos que lo envía, y si ponemos ambos pues ambas funciones podrá permitir), de esta manera podemos manejar la información del componente [ngModel]="usuario.nombre"

[Volver al Índice](#%C3%ADndice-del-curso)

## 190. Template: Validaciones independientes y cambio de estilo

Podría ser de muchas otras maneras, para este ejercicio vamos a usar clases de bootstrap para los estilos de validación, usaremos is-invalid, is-valid, y nosotros con Angular determinaremos cuando aplicar o no esas clases css de bootstrap.

Para ello haremos una referencia local al input definiéndola como un modelo de Angular, de esta manera podremos acceder a las propiedades de dicho campo (entre ellas valid o invalid...) `#nombre="ngModel"` entonces en el html podremos dinamicamente añadir una clase, esto se hace indicándolo con corchetes `[class.is-invalid]="nombre.invalid`, esto significa si la propiedad invalid de la referencia local nombre es verdadera (falsa sería !nombre.invalid) entonces añade la class css 'is-invalid' a la etiqueta. Pero de esta manera ya de base aparecería en rojo, con la clase aplicada, así que añadiremos la condición de que haya sido tocado el campo por el usuario `[class.is-invalid]="nombre.invalid && nombre.touched`.

Para el input del email tenemos en los recursos de la sección una expresión regular que añadiremos  

[Volver al Índice](#%C3%ADndice-del-curso)

## 191. Tarea: Repaso de las validaciones

Completar las validaciones pendientes de los tres inputs.

[Volver al Índice](#%C3%ADndice-del-curso)

## 192. Resolución de la tarea - Validaciones

```
<div>

        <div class="form-group row">
            <label class="col-2 col-form-label">Nombre</label>
            <div class="col-8">

                <input class="form-control" #nombre="ngModel" [class.is-invalid]="nombre.invalid && nombre.touched" required minlength="5" [ngModel]="usuario.nombre" name="nombre" type="text" placeholder="Nombre">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-2 col-form-label">Apellido</label>
            <div class="col-8">

                <input class="form-control" #apellidos="ngModel" [class.is-invalid]="apellidos.invalid && apellidos.touched" required minlength="5" [ngModel]="usuario.apellidos" name="apellidos" type="text" placeholder="Apellido">
            </div>
        </div>

    </div>

    <div class="form-group row">
        <label class="col-2 col-form-label">Correo</label>
        <div class="col-8">

            <input #correo="ngModel" [ngModel]="usuario.correo" [class.is-invalid]="correo.invalid && correo.touched" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" type="email" name="correo" placeholder="Correo electrónico">
        </div>
    </div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 193. Template: Mostrar errores y mensajes

Sencillamente usaremos *ngIf para generar un label cuando no se cumplan las condiciones:

`<small class="form-text text-danger" *ngIf="correo.invalid && correo.touched">Correo obligatorio. Ingrese un formato correcto de correo</small>`

Por afinar un poco, si se toca el boton de guardar sin haber rellenado ningún campo debería marcar los avisos, aplicar los estilos, pero no lo hace, hay muchas maneras de poder hacer esto, en este ejercicio lo que haremos será en el componente, en la función guardar, comprobar si el formulario es inválido, y en tal caso hacer un return para que no ejecute el formulario y modificar la propiedad de los campos a "touched" para que así se active los condicionales del html:

```
guardar( forma: NgForm ) {

  console.log( forma );

  if ( forma.invalid ) {
    Object.values( forma.controls ).forEach( control => {

      control.markAsTouched();
    });
    return;
  }
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 194. API de países del mundo

API de países del mundo

Nota:

En la siguiente clase vamos a trabajar creando un select, pero con información de países del mundo, por lo que únicamente les pido que abran este URL que usaremos en la próxima clase:

https://restcountries.eu/

Y específicamente usaremos este endpoint:

https://restcountries.eu/rest/v2/lang/es

Nota2:

Se proporciona una extensión de Chrome para ver archivos JSON pero ya no existe o estará desactualizada, se recomienda buscar una alternativa pero no es fundamental.

[Volver al Índice](#%C3%ADndice-del-curso)

## 195. Servicio Pais - Obtener información de países

Usaremos la api REST proporcionada para crearnos un servicio con los datos que posteriormente usaremos para generar un select de países en el formulario.

`http://restcountries.eu/rest/v2/lang/es`

Por tanto vamos a crear el servicio para tener toda la información centralizada:

>ng g s services/pais --skipTests

Recordamos que necesitamos importar el módulo para hacer peticiones http en el app.module.ts:

`
import { HttpClientModule } from '@angular/common/http';
...
imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpClientModule
  ],
`

A continuación importamos HttpClient en pais.service.ts e inyectamos una instancia del mismo en el constructor, y crearemos una función que haga la petición get al endpoint:

`
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PaisService {

  constructor( private http: HttpClient ) { }

  getPaises() {

    return this.http.get('http://restcountries.eu/rest/v2/lang/es');
    
  }
}
`

Regresamos a template.component.ts , importando el servicio personalizado e inyectando una instancia en el constructor:

`
import { PaisService } from 'src/app/services/pais.service';
...
constructor( private paisService: PaisService ) { }
`

La petición http la haremos en el ngOnInit del template.component.ts para asegurarnos de tener los datos cuando se genere el componente.

Como lo que devuelve es un observable tendremos, además, que suscribirnos a él:

`
ngOnInit(): void {

    this.paisService.getPaises()
    .subscribe( paises => {
      console.log(paises);
    })
  }
`

El observable nos devuelve mucha información y nosotros sólo vamos a necesitar un par de propiedades del objeto que devuelve, como el servicio está centralizado lo ideal es filtrar la petición con un pipe map en el servicio mismo:

`
getPaises() {

    return this.http.get('http://restcountries.eu/rest/v2/lang/es')
    .pipe(
      map( (resp: any[] ) => {
        return resp.map( pais => {
          return {
            nombre: pais.name,
            codigo: pais.alpha3Code
          };
        });
      })
    );
  }
`

Notese que al aplicar el map de react (rxjs) le indicamos que la respuesta la devolveremos como un array, por tanto después usamos otro metodo map, pero este es el propio de los tipo array que a su vez devuelve un array sociativo que creamos con los atributos nombre y código teniendo como valores las propiedades que nos interesan del objeto de pais.

Una versión más simplificada sería:

``
getPaises() {

    return this.http.get('http://restcountries.eu/rest/v2/lang/es')
    .pipe(
      map( (resp: any[] ) => resp.map( pais =>  ({ nombre: pais.name, codigo: pais.alpha3Code }) ) )
    );
  }
`

[Volver al Índice](#%C3%ADndice-del-curso)
## 196. Template: Select y sus validaciones

Crearemos un select de html en nuestra plantilla, añadiéndole las propiedades name="pais", required y ngModel, por otro lado en el TS crearemos una variable paises que contendrá el array de paises que recibiremos del subscribe al observador, para poder renderizar los datos posteriormente en el html mediante un ngFor del array de paises. Podemos además añadir una validación de que cuando no estén aun cargados los datos del servicio el select quede bloqueado, usando [disabled]. También añadiremos en nuestro TS un valor por defecto añadido al principio del array, usando 'unshift', con un valor nulo.

Añadiendo el atributo 'pais' al objeto usuario e igualando [ngModel]="usuario.pais" en el html ahora podremos por ejemplo poner valores por defecto en el componente, indicando el código del país (recordemos que el value del campo es el código, no el nombre). Resumiendo el código html y ts quedan así:

`
 <select #pais="ngModel" class="form-control" [class.is-invalid]="pais.invalid && pais.touched" name="pais" id="pais" required [ngModel]="usuario.pais" [disabled]="paises.length === 0">
                <option *ngFor="let pais of paises" [value]="pais.codigo">
                    {{ pais.nombre }}
                </option>
            </select>
`

`
export class TemplateComponent implements OnInit {

  usuario = {
    nombre: 'Eduardo',
    apellidos:'Córdoba',
    correo:'correo@correo.com',
    pais:''
  }

  paises: any[] = [];

  constructor( private paisService: PaisService ) { }

  ngOnInit(): void {

    this.paisService.getPaises()
    .subscribe( paises => {
      this.paises = paises;
      this.paises.unshift({
        nombre: '[Seleccione un País]',
        codigo: ''
      });
      console.log(paises);
    });
  }
`



[Volver al Índice](#%C3%ADndice-del-curso)

## 197. Template: Uso de radio buttons

Con esta lección queremos demostrar que la aproximación por template en formularios html usando Angular no es efectiva, se genera mucho código y habría que hacer mucho trabajo adicional a la hora de manejar observables, etc. El código respecto a los radio buttons es lo mismo que las lecciones anteriores, así que en esta lección me limitaré a poner todo el html y ts resultantes de template.component para que se vea el resultado y se pueda comparar con las siguientes lecciones, formularios reactivos (que es lo que se recomienda usar, ya que estamos trabajando con Angular):

template.component.html:

```
<h4>Formularios <small> Template </small></h4>
<hr>
<form autocomplete="off" (ngSubmit)="guardar( forma )" #forma="ngForm">

    <div>

        <div class="form-group row">
            <label class="col-2 col-form-label">Nombre</label>
            <div class="col-8">

                <input class="form-control" #nombre="ngModel" [class.is-invalid]="nombre.invalid && nombre.touched" required minlength="5" [ngModel]="usuario.nombre" name="nombre" type="text" placeholder="Nombre">
                <small class="form-text text-danger" *ngIf="nombre.invalid && nombre.touched">Nombre obligatorio. Ingrese 5 letras</small>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-2 col-form-label">Apellido</label>
            <div class="col-8">

                <input class="form-control" #apellidos="ngModel" [class.is-invalid]="apellidos.invalid && apellidos.touched" required minlength="5" [ngModel]="usuario.apellidos" name="apellidos" type="text" placeholder="Apellido">
                <small class="form-text text-danger" *ngIf="apellidos.invalid && apellidos.touched">Apellido obligatorio. Ingrese 5 letras</small>
            </div>
        </div>

    </div>



    <div class="form-group row">
        <label class="col-2 col-form-label">Correo</label>
        <div class="col-8">

            <input #correo="ngModel" [ngModel]="usuario.correo" [class.is-invalid]="correo.invalid && correo.touched" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" type="email" name="correo" placeholder="Correo electrónico">
            <small class="form-text text-danger" *ngIf="correo.invalid && correo.touched">Correo obligatorio. Ingrese un formato correcto de correo</small>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-2 col-form-label">País</label>
        <div class="col-8">

            <select #pais="ngModel" class="form-control" [class.is-invalid]="pais.invalid && pais.touched" name="pais" id="pais" required [ngModel]="usuario.pais" [disabled]="paises.length === 0">
                <option *ngFor="let pais of paises" [value]="pais.codigo">
                    {{ pais.nombre }}
                </option>
            </select>
            <small class="form-text text-danger" *ngIf="pais.invalid && pais.touched">Seleccione un país</small>

        </div>
    </div>

    <div class="form-group row">
        <label class="col-2 col-form-label">Género</label>
        <div class="col-8">
            <div class="form-check form-check-inline">
                <input #genero="ngModel" type="radio" [class.is-invalid]="genero.invalid && genero.touched" class="form-check-input" name="genero" value="M" required ngModel>
                <label class="form-check-label">Masculino</label>
            </div>
            <div class="form-check form-check-inline">
                <input #genero="ngModel" type="radio" [class.is-invalid]="genero.invalid && genero.touched" class="form-check-input" name="genero" value="F" required ngModel>
                <label class="form-check-label">Femenino</label>
            </div>
            <small class="form-text text-danger" *ngIf="genero.invalid && genero.touched">Seleccione un género</small>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-2 col-form-label">&nbsp;</label>
        <div class="input-group col-md-8">
            <button type="submit" class="btn btn-outline-primary btn-block">
        Guardar
      </button>
        </div>
    </div>

</form>
```

template.component.ts

```
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PaisService } from 'src/app/services/pais.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  usuario = {
    nombre: 'Eduardo',
    apellidos:'Córdoba',
    correo:'correo@correo.com',
    pais:'CRI'
  }

  paises: any[] = [];

  constructor( private paisService: PaisService ) { }

  ngOnInit(): void {

    this.paisService.getPaises()
    .subscribe( paises => {
      this.paises = paises;
      this.paises.unshift({
        nombre: '[Seleccione un País]',
        codigo: ''
      });
      // console.log(paises);
    });
  }

  guardar( forma: NgForm ) {

    console.log( forma );

    if ( forma.invalid ) {
      Object.values( forma.controls ).forEach( control => {

        control.markAsTouched();
      });
      return;
    }
  }

}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 198. Reactivo: Aproximación de formularios utilizando código

Ajustaremos el proyecto, en app-routing.module.ts cambiaremos la página por defecto de 'template' a 'reactivo'.

Y tenemos que importar el modulo de formularios reactivos en app.module.ts (al igual que anteriormente importamos el de formularios por template 'FormsModule'):

```
...
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
...
 imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 199. Reactivo: Creación del formulario - FormGroup

En lugar de referencia local en el html como hacíamos con los template, ahora lo haremos en el componente como una propiedad de la clase. 

Por tanto importaremos FormGroup de @angular/forms y crearemos una instancia 'forma' de ese tipo, la cual inyectaremos además en el constructor porque necesitamos tenerla lista antes de que se renderice el html.

También hay que considerar que cuando prevemos generar más de dos líneas de código en el constructor se recomienda generar métodos independientes fuera de él y desde el constructor los llamaremos.

Usaremos FormBuilder que es un servicio que nos permite simplificar el manejo de formularios en Angular, también lo importaremos de @Angular/forms y lo inyectaremos en el constructor.

Con esto podemos crear un metodo crearFormulario() que llamaremos desde el constructor, en él crearemos la variable de tipo FormGroup (forma), que será el resultado de llamar al método 'group' del servicio FormBuilder, en él definiremos un objeto que contendrá el nombre de los inputs y como valor un array que contendrá el valor por defecto, y más adelante además tendrá validaciones síncronas y asíncronas.

Para hacer referencia a esto en el html en el formulario indicaremos que el formGroup va a ser 'forma' y también dejaremos listo el ngSubmit, que lanzará una función guardar() que de momento sólo nos hará console.log del formulario y sus valores. Para poder hacer referencia a dichos valores tendremos que indicar en cada uno de los inputs una propiedad formControlName.

El código pues hasta ahora sería:

```
...
<form autocomplete="off" [formGroup]="forma" (ngSubmit)="guardar()">
...
<input class="form-control" type="text" placeholder="Nombre" formControlName="nombre">
...
```

```
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  forma: FormGroup;

  constructor( private fb: FormBuilder ) {
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  crearFormulario() {

    this.forma = this.fb.group({
      nombre: ['Eduardo'],
      apellidos: ['Córdoba'],
      correo: ['correo@correo.com'],
    });

  }

  guardar() {
    console.log(this.forma);
  }

}

```

[Volver al Índice](#%C3%ADndice-del-curso)

## 200. Reactivo: Validaciones síncronas

Las validaciones síncronas son aquellas que se pueden hacer inmediatamente y que no requieren interacción con servicios web externos.

Como indicamos en la sección anterior, las validaciones síncronas se pasan como segundo parámetro de valor del campo de formulario, el primero era el "name", Angular ya trae los métodos de validación, constando de la propiedad Validators seguida del método de validación a usar, a continuación vemos el ejemplo de validaciones de requerido, longitud mínima y el patrón para el correo:

```
crearFormulario() {

  this.forma = this.fb.group({
    nombre: ['', [Validators.required, Validators.minLength(5)]],
    apellidos: ['', [Validators.required, Validators.minLength(5)]],
    correo: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
  });

}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 201. Reactivo: Validaciones y HTML

Vamos a añadir lo que teníamos en las lecciones de aproximación por template en cuanto a cambios de estilo y control de validaciones.

Recordamos que aplicábamos dinámicamente la clase is-invalid en función de si era válido o no el campo, nosotros como queremos trabajar del lado del componente lo que podemos hacer es usar un getter (captador) que devolverá verdadero o falso, y cuya funcionalidad será acceder a las propiedades del objeto forma (recordemos que los atributos del objeto del formulario valid, invalid, etc, tenían valor booleano) y verificar si son verdaderas o falsas, tras eso podremos aplicar la clase dinámicamente teniendo como condición dicha función que devuelve verdadero o falso:

```
get nombreNoValido() {
  return this.forma.get('nombre').invalid && this.forma.get('nombre').touched;
}
```
Nótese que 'nombre' es el valor que aplicamos al input mediante formControlName

Entonces desde el lado del html llamaremos a la función así:

```
<input class="form-control" type="text" placeholder="Nombre" formControlName="nombre" [class.is-invalid]="nombreNoValido">
```

De la misma manera controlaremos también que se muestre un mensaje de error cuando la validación sea errónea:

```
<small *ngIf="nombreNoValido" class="text-danger">Ingrese 5 caracteres</small>
```

Para terminar la lección dejaremos también hecha la validación al pulsar el botón de guardar, como lo teníamos en la aproximación por template. Con la diferencia de que aqui 'forma' es un objeto de tipo "FormGroup" y en template era una referencia local y, por tanto, se lo teniamos que pasar como argumento a la función guardar, aqui tendremos que usar this.forma para hacer alusión al objeto del ts:

```
guardar() {
  console.log(this.forma);
  if ( this.forma.invalid ) {
    return Object.values( this.forma.controls ).forEach( control => {

      control.markAsTouched();
    });
  }
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 202. Reactivo: Agrupaciones de los objetos - formGroupName

Aquí veremos control de inputs agrupados, por ejemplo una dirección que es un sólo concepto pero aglutina varios inputs.

En nuestro componente habíamos definido el formulario con formbuilder y el metodo group, pues bien, para eso tendremos que definir un campo que será otro group de formbuilder, y en el tendrá los distintos campos que queremos agrupar:

```
crearFormulario() {

    this.forma = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(5)]],
      apellidos: ['', [Validators.required, Validators.minLength(5)]],
      correo: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      direccion: this.fb.group({
        distrito: ['', Validators.required ],
        ciudad: ['', Validators.required ],
      })
    });

  }
```

Para referenciarlo en el html definiremos en el divider padre formGroupName en dirección para indicar que es un grupo de inputs, y luego como en los ejemplo anteriores podremos usar formControlName en cada uno de los elementos hijos.

```
<div class="form-group row" formGroupName="direccion">
    <label class="col-2 col-form-label">Dirección</label>
    <div class="form-row col">
        <div class="col">
            <input type="text" class="form-control" placeholder="Distrito" formControlName="distrito">
        </div>
        <div class="col">
            <input type="text" class="form-control" placeholder="Ciudad" formControlName="ciudad">
        </div>
    </div>
</div>
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 203. Reactivo: Validación visual de los campos anidados

Primero haremos los getter de propiedades de los inputs de distrito y ciudad, es un poco distinto al resto, porque al ser un input anidado debemos hacer referencia al objeto padre/hijo (direccion.distrito por ej):

```
get distritoNoValido() {
  return this.forma.get('direccion.distrito').invalid && this.forma.get('direccion.distrito').touched;
}
get ciudadNoValido() {
  return this.forma.get('direccion.ciudad').invalid && this.forma.get('direccion.ciudad').touched;
}
```

Ahora podremos usar los getters para los condicionales de la clase dinámica:

```
<input type="text" class="form-control" placeholder="Distrito" formControlName="distrito" [class.is-invalid]="distritoNoValido">
```

Lo siguiente sería controlar aplicar la clase invalid si pulsamos en guardar y no estan válidos estos dos campos, actualmente esto no sucede porque en la función guardar está definido que se recorran los controles del formulario, pero estos dos nuevos son "sub-controles" dentro del mismo, así que tendremos que recorrerlos también, como el campo a verificar que contiene los dos inputs es un formgroup, diremos:

```
guardar() {
    console.log(this.forma);
    if ( this.forma.invalid ) {
      return Object.values( this.forma.controls ).forEach( control => {

        if ( control instanceof FormGroup ) {
          Object.values( control.controls ).forEach ( control =>
            control.markAsTouched()
          );
        } else {
          control.markAsTouched();
        }
      });
    }
  }
```

En la condicional preguntamos si el control es una instancia de formgroup volveremos a recorrer todos los campos de ese formgroup

[Volver al Índice](#%C3%ADndice-del-curso)

## 204. Reactivo: Reset y carga de la data inicial

Supongamos que nuestro formulario reciba datos desde un servicio y que usemos dichos datos para rellenar los campos del formulario con valores por defecto.

Para esto vamos a crear una nueva función al que llamaremos desde el constructor *después de llamar al método que carga el formulario*, cargarDataAlFormulario().

En la función lo que haremos será llamar al método setValue para pasarle el objeto con los valores:

```
cargarDataAlFormulario() {
    this.forma.setValue({
      nombre: 'Fernando',
      apellidos: 'Perez',
      correo: 'asdf@adfg.com',
      direccion: {
        distrito: 'barcelona',
        ciudad: 'barcelona'
      }

    });
  }
```

Para resetear el formulario y dejar todos los campos vacíos sería tan sencillo como al final de la función guardar() llamar al método reset de forma:

```
this.forma.reset();
```

Pero quizás quisiéramos conservar algunos valores del formulario, como checkboxes o similar, entonces deberemos pasarle como argumento un objeto con el valor o valores a conservar:

```
this.forma.reset({
      nombre: 'Sin nombre'
    });
```

De hecho en el ejemplo donde usamos setValue debemos especificar todos los campos a los cuales queremos darle valores, si alguno no recibiera el valor daría error, así que podríamos usar el metodo reset en lugar de setValue para definir valores, de esta manera si faltase alguno no daría error y lo pondría a null:

```
cargarDataAlFormulario() {
    this.forma.reset({
      nombre: 'Fernando',
      apellidos: 'Perez',
      correo: 'asdf@adfg.com',
      direccion: {
        distrito: 'barcelona',
        ciudad: 'barcelona'
      }

    });
  }
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 205. Reactivo: Arreglos de FormControl - FormArray

Para definir un campo como un array de datos usaremos el metodo array de formbuilder, al cual le pasaremos tantos controles como queramos que tenga el array:

```
pasatiempos: this.fb.array([
      [],[],[],[],[]
    ])
```

En el html generaremos una tabla para renderizar los datos, necesitaremos que un elemento padre (por ejemplo el tbody) tenga la propiedad formArrayName="pasatiempos", para así indicar que los campos que replicaremos posteriormente serán los valores del array (elementos hijos), para ello definiremos en el componente un getter que nos permita recibir los datos del array, indicandole que sera de tipo FormArray y en el html llamaremos a la función getter para recorrer los datos con un bucle for.

```
get pasatiempos() {
  return this.forma.get('pasatiempos') as FormArray;
}
```

```
<div class="row">
        <div class="col">
            <table class="table">
                <thead class="table-dark">
                    <tr>
                        <th>ID</th>
                        <th>Pasatiempo</th>
                        <th>Borrar</th>
                    </tr>
                </thead>
                <tbody formArrayName="pasatiempos">
                    <tr *ngFor="let control of pasatiempos.controls; let i = index;">
                        <td>{{ i + 1 }}</td>
                        <td>
                            <input class="form-control" type="text" [formControlName]="i">
                        </td>
                        <td><button class="btn btn-danger" type="button">Borrar</button></td>
                    </tr>
                </tbody>
            </table>
            <button type="button" class="btn btn-primary mt-3 mb-5 btn-block">+Agregar</button>
        </div>
    </div>
```

Al darle un nombre con formControlName en el input, y definirlo entre corchetes, tendremos la manera de que pueda recibir datos y quedará identificado.

Añadiremos también botones que usaremos en la siguiente lección para añadir y borrar campos, importante definirlos como type="button" para que no dispare el submit (por defecto se les entiende como submit)

[Volver al Índice](#%C3%ADndice-del-curso)

## 206. Reactivo: Añadir y borrar FormControls de forma dinámica

Para añadir form controls de forma dinámica vamos a definir una función en el componente que, haciendo uso del getter, haga push dentro del array recibido con el getter (pasatiempos) con un valor que nosotros definamos:

```
agregarPasatiempo() {
    this.pasatiempos.push( this.fb.control('Nuevo elemento', Validators.required) );
  }
```

Y en el html simplemente lo llamaremos con el evento click:

```
<button type="button" class="btn btn-primary mt-3 mb-5 btn-block" (click)="agregarPasatiempo()">+Agregar</button>
```

Para eliminar será similar solo que en lugar del método "push" usaremos el removeAt que nos permite eliminar una posición del array dado su índice, el cual pasaremos a través de la llamada a la función en el html:

```
borrarPasatiempo( i: number ) {
  this.pasatiempos.removeAt(i);
}
```

```
<td><button class="btn btn-danger" type="button" (click)="borrarPasatiempo(i)">Borrar</button></td>
```

Si quisieramos cargar datos en los pasatiempos en el momento en el que llamamos a la función cargarDataAlFormulario que tenemos definida en el constructor, podríamos hacerlo creando un array de datos y luego aplicando un foreach en el array, que vaya haciendo push en el array por cada posición, llamando al form builder, creando el control y dándole dicho valor en esa posición:

```
cargarDataAlFormulario() {
  this.forma.reset({
    nombre: 'Fernando',
    apellidos: 'Perez',
    correo: 'asdf@adfg.com',
    direccion: {
      distrito: 'barcelona',
      ciudad: 'barcelona'
    }
  });
  ['comer','dormir'].forEach( valor => this.pasatiempos.push( this.fb.control(valor)));
}
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 207. Reactivo: Validaciones personalizadas

Si queremos utilizar validaciones personalizadas lo ideal es tenerlas centralizadas, de esta manera en un futuro podríamos reutilizarlas en otros componentes, podría ser un clase, podría ser un archivo de typescript, pero nosotros vamos a crear un servicio para ello:

```
ng g s services/validadores --skipTests
```

Vamos al archivo del servicio creado validadores.service.ts, en él lo que vamos a crear es una colección de validadores, los validadores son funciones que devuelven un objeto.

Vamos a crear un método que usaremos para validar apellidos, para que no se pueda introducir ningún apellido que no sea el que especifiquemos nosotros.

Como argumento recibiremos el control, que será de tipo form control (nos aseguraremos de que lo importe de Angular/Forms), vamos a especificar el tipo de salida como un objeto, este objeto tendrá como atributo un string (s) y devolverá un booleano.

Una de las ventajas de crear un servicio para validadores es que podríamos, por ejemplo, inyectar en el constructor otros servicios, pudiendo por ejemplo hacer validaciones asíncronas, consumir una base de datos, etc.

```
import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  constructor() { }

  noHerrera( control: FormControl ): { [s:string]: boolean } {

    if ( control.value.ToLowerCase() === 'herrera') {
      return {
        noHerrera: true
      }
    }

    return null;
  }
}
```

En el código vemos que no dejará que se introduzca ninguna cadena de caracteres que sea 'herrera'. Guardamos y vamos al reactive.component.ts para inyectarlo:

```
import { ValidadoresService } from 'src/app/services/validadores.service';
constructor( private fb: FormBuilder, private validadores: ValidadoresService ) {
```

Para usarlo lo llamaremos cuando creamos el formulario y le aplicamos las validaciones:

```
apellidos: ['', [Validators.required, Validators.minLength(5), this.validadores.noHerrera ]],
```

Nótese que no ejecutamos la función tipo noHerrera(), sino simplemente le pasamos la referencia.

Si pulsamos guardar habiendo escrito en el input de apellido una cadena que incluya un número nos dará un error, pues tratará de hacer la conversión con toLowerCase, para evitar esto le pondremos un condicional al value en el valor que recibimos en el validador del servicio:

```
if ( control.value?.toLowerCase() === 'herrera') {
```
[Volver al Índice](#%C3%ADndice-del-curso)

## 208. Reactivo: Validar que el password2 sea igual al password1

Vamos a crear un par de inputs nuevos para practicar otra validación, las añadiremos debajo del correo, en reactive.component.html

```
 <div class="form-group row">
        <label class="col-2 col-form-label">Contraseña</label>
        <div class="col">

            <input class="form-control" type="text" placeholder="Contraseña" formControlName="pass1">

        </div>
    </div>

    <div class="form-group row">
        <label class="col-2 col-form-label">Repita Contraseña</label>
        <div class="col">

            <input class="form-control" type="text" placeholder="Repita Contraseña" formControlName="pass2">

        </div>
    </div>
```

Añádiremos los nuevos campos a nuestra creación del formulario (crearFormulario()):

```
  crearFormulario() {

    this.forma = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(5)]],
      apellidos: ['', [Validators.required, Validators.minLength(5), this.validadores.noHerrera ]],
      correo: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      pass1: ['', Validators.required ],
      pass2: ['', Validators.required ],
      ...
```

A continuación definiremos getters para manejar la información que recibimos en el input, al menos sabremos si este campo tiene información. Tras esto podremos usarlo para añadir la clase 'invalid' dinámicamente:

```
get pass1NoValido() {
    return this.forma.get('pass1').invalid && this.forma.get('pass1').touched;
  }
...................
<input class="form-control" type="text" placeholder="Contraseña" formControlName="pass1" [class.is-invalid]="pass1NoValido">
```

Para el segundo campo, crearemos un getters que lo que hará será recibir los valores de pass1 y pass2, y con un operador ternario devolveremos true si sus valores son iguales y en caso contrario devolveremos false:

```
get pass2NoValido() {
    const pass1 = this.forma.get('pass1').value;
    const pass2 = this.forma.get('pass2').value;

    return ( pass1 === pass2 ) ? false : true;

  }
```

Como en el anterior caso, llamaremos a la función para añadir dinámicamente la clase CSS invalid:

```
<input class="form-control" type="text" placeholder="Repita Contraseña" formControlName="pass2" [class.is-invalid]="pass2NoValido">
```

Para que no se vean los caracteres de la contraseña tendremos que definir el type del input como "password"

Ahora tendremos que definir estos casos como parte de la validación del formulario, puesto que hasta ahora sólo es visual.

A priori cabría pensar que la validación la podríamos definir cuando se crea el form builder, pero pudiera ser que la comparación entre campos se intentase hacer contra campos que aun no han sido creados, o que se encuentran en otro lugar, etc, entonces tendremos que hacer la validación a nivel de formulario.

Así que a continuación del objeto que tenemos con la definición de los campos (y algunas de sus validaciones 'menores') añadiremos una coma y otro objeto en el cual añadiremos todos los validadores que queramos a nivel de formulario, en nuestro caso sólo incluiremos uno.

Dentro de ese objeto tenemos varias propiedades disponibles interesantes, una de ellas es 'asyncValidators' (que veremos en la siguiente lección) que nos permite hacer validaciones asíncronas, tambien tenemos 'validators' que nos permitirá las validaciones síncronas. Podemos incluir, como hemos dicho, tantos como queramos, definiéndolos entre corchetes y separándolos con comas (si vamos a definir solamente uno esto no es necesario).

Vamos a definir la validación como una llamada a una función passwordsIguales, a la cual le pasaremos como argumentos los dos form controls del form group, pass1 y pass2. Esta función nos deberá devolver otra función que nos sirva para validar el formulario.  Declararemos la función validadora en nuestro servicio de validadores creado en las lecciones anteriores. Así pues declaramos en validadores.service.ts:

```
passwordsIguales( pass1Value: string , pass2Value: string ) {

  return ( formGroup: FormGroup) => {

    const pass1Control = formGroup.controls[pass1Value];
    const pass2Control = formGroup.controls[pass2Value];

    if ( pass1Control.value === pass2Control.value ) {
      pass2Control.setErrors(null);
    } else {
      pass2Control.setErrors( {noEsIgual: true} );
    }
  }
}

```

Como dijimos, debe devolver una función, esta función recibirá un FormGroup (todo el formulario, al completo, todos sus campos, validaciones síncronas, etc), y lo que gestionará será la recepcion de los controles del formulario, una vez recibidos comprobaremos si sus valores son iguales, y usaremos el método setErrors para que tenga valor 'null' (validado correctamente) o en caso contrario se le añada un error que será un booleano con valor true.


[Volver al Índice](#%C3%ADndice-del-curso)

## 209. Reactivo: Validadores asíncronos

Para la validación asíncrona vamos a crear un nuevo campo en el formulario:

```
<div class="form-group row">
        <label class="col-2 col-form-label">Usuario</label>
        <div class="col">

            <input class="form-control" type="text" placeholder="Usuario" formControlName="usuario" [class.is-invalid]="usuarioNoValido">
            <small *ngIf="usuarioNoValido" class="text-danger">El nombre de usuario ya existe.</small>
        </div>
    </div>
```
```
get usuarioNoValido() {
    return this.forma.get('usuario').invalid && this.forma.get('usuario').touched;
  }
```

Recordar que al definir los campos del FormGroup en el FormBuilder, entre corchetes se situan: [valor, validacion síncrona, validación asíncrona], por tanto:

```
crearFormulario() {

    this.forma = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(5)]],
      apellidos: ['', [Validators.required, Validators.minLength(5), this.validadores.noHerrera ]],
      correo: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      usuario: ['', , this.validadores.existeUsuario ],
      ......
```

Mientras que en las funciones de validaciones síncronas devolvíamos una función que tenía como propiedad un string y devolvía un booleano, en las asíncronas haremos la misma devolución, pero a través de una promesa que tendremos que definir. Por tanto la función devolverá un Promise o un Observable.

Necesitaremos importar los observables de rxjs `import { Observable } from 'rxjs';`

Vamos a definir también una interfaz para definir un tipo de dato que será el de la promesa y el observable, este tipo lo que definirá será el objeto que queremos devolver, al igual que en las validaciones síncronas de ejemplo anteriores, un objeto que tendrá un string como propiedad y que devolverá un booleano, quedando la definición de la interfaz así:

```
interface ErrorValidate {
  [s:string]: boolean
}
```

De hecho ahora podríamos actualizar la validación noHerrera que teníamos, por ejemplo, de esta manera:

```
noHerrera( control: FormControl ):  ErrorValidate  {

    if ( control.value?.toLowerCase() === 'herrera') {
      return {
        noHerrera: true
      };
    }

    return null;
  }
```

Nuestro validador recibirá el control del formulario y lo que devolverá será el resultado de una nueva promesa, en la cual simularermos una petición http haciendo un timeout de 3500 (3 segundos y medio), en esta simulación de petición comprobaremos si el valor del control es igual a la cadena de ejemplo que queremos validar (el test es un supuesto usuario y verificar que no se está tratando de usar el mismo), en tal caso no se validará y el return (resolve, al ser una promesa) será crear una variable existe: true. En caso contrario será null.

```
existeUsuario( control: FormControl ): Promise<ErrorValidate> | Observable<ErrorValidate> {

    return new Promise( (resolve, reject) => {

      setTimeout(() => {
        if ( control.value === 'strider' ) {
          resolve({ existe: true })
        } else {
          resolve( null )
        }
      }, 3500);
    })
  }
```

Para terminar perfilaremos algo, pues cada vez que se carga el formulario se dispara la "petición http falseada", y no queremos eso, porque lo que nos interesa es que solo se dispare cuando introduzcamos datos en el input, para ello simplemente haremos una comprobación de si no tiene valor el input devuelva null para que no entre en la validación, mejor dicho, devolverá un resolve de promesa (definido y creado sobre la marcha) cuyo valor será null

```
 existeUsuario( control: FormControl ): Promise<ErrorValidate> | Observable<ErrorValidate> {

    if ( !control.value ) {
      return Promise.resolve(null)
    }

    return new Promise( (resolve, reject) => {

      setTimeout(() => {
        ......
```

[Volver al Índice](#%C3%ADndice-del-curso)

## 210. Reactivo: Detectar cambios en los valores, estado del formulario o controles

Nosotros tenemos un método para crear el formulario (crearFormulario()), por tanto podemos crear otro método que, después de la creación de este, active listeners, observadores, para verificar cambios en el mismo o sus atributos. Podemos llamar a este método en el constructor tras la creación del formulario y su carga de valores por defecto:

```
constructor( private fb: FormBuilder,
              private validadores: ValidadoresService ) {
  this.crearFormulario();
  this.cargarDataAlFormulario();
  this.crearListeners();
}
```

En el método podríamos, por ejemplo, usar una propiedad del objeto tipo formulario (en nuestro caso 'forma') que es un observable llamado valueChanges, al cual nos podremos subscribir y realizar alguna función cuando se realice algún tipo de cambio en el formulario, en nuestro ejemplo simplemente mostraremos el valor del formulario por consola:

```
crearListeners() {
    this.forma.valueChanges.subscribe( valor => {
      console.log(valor);
    })
  }
```

Igualmente podemos verificar el estado del formulario (válido, inválido, pendiente...) con el observable statusChanges

```
this.forma.statusChanges.subscribe( status => {
    console.log(status);
  });
```

Para que no haya confusión al ver el resultado del console log del estado (aparece primero valid y luego un montón de pending), primero se ejecutan las tareas síncronas y posteriormente las asíncronas, de ahí el resultado que vemos.

Si nos interesase un campo en específico podríamos hacer lo mismo, pero de esta manera:

```
this.forma.get('nombre').valueChanges.subscribe( nombre => {
    console.log(nombre);
  });
```

[Volver al Índice](#%C3%ADndice-del-curso)

## Cuestionario 4: Examen teórico sobre formularios

[Volver al Índice](#%C3%ADndice-del-curso)

## 211. Código fuente de los formularios

[Volver al Índice](#%C3%ADndice-del-curso)
